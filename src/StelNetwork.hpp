#ifndef STELNETWORK_H_
#define STELNETWORK_H_

#include "gameplay.h"
#include "StelNetObj.hpp"
#include "ReplicaManager3.h"
#include "NetworkIDManager.h"
#include "RakPeerInterface.h"
#include "MessageIdentifiers.h" // Check all MessageIDs here
#include "GetTime.h"
#include "StelDefines.hpp"
//#include "PacketFileLogger.h" // For debugging packets
#include <unordered_map>
#include <utility> 

#define SERVER_PORT 60000 // ITS OVER 9000!!!!! :DDD
#define DEFAULT_SERVER_MILLISECONDS_BETWEEN_UPDATES 50

using namespace gameplay;
using namespace RakNet;

class StelNetPackage;
class StelNetwork;

typedef std::function<StelNetObj*()> StelNetObjFactory;
typedef std::function<StelNetPackage*()> StelNetPackageFactory;

enum GameMessages
{
	ID_STEL_GAME_MSG = ID_USER_PACKET_ENUM +1
};

// One instance of Connection_RM2 is implicitly created per connection that uses ReplicaManager2
// It is designed this way so you can override per-connection behavior in your own game classes
class StelConnection : public Connection_RM3
{
public:

	StelConnection(const SystemAddress &_systemAddress, RakNetGUID _guid, StelNetwork* network) : Connection_RM3(_systemAddress, _guid), _network(network){}
	virtual ~StelConnection() {}

	// Callback used to create objects
	virtual Replica3* AllocReplica(RakNet::BitStream *allocationIdBitstream, ReplicaManager3 *replicaManager3);

private:
	StelNetwork* _network;
};

class StelReplicaMgr : public ReplicaManager3
{
public:
	StelReplicaMgr(StelNetwork* network) : _network(network){}

	virtual Connection_RM3* AllocConnection(const SystemAddress &systemAddress, RakNetGUID rakNetGUID) const
	{
		return new StelConnection(systemAddress, rakNetGUID, _network);
	}
	virtual void DeallocConnection(Connection_RM3 *connection) const 
	{
		delete connection;
	}
private:
	StelNetwork* _network;
};

class StelNetwork
{
public:
	StelNetwork();
	virtual ~StelNetwork();

	// Initilize network system
	virtual void initNetwork(const char* ipAddress) = 0;
	
	// Update de la red
	virtual void update(float elapsedTime) = 0;
	
	// Aqu� damos de alta el id en Nework, y la Factory para que pueda crear r�plicas
	void registerFactoryNetObj(StelNetObjFactory factory, std::string netObjId);
	// Aqu� damos de alta el id en Nework, y la Factory para que pueda crear paquetes
	void registerFactoryPkg(StelNetPackageFactory factory, StelNetPkgID id);
	// Get NetObject's id specific factory
	StelNetObjFactory getNetObjFactory(std::string netObjId);
	// Get NetObject's id specific factory
	StelNetPackageFactory getNetPckFactory(StelNetPkgID id);
	
	//Mandar (o encolar para mandar) un paquete
	void sendStelPackage(StelNetPkgID id, StelNetPackage* package);
	
	inline bool hasStarted(){ return _networkStarted; }
	inline RakPeerInterface* getPeer(){ return _peer; }
	inline uint64_t getMyGUID(){ return _peer->GetMyGUID().g; }
	inline uint64_t getUnassignedGUID(){ return UNASSIGNED_RAKNET_GUID.g; }

	std::vector<std::pair<int, StelNetPackage*> >* getInQueue() { return &_packageQueuein; }

protected:
	void sendOutPackages();

	bool _networkStarted;
	StelReplicaMgr* _replicaMgr;
	RakPeerInterface* _peer;
	NetworkIDManager* _networkIdMgr;
	std::unordered_map<std::string, StelNetObjFactory> _netObjFactoryMap;
	std::unordered_map<int, StelNetPackageFactory> _netPackFactoryMap;
	std::vector<std::pair<int, StelNetPackage*> > _packageQueueout;
	std::vector<std::pair<int, StelNetPackage*> > _packageQueuein;

private:
	//PacketFileLogger _pktLogger;
};

#endif
