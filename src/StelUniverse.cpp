#include "StelUniverse.hpp"
#include "StelRandom.hpp"


StelUniverse::StelUniverse()
: _scene(NULL)
{
}

StelUniverse::~StelUniverse()
{

}

void StelUniverse::initialize()
{
	// Create empty scene
	_scene = Scene::create("gameScene"); //Ricky: varias clases dependen de encontrar el scene con id == "gameScene", algo cutre eso

	// Scene nodes -------------------------------

}

void StelUniverse::finalize()
{
	SAFE_RELEASE ( _scene );
	//TODO hay muchos punteros a borrar, a por ejemplo
	//los vector con StelCosas*
}

void StelUniverse::update(float elapsedTime)
{
	//TODO esto hacer con bullet y collision ghosts
	for(StelPlayer* player : _players) {
		if(player->getState() != StelPlayer::BLACK_HOLE) {
			float nearestD = -1;
			Node* nearest = NULL;
			Vector3  playerPos = player->getPlayerNode()->getTranslationWorld();
			for(StelPlanet * planet :_planets) {
				float dist = planet->getNode()->getTranslationWorld().distanceSquared(playerPos);
				if(!nearest || nearestD > dist) {
					nearest = planet->getNode();
					nearestD = dist;
				}
			}
			for(StelBase* base : _bases) {
				float dist = base->getNode()->getTranslationWorld().distanceSquared(playerPos);
				if(!nearest || nearestD > dist) {
					nearest = base->getNode();
					nearestD = dist;
				}
			}
			
			player->setGravityParams(nearest, 300, true);
		}
		else {
			player->setGravityParams(NULL, 1, false);
		}
	}	
}

void StelUniverse::spawnBase(StelBase* base)
{
	_bases.push_back(base);
}

void StelUniverse::spawnPlayer(StelPlayer* player)
{
	_players.push_back(player);
}

void StelUniverse::spawnGenkiBall(StelGenkiBall* genkiBall)
{
	_genkiballs.push_back(genkiBall);
}

void StelUniverse::removePlayer(StelPlayer* player)
{
	_scene->removeNode(player->getPlayerNode());
	_players.erase(std::remove(_players.begin(), _players.end(), player), _players.end());
}

void StelUniverse::removeGenkiBall(StelGenkiBall* genki)
{
	_scene->removeNode(genki->getNode());
}