#include "StelNetSetup.hpp"
#include "StelController.hpp"
#include "StelUniverse.hpp"
#include <map>

/* Conditional includes */
#ifdef STELSERVER

#include "server/StelPlanetSrv.hpp"
#include "server/StelNetworkSrv.hpp"
#include "server/StelGenkiBallSrv.hpp"
#include "server/StelPlayerSrv.hpp"
#include "server/StelPelletSrv.hpp"
#include "server/StelBaseSrv.hpp"
#include "server/StelTotemSrv.hpp"

#else

#include "client/StelPlanetCln.hpp"
#include "client/StelNetworkCln.hpp"
#include "client/StelGenkiBallCln.hpp"
#include "client/StelPlayerCln.hpp"
#include "client/StelPelletCln.hpp"
#include "client/StelBaseCln.hpp"
#include "client/StelTotemCln.hpp"

#endif

/* macro to create the correct instances */
#ifdef STELSERVER
#define ClnOrSrv(x) x ## Srv
#else
#define ClnOrSrv(x) x ## Cln
#endif


template<typename T>
std::function<T*()> f()
{
	return  [] () { return new T; };
}


void initAllNetworkFactories(StelNetwork* network) 
{
	typedef std::function<StelNetObj*()> StelNetObjFactory;
	typedef std::function<StelNetPackage*()> StelNetPackageFactory;

	//NET OBJECTS *************************
	std::map<std::string ,StelNetObjFactory> net_obj_map = {
		{ "Planet",		f<ClnOrSrv(StelPlanet)>()},
		{ "Pellet",		f<ClnOrSrv(StelPellet)>()},
		{ "GenkiBall",	f<ClnOrSrv(StelGenkiBall)>()},
		{ "Player",		f<ClnOrSrv(StelPlayer)>() },
		{ "Base",		f<ClnOrSrv(StelBase)>() },
		{ "Totem",		f<ClnOrSrv(StelTotem)>() },
	};

	//**************************************


	//NET PACKETS
	std::map<StelNetPkgID, StelNetPackageFactory> packet_map = {
		{ CONTROLLER_CMDS,	f<ControllerCmdsPkg>() },
		{ TEAM_INFO,		f<TeamInfoPkg>() },
		{ GAME_STATE,		f<StelGameStateChangePkg>() },
		/* { CONTROLLER_CMDS,	f<???>()}, */
	};

	//**************************************

	// StringTable has to be called after RakPeer started, or else first call StringTable::AddRef() yourself [CHECK ME!]
	StringTable::Instance()->AddReference();

	for(auto x : net_obj_map) {
		network->registerFactoryNetObj(x.second, x.first);
	}

	for (auto x : packet_map) {
		network->registerFactoryPkg(x.second, x.first);
	}
}
