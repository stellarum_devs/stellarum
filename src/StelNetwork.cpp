#include "StelNetwork.hpp"
#include "StringTable.h"
#include "StelNetPackages.hpp"


Replica3* StelConnection::AllocReplica(RakNet::BitStream *allocationIdBitstream, ReplicaManager3 *replicaManager3)
{
	char objectName[128];
	//StringTable: predefine static strings to reduce bandwidth
	StringTable::Instance()->DecodeString(objectName, 128, allocationIdBitstream);

	//Both systems must have the same set of strings registered in advance, in the same order, and both systems must use 
	//StringTable and StringCompressor in the corresponding send and receive calls. You cannot mix and match the two

	StelNetObjFactory factory = _network->getNetObjFactory(objectName);
	StelNetObj* netObject = factory();

	return netObject;
}

//-----------------------

StelNetwork::StelNetwork() : _peer(NULL), _networkIdMgr(NULL), _replicaMgr(NULL), _networkStarted(false)
{
	_replicaMgr = new StelReplicaMgr(this);
	_networkIdMgr = new NetworkIDManager();
}

StelNetwork::~StelNetwork()
{
	RakNet::RakPeerInterface::DestroyInstance(_peer);
	RakNet::NetworkIDManager::DestroyInstance(_networkIdMgr);
	delete _replicaMgr;
}

void StelNetwork::registerFactoryNetObj(StelNetObjFactory factory, std::string netObjId)
{	
	StringTable::Instance()->AddString(netObjId.c_str(), true);
	auto i = _netObjFactoryMap.find(netObjId);
	if ( i == _netObjFactoryMap.end() ) {
		_netObjFactoryMap[netObjId] = factory;
	}
	else {
		 // NetObj factory id already in use!
		assert(false);
	}
}

void StelNetwork::registerFactoryPkg(StelNetPackageFactory factory, StelNetPkgID id)
{
	auto i = _netPackFactoryMap.find(id);
	if ( i == _netPackFactoryMap.end() ) {
		_netPackFactoryMap[id] = factory;
	}
	else {
		// NetPackage factory id already in use!
		assert(false);
	}
}

// Get NetObject's id specific factory
StelNetObjFactory StelNetwork::getNetObjFactory(std::string id)
{
	return _netObjFactoryMap[id];
}

// Get NetPackage's id specific factory
StelNetPackageFactory StelNetwork::getNetPckFactory(StelNetPkgID id)
{
	return _netPackFactoryMap[id];
}

void StelNetwork::sendStelPackage(StelNetPkgID id, StelNetPackage* package)
{
	_packageQueueout.push_back(std::make_pair(id,package));
}

void StelNetwork::sendOutPackages()
{
	//send packages in queue
	while (!_packageQueueout.empty()) {
		int id = _packageQueueout.back().first;
		StelNetPackage* p = _packageQueueout.back().second;
		_packageQueueout.pop_back();
		RakNet::BitStream bsOut;
		//id general, paquete de juego
		bsOut.Write((RakNet::MessageID)ID_STEL_GAME_MSG);
		//id del tipo de paquete de juego
		bsOut.Write(id);
		//contenido
		p->write(bsOut);
		_peer->Send(&bsOut, p->_priority, p->_reliability, 0, p->_sendToGuid ? AddressOrGUID(RakNetGUID(p->_guid)) : RakNet::UNASSIGNED_SYSTEM_ADDRESS, !p->_sendToGuid); //TODO fix broadcast
		delete p;
	}
}
