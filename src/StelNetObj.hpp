#ifndef STELNETOBJ_H_
#define STELNETOBJ_H_

#include "ReplicaManager3.h"
#include "StringTable.h"
#include <string>

using namespace RakNet;

// Esta clase esta bien tenerla, porq un monton de funciones q hay q poner por defecto de Replica3 son siempre igual
class StelNetObj : public Replica3
{
public:
	StelNetObj(){}
	virtual ~StelNetObj() {};

	virtual std::string getAllocID()const = 0;

private:
	// Replica3 methods
	// Write a unique identifer that can be read on a remote system to create an object of this same class
	virtual void WriteAllocationID(Connection_RM3 *destinationConnection, BitStream *allocationIdBitstream) const
	{StringTable::Instance()->EncodeString(getAllocID().c_str(), 128, allocationIdBitstream);}

	// QuerySerialization() is a first pass query to check if a given object should serializable to a given system. 
	// The intent is that the user implements with one of the defaults for client, server, or peer to peer.
	// Without this function, a careless implementation would serialize an object anytime it changed to all systems. 
	// This would give you feedback loops as the sender gets the same message back from the recipient it just sent to.
	virtual RM3ConstructionState QueryConstruction(Connection_RM3 *destinationConnection, ReplicaManager3 *replicaManager3) = 0;
	virtual bool QueryRemoteConstruction(Connection_RM3 *sourceConnection) = 0;

	// SerializeConstruction is used to write out data that you need to create this object in the context of your game,
	// such as health, score, name. Use it for data you only need to send when the object is created
	virtual void SerializeConstruction(BitStream *constructionBitstream, Connection_RM3 *destinationConnection) = 0;
	virtual bool DeserializeConstruction(BitStream *constructionBitstream, Connection_RM3 *sourceConnection) = 0;
	virtual void SerializeDestruction(BitStream *destructionBitstream, Connection_RM3 *destinationConnection) = 0;
	virtual bool DeserializeDestruction(BitStream *destructionBitstream, Connection_RM3 *sourceConnection) = 0;
	
	// Called every time the time interval to ReplicaManager3::SetAutoSerializeInterval() elapses and ReplicaManager3::Update is subsequently called.
	virtual RM3SerializationResult Serialize(SerializeParameters *serializeParameters) = 0;
	// Called whenever Serialize() is called with different data from the last send.
	virtual void Deserialize(DeserializeParameters *deserializeParameters) = 0;

	virtual RM3ActionOnPopConnection QueryActionOnPopConnection(Connection_RM3 *droppedConnection) const = 0;
	virtual void DeallocReplica(Connection_RM3 *sourceConnection) {delete this;}
	virtual RM3QuerySerializationResult QuerySerialization(Connection_RM3 *destinationConnection) = 0;
};

class StelNetObjCln : virtual public StelNetObj
{
public:
	StelNetObjCln(){}
	virtual ~StelNetObjCln(){}
	
private:
	void SerializeConstruction(BitStream *constructionBitstream, Connection_RM3 *destinationConnection) final { assert(false);}
	virtual bool DeserializeConstruction(BitStream *constructionBitstream, Connection_RM3 *sourceConnection) = 0;
	void SerializeDestruction(BitStream *destructionBitstream, Connection_RM3 *destinationConnection) final{ assert(false);}
	virtual bool DeserializeDestruction(BitStream *destructionBitstream, Connection_RM3 *sourceConnection) = 0;
	
	RM3SerializationResult Serialize(SerializeParameters *serializeParameters)final { assert(false); return RM3SR_NEVER_SERIALIZE_FOR_THIS_CONNECTION;}
	virtual void Deserialize(DeserializeParameters *deserializeParameters) = 0;

	RM3ConstructionState QueryConstruction(Connection_RM3 *destinationConnection, ReplicaManager3 *replicaManager3)
	{
		return QueryConstruction_ClientConstruction(destinationConnection, false);
	}

	bool QueryRemoteConstruction(Connection_RM3 *sourceConnection)
	{
		return QueryRemoteConstruction_ClientConstruction(sourceConnection, false);
	}

	RM3ActionOnPopConnection QueryActionOnPopConnection(Connection_RM3 *droppedConnection) const
	{
		return QueryActionOnPopConnection_Client(droppedConnection);
	}

	RM3QuerySerializationResult QuerySerialization(Connection_RM3 *destinationConnection)
	{
		return QuerySerialization_ClientSerializable(destinationConnection, false);
	}	
};

class StelNetObjSrv : virtual public StelNetObj
{
public:
	StelNetObjSrv(){}
	virtual ~StelNetObjSrv(){}
		
private:
	virtual void SerializeConstruction(BitStream *constructionBitstream, Connection_RM3 *destinationConnection) = 0;
	bool DeserializeConstruction(BitStream *constructionBitstream, Connection_RM3 *sourceConnection)final{ assert(false); return true;}
	virtual void SerializeDestruction(BitStream *destructionBitstream, Connection_RM3 *destinationConnection) = 0;
	bool DeserializeDestruction(BitStream *destructionBitstream, Connection_RM3 *sourceConnection)final{ assert(false); return true;}

	virtual RM3SerializationResult Serialize(SerializeParameters *serializeParameters) = 0;
	void Deserialize(DeserializeParameters *deserializeParameters) final { assert(false);}
	
	RM3ConstructionState QueryConstruction(Connection_RM3 *destinationConnection, ReplicaManager3 *replicaManager3)
	{
		return QueryConstruction_ServerConstruction(destinationConnection, true);
	}

	bool QueryRemoteConstruction(Connection_RM3 *sourceConnection)
	{
		return QueryRemoteConstruction_ServerConstruction(sourceConnection, true);
	}

	RM3ActionOnPopConnection QueryActionOnPopConnection(Connection_RM3 *droppedConnection) const
	{
		return QueryActionOnPopConnection_Server(droppedConnection);
	}

	RM3QuerySerializationResult QuerySerialization(Connection_RM3 *destinationConnection)
	{
		return QuerySerialization_ServerSerializable(destinationConnection, true);
	}	
};

#endif
