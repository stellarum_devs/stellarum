#include "StelNetCubeCln.hpp"
#include "../StelPrimitives.hpp"
#include "GetTime.h"

static const float DOWNWARD_ACCELERATION = -15.0f;
static const float POSITION_VARIANCE = 100.0f;

StelNetCubeCln::StelNetCubeCln() : _visiblePosition(Vector3::zero()),
 _visibleRotation(Quaternion::identity())
{
	// Buffer up for 3 seconds if we were to get 30 updates per second
	_transformationHistory.init(30, 3000);
	
	_enableInterpolation = true; // Manually set this here for testing

	// Initialize netcube node
	std::string nodeId = "netCube_" + std::to_string(Scene::getScene()->getNodeCount());
	_netCubeNode = Scene::getScene()->addNode(nodeId.c_str());
	Mesh* netCubeMesh = createCubeMesh(10.f);
	Model* netCubeModel = Model::create(netCubeMesh);
	_netCubeNode->setModel(netCubeModel);
	Material* netCubeMaterial = netCubeModel->setMaterial("res/materials/planets.material#salad");

	// Release items
	SAFE_RELEASE(netCubeMesh);
	SAFE_RELEASE(netCubeModel);
}

StelNetCubeCln::~StelNetCubeCln()
{
	//_networkRef->netCubeList.RemoveAtIndex(_networkRef->netCubeList.GetIndexOf(this));
	_netCubeNode->release();
}

void StelNetCubeCln::update(float elapsedTime)
{
	_visiblePosition = _position;
	_visibleRotation = _rotation;
	// interpolate visible position, lagging behind by a small amount so where know where to update to
	if (_enableInterpolation)
	{
		// Important: the first 3 parameters are in/out parameters, so set their values to the known current values before calling Read()
		// We are subtracting DEFAULT_SERVER_MILLISECONDS_BETWEEN_UPDATES from the current time to get an interpolated position in the past
		// Without this we wouldn't have a node to interpolate to, and wouldn't know where to go
		_transformationHistory.read(&_visiblePosition, NULL, &_visibleRotation, GetTimeMS() - DEFAULT_SERVER_MILLISECONDS_BETWEEN_UPDATES, GetTimeMS());
	}

	_netCubeNode->setTranslation(_visiblePosition);
	_netCubeNode->setRotation(_visibleRotation);
}

void StelNetCubeCln::Deserialize(DeserializeParameters *deserializeParameters)
{
	deserializeParameters->serializationBitstream[0].ReadVector(_position.x, _position.y, _position.z);
	deserializeParameters->serializationBitstream[0].ReadVector(_rotation.x, _rotation.y, _rotation.z);
	deserializeParameters->serializationBitstream[0].ReadVector(_velocity.x, _velocity.y, _velocity.z);

	// Every time we get a network packet, we write it to the transformation history class.
	// This class, given a time in the past, can then return to us an interpolated position of where we should be in at that time
	_transformationHistory.write(_position, _velocity, _rotation, RakNet::GetTimeMS());
}