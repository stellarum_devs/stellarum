#include "StelClient.hpp"
#include "../StelNetSetup.hpp"

// Declare our game instance
static StelClient game;

StelClient::StelClient()
: _universe(NULL), _font(NULL), _playerController(NULL), _ghostController(NULL), _network(NULL), _ui(NULL),
_usePlayer(false), _uiIsLocalhost(true), _menuForm(NULL), _menuVisible(true),_scout(false), cmd_time(0.0f), _gameState(NORMAL)
{
}

StelClient::~StelClient()
{
}

void StelClient::initialize()
{
	// Vsync off for now
	setVsync(false);

	// Network
	_network = new StelNetworkCln();
	initAllNetworkFactories(_network);

	// Create a scene with planets
	_universe = new StelUniverseCln();
	_universe->initialize();

	// Load font
	_font = Font::create("res/fonts/arial.gpb");

	// Create a Ghost Controller to move around freely
	_ghostController = new StelGhostController();
	_ghostController->initialize();
	_ghostController->setPosLookAt(Vector3(500.f, 500.f, 500.f), Vector3::zero());

	// Create this client's PlayetController
	_playerController = new StelLocalController(_network->getUnassignedGUID());

	// Init forms / UI
	_menuForm = Form::create("res/ui/menuControls.form");
	_menuForm->setVisible(false);
	_menuForm->getControl("toggleLocalRemoteButt")->addListener(this, Control::Listener::CLICK);
	_menuForm->getControl("joinGameButt")->addListener(this, Control::Listener::CLICK);
	_ui = &StelUI::getInstance();
	_menuForm->setVisible(_menuVisible);

	// For my crappy particles :P
    #ifdef __APPLE__
        glEnable(GL_PROGRAM_POINT_SIZE_EXT);
    #else
        glEnable(GL_PROGRAM_POINT_SIZE);
    #endif
	glEnable(GL_POINT_SPRITE);
}

void StelClient::finalize()
{
	SAFE_RELEASE(_menuForm);
	SAFE_DELETE(_universe);
	SAFE_RELEASE(_font);
}

void StelClient::update(float elapsedTime)
{
	if (_network->hasStarted()) {
		_network->update(elapsedTime); // this will need to change... lower updt freq
		processNetQueue();
	}
		
	//client, por ahora, siempre somos client a la vez que server
	_universe->update(elapsedTime);
	_ui->update(elapsedTime);
	if (!_usePlayer) {
		_ghostController->update(elapsedTime);
	}

	cmd_time += elapsedTime;
	if (this->_playerController->getPlayer() && cmd_time >= 30.0)
	{
		_playerController->createCmdPkg()->send(_network);
		cmd_time = 0;
		
	}
	if (this->_playerController->getPlayer())
		_playerController->update(elapsedTime);
	
}

void StelClient::render(float elapsedTime)
{
	/******************************************
	 * Render onto the scene's graphic target
	 *****************************************/
	
	FrameBuffer *prev = _universe->getSceneBuffer()->bind();
	// Clear the color and depth buffers
	clear(CLEAR_COLOR_DEPTH, Vector4::zero(), 1.0f, 0);
	// Visit all the nodes in the scene for drawing
	_universe->render(elapsedTime,debugMode(),wireframeEnabled(),scoutEnabled());
	prev->bind();
	
	/**************************
	 * Render onto the sceen
	 *************************/

	// Effects and final scene
	_universe->commitRender(elapsedTime);
	
	// Draw menu
	_menuForm->draw();
	_ui->render(elapsedTime);
	
	// Draw Health + Energy values
	drawValue(_font, Vector4(0, 0.5f, 1, 1), 5, 1, getFrameRate(), "%u");
	if (this->_playerController->getPlayer() && _usePlayer)
	{
		drawValue(_font, Vector4(1.f, 0.0f, 0.1f, 0.5f), 1124, 620, _playerController->getPlayer()->getHealth(), "Health %u");
		drawValue(_font, Vector4(0.f, 1.0f, 0.5f, 0.5f), 1124, 650, _playerController->getPlayer()->getEnergy(),"Energy %u") ;
		if(_playerController->getPlayer()->getChargingGenki())
			drawValue(_font, Vector4(1.f, 1.0f, 0.0f, 0.5f), 1124, 680, _playerController->getPlayer()->getEnergyInUse()," %u");
	}

	// Draw player names
	if (!_usePlayer)
	{
		Vector2 coord;
		Font* font = Font::create("res/fonts/arial.gpb");
		gameplay::Rectangle rec = getViewport();
		font->start();
		Camera* cam = _universe->getScene()->getActiveCamera();
		for (StelPlayer* p : *_universe->getPlayers())
		{
			Vector3 pPos = p->getPlayerNode()->getTranslationWorld();

			if (cam->getFrustum().intersects(pPos))
			{
				_universe->getScene()->getActiveCamera()->project(rec, pPos, &coord);
				font->drawText(p->getName().c_str(), coord.x, coord.y, Vector4::one());
				//p->getPlayerController()->getTeamID();
			}
		}
		font->finish();
		font->release();
	}
}

void StelClient::keyEvent(Keyboard::KeyEvent evt, int key)
{
	// Pass events to player & others
	if (_usePlayer)
		_playerController->keyEvent(evt, key);
	else
		_ghostController->keyEvent(evt, key);

	// General stuff
	if (evt == Keyboard::KEY_PRESS) {
		switch (key) {
			case Keyboard::KEY_V:
				_wireframe = !_wireframe;
				break;
			case Keyboard::KEY_B:
				_drawDebug = (_drawDebug + 1) % 2;
				break;
			case Keyboard::KEY_P:
				togglePlayer();
				break;
			case Keyboard::KEY_N:
				_scout = !_scout;
				break;
			case Keyboard::KEY_M:
				_menuVisible = !_menuVisible;
				_menuForm->setVisible(_menuVisible);
				break;
			case Keyboard::KEY_ESCAPE:
				exit();
				break;
		}
	}
}

bool StelClient::mouseEvent(Mouse::MouseEvent evt, int x, int y, int wheelDelta)
{
	if (_usePlayer)
		return _playerController->mouseEvent(evt, x, y, wheelDelta);
	else
		return _ghostController->mouseEvent(evt, x, y, wheelDelta);
}	

void StelClient::touchEvent(Touch::TouchEvent evt, int x, int y, unsigned int contactIndex)
{
	if (_usePlayer)
	{
		_playerController->touchEvent(evt, x, y, contactIndex);
	}
	else
	{
		_ghostController->touchEvent(evt, x, y, contactIndex);
	}
}

void StelClient::gamepadEvent(Gamepad::GamepadEvent evt, Gamepad* gamepad)
{
	if (_usePlayer)
	{
		_playerController->gamepadEvent(evt, gamepad);
	}
}

void StelClient::controlEvent(Control* control, EventType evt)
{
	if (strcmp(control->getId(), "toggleLocalRemoteButt") == 0)
	{
		_uiIsLocalhost = !_uiIsLocalhost;
		Properties* config = getConfig()->getNamespace("network", true);
		const char* ip = config->getString(_uiIsLocalhost ? "localhost" : "remotehost", "127.0.0.1");
		if (_uiIsLocalhost)
		{
			static_cast<Label*>(control)->setText("Local Host");
			_menuForm->getControl("ipNetworkTextBox")->setEnabled(false);
			static_cast<TextBox*>(_menuForm->getControl("ipNetworkTextBox"))->setText(ip);
		}
		else
		{
			static_cast<Label*>(control)->setText("Remote Host");
			static_cast<TextBox*>(_menuForm->getControl("ipNetworkTextBox"))->setText(ip);
		}
		_menuForm->getControl("ipNetworkTextBox")->setEnabled(!_uiIsLocalhost);
	}
	else if (strcmp(control->getId(), "joinGameButt") == 0)
	{
		_network->initNetwork(static_cast<TextBox*>(_menuForm->getControl("ipNetworkTextBox"))->getText());
		_menuVisible = false;
		_menuForm->setVisible(false);
	}
}

void StelClient::drawValue(Font* font, const Vector4& color, unsigned int x, unsigned int y, unsigned int value, const char* formatStr)
{
	char buffer[40];
	sprintf(buffer, formatStr, value);
	font->start();
	font->drawText(buffer, x, y, color, 30);
	font->finish();
}

void StelClient::togglePlayer()
{
	if (!_playerController->getPlayer())
		return;

	_usePlayer = !_usePlayer;
	setMouseCaptured(_usePlayer);
	_ui->enableDrawEnergy(_usePlayer);

	if (_usePlayer)
		_universe->getScene()->setActiveCamera(((StelPlayerCln*)_playerController->getPlayer())->getCameraNode()->getCamera());
	else
		_universe->getScene()->setActiveCamera(_ghostController->getCamera());
}

void StelClient::processTeamInfo(TeamInfoPkg* teamInfoPkg)
{
	StelTeamMgr::getInstance().setTeamMap(teamInfoPkg->_teamMap);
	_playerController->setTeamID(teamInfoPkg->_myID);

	std::string teamMsg = "Joined team: ";
	StelTeamMgr::TeamInfo teamInf = StelTeamMgr::getInstance().getTeamInfo(_playerController->getTeamID());
	_ui->setTeam(_playerController->getTeamID());
	teamMsg.append(teamInf._teamName);
	_ui->showDebugMsg(teamMsg, teamInf._teamColor);
}

void StelClient::processNetQueue()
{
	std::vector<std::pair<int, StelNetPackage*> >* queue =  _network->getInQueue();
	while (!queue->empty()) {
		StelNetPackage* p = queue->back().second;
		char dest[256];
		printf("read un paquete con id %d",p->_id);
		sprintf(dest, "read un paquete con id %d", p->_id);
		StelUI::getInstance().showDebugMsg(std::string(dest));
		p->execute(this);
		delete p;
		queue->pop_back();
	}
}

void StelClient::spawnPlayer(StelPlayerCln* player)
{
	_universe->getPlayers()->push_back(player);
	if (player->getGuid() == _network->getMyGUID())
	{
		_playerController->possess(player);
		togglePlayer();
	}
}

void StelClient::updateGameState(StelGameStateChangePkg* gamestatePkg)
{
	_gameState = gamestatePkg->_state;

	if (_gameState == GAME_OVER)
	{
		AudioSource* gameEndSound = AudioSource::create("res/audio/game_overE.ogg");
		gameEndSound->setGain(5.f);
		gameEndSound->play();
		SAFE_RELEASE(gameEndSound);

		StelTeamMgr::TeamInfo tInfo = StelTeamMgr::getInstance().getTeamInfo(gamestatePkg->_winnerTeamID);
		_ui->showEndGameMsg(tInfo);
	}
	if ( _gameState == LATE_GAME) 
	{
		AudioSource* lateGameSound = AudioSource::create("res/audio/hahahaE.ogg");
		lateGameSound->setGain(5.f);
		lateGameSound->play();
		SAFE_RELEASE(lateGameSound);

		for (StelPlayer* player : *_universe->getPlayers())
		{
			player->setState(StelPlayer::BLACK_HOLE);
		}
		_universe->setDarkSun(true);
	}
}
