#ifndef __STELPARTICLEMGR_H__
#define __STELPARTICLEMGR_H__

#include "gameplay.h"
#include <forward_list>
using namespace gameplay;

struct StelParticle;

class StelParticleMgr
{
public:
	StelParticleMgr(Scene* scene);
	~StelParticleMgr() {};

	void update(float elapsedTime);

	void addExplosion(Vector3 pos, bool repeat);
	void createStars();
	inline void setScene(Scene* scene){ _scene = scene; }
	inline Scene* getScene(){ return _scene; }

	Model* createStarsModel(unsigned int starCount = 300, float radius = 15.f, bool isStatic = false);

private:

	std::forward_list<StelParticle*> particles;
	Scene* _scene;
};

struct StelParticle
{
	float elapsedTime;
	float duration;
	bool repeat;
	MaterialParameter* matParam;
	StelParticleMgr* pMgr;
	Node* pNode;
	StelParticle(StelParticleMgr* mgr, bool rpt) : pMgr(mgr), repeat(rpt){}
};

#endif
