#include "StelLocalController.hpp"

StelLocalController::StelLocalController(uint64_t guid) : StelController(guid),
_gamepad(NULL), _x(0), _y(0), _timeSincePressed(0.0f), _checkTimeSincePressed(false)
{
}

void StelLocalController::gamepadEvent(Gamepad::GamepadEvent evt, Gamepad* gamepad)
{
	switch (evt)
	{
	case Gamepad::CONNECTED_EVENT:
		_gamepad = Game::getInstance()->getGamepad(0);
		break;
	case Gamepad::DISCONNECTED_EVENT:
		break;
	default:
		break;
	}
}

void StelLocalController::keyEvent(Keyboard::KeyEvent evt, int key)
{
	if (evt == Keyboard::KEY_PRESS) {
		switch (key) {
			case Keyboard::KEY_W:
				_keyFlags |= StelKeyFlags::MOVE_FORWARD;
				break;
			case Keyboard::KEY_S:
				_keyFlags |= StelKeyFlags::MOVE_BACKWARD;
				break;
			case Keyboard::KEY_A:
				_keyFlags |= StelKeyFlags::MOVE_LEFT;
				break;
			case Keyboard::KEY_D:
				_keyFlags |= StelKeyFlags::MOVE_RIGHT;
				break;				
			case Keyboard::KEY_Q:
				_keyFlags |= StelKeyFlags::MOVE_DOWN;
				break;
			case Keyboard::KEY_E:
				_keyFlags |= StelKeyFlags::MOVE_UP;
				break;
			case Keyboard::KEY_SHIFT:
				_keyFlags |= StelKeyFlags::MOVE_RUN;
				break;
			case Keyboard::KEY_SPACE: {
				_keyFlags |= StelKeyFlags::JUMP;
				if(!_checkTimeSincePressed)
				{
					_checkTimeSincePressed = true;
					_timeSincePressed = 0.0f;
				}
				break;
			}
		}
	}
	else if (evt == Keyboard::KEY_RELEASE) {
		switch (key) {
			case Keyboard::KEY_W:
				_keyFlags &= ~StelKeyFlags::MOVE_FORWARD;
				break;
			case Keyboard::KEY_S:
				_keyFlags &= ~StelKeyFlags::MOVE_BACKWARD;
				break;
			case Keyboard::KEY_A:
				_keyFlags &= ~StelKeyFlags::MOVE_LEFT;
				break;
			case Keyboard::KEY_D:
				_keyFlags &= ~StelKeyFlags::MOVE_RIGHT;
				break;
			case Keyboard::KEY_Q:
				_keyFlags &= ~StelKeyFlags::MOVE_DOWN;
				break;
			case Keyboard::KEY_E:
				_keyFlags &= ~StelKeyFlags::MOVE_UP;
				break;
			case Keyboard::KEY_SHIFT:
				_keyFlags &= ~StelKeyFlags::MOVE_RUN;
				break;
			case Keyboard::KEY_SPACE: {
				_keyFlags &= ~StelKeyFlags::GAS;
				_keyFlags &= ~StelKeyFlags::JUMP;
				_checkTimeSincePressed = false;
				break;
			}

		}
	}
}

void StelLocalController::touchEvent(Touch::TouchEvent evt, int x, int y, unsigned int contactIndex)
{
}

bool StelLocalController::mouseEvent(Mouse::MouseEvent evt, int x, int y, int wheelDelta)
{
	switch (evt)
	{
	case Mouse::MOUSE_PRESS_RIGHT_BUTTON:
		_keyFlags |= StelKeyFlags::FIRE;
		_player->startCharging();
		return true;
	case Mouse::MOUSE_RELEASE_RIGHT_BUTTON:
		_keyFlags &= ~StelKeyFlags::FIRE;
		_player->stopCharging();
		return true;
	case Mouse::MOUSE_PRESS_LEFT_BUTTON:
		_keyFlags |= StelKeyFlags::ALT_FIRE;
		_player->startUsingObj();
		return true;
	case Mouse::MOUSE_RELEASE_LEFT_BUTTON:
		_keyFlags &= ~StelKeyFlags::ALT_FIRE;
		_player->stopUsingObj();
		return true;
	case Mouse::MOUSE_MOVE:
		_x+=x;
		_y+=y;
		return true;
	default:
		return false;
	}
}

void StelLocalController::update ( float elapsedTime )
{
	float sens =  (1.0f/elapsedTime);
	
	if(_x != 0) 
	{
		_player->getPlayerNode()->rotateY(-MATH_DEG_TO_RAD(_x * sens));
	}

	if(_y!=0) 
	{
		if (!_player->getPhysicsCharacter()->isPhysicsEnabled()) {
			_player->getPlayerNode()->rotateX(-MATH_DEG_TO_RAD(_y * sens));
		}
		else {
			((StelPlayerCln*)_player)->getCameraNode()->rotateX(-MATH_DEG_TO_RAD(_y *sens));
		}
	}
	_x = 0;
	_y = 0;

	// Hold jump key time update
	if(_checkTimeSincePressed) 
	{
		_timeSincePressed += elapsedTime;
		if (_timeSincePressed >= 1000)
		{
			_keyFlags |= StelKeyFlags::GAS;
			_checkTimeSincePressed = false;
		}
	}
}


void StelLocalController::fireGenkiBall()
{
	((StelPlayerCln*)_player)->playSoundLaunch();
}

ControllerCmdsPkg* StelLocalController::createCmdPkg() const {
	ControllerCmdsPkg* p = new ControllerCmdsPkg();
	p->_guid = _guid;
	p->flags = _keyFlags;
	p->rot = _player->getPlayerNode()->getRotation();
	Node* c = ((StelPlayerCln*)_player)->getCameraNode();
	if(c) {
		p->cam = c->getForwardVectorWorld();
	}
	return p;
}
