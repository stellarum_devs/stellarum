#include "StelPlayerCln.hpp"
#include "../StelController.hpp"
#include "StelClient.hpp"
#include "StringCompressor.h"
#include "../StelPrimitives.hpp"
#include "StelUI.hpp"

StelPlayerCln::StelPlayerCln() : 
_camera(NULL),
_genkiBallNode(NULL),
_genkiBallChargeSound(NULL),
_genkiBallLaunchSound(NULL),
_genkiBallPE(NULL),
_isUsing(false)
{
	// Buffer up for 500 mseconds if we were to get 1 update per 50 msecond
	_transformationHistory.init(25, 500);

	_enableInterpolation = true; // Manually set this here for testing
}

StelPlayerCln::~StelPlayerCln()
{
}

void StelPlayerCln::initialize()
{
	std::string nodeName = _isBat ? "bat" : "micro_orc";
	std::string bundlePath = _isBat ? "res/models/bat.gpb" : "res/models/orc.gpb";
	std::string matPath = _isBat ? "res/materials/bat.material#bat" : "res/materials/orc.material#orc";
	std::string physPath = _isBat ? "res/physics/bat.physics#bat" : "res/physics/orc.physics#orc";
	std::string animPath = _isBat ? "res/anims/bat.animation" : "res/anims/orc.animation";

	// Load player mesh, phys, anims...
	Bundle* bundle = Bundle::create(bundlePath.c_str());
	GP_ASSERT(bundle);
	_playerNode = bundle->loadNode(nodeName.c_str());
	Material* playerMaterial = _playerNode->getModel()->setMaterial(matPath.c_str());
	GP_ASSERT(playerMaterial);

	//Maybe not the cleanest way
	Node* light = ((StelClient*)Game::getInstance())->getUniverse()->getMainLight();
	playerMaterial->getTechnique()->getParameter("u_ambientColor")->setValue(Vector3(0.1f, 0.0f, 0.0f));
	playerMaterial->getTechnique()->getParameter("u_pointLightColor[0]")->setValue(Vector3(1.1f, 1.0f, 1.0f));
	//playerMaterial->getTechnique()->getParameter("u_pointLightPosition[0]")->bindValue(light, &Node::getTranslationView);
	playerMaterial->getTechnique()->getParameter("u_pointLightRangeInverse[0]")->setValue(light->getLight()->getRangeInverse());

	// Load player physics
	_playerNode->setCollisionObject(physPath.c_str());
	_playerPhysics = static_cast<PhysicsCharacter*>(_playerNode->getCollisionObject());
	_jetPhysics = new StelJetPhysics(_playerNode);
	_playerPhysics->setPhysicsEnabled(false);
	GP_ASSERT(_playerPhysics);
	_scene->addNode(_playerNode);

	// Load character animations.
	_playerAnimation = _playerNode->getAnimation("animations");
	GP_ASSERT(_playerAnimation);
	_playerAnimation->createClips(animPath.c_str());
	_jumpClip = _playerAnimation->getClip("jump");
	GP_ASSERT(_jumpClip);
	_jumpClip->addListener(this, _jumpClip->getDuration() - 20);
	SAFE_RELEASE(bundle);

	_camera = Camera::createPerspective(80.0f, Game::getInstance()->getAspectRatio(), 0.4f, 1000000.0f);
	_cameraNode = Node::create("playerCameraNode");
	_playerNode->addChild(_cameraNode);
	_cameraNode->setCamera(_camera);
	_cameraNode->setTranslationY(PLAYER_CAM_OFFSET);
	
	// GenkiBall crap
	_genkiBallNode = Node::create("GenkiBallPlayerNode");
	Model* model = Model::create(createSphereMesh(2.0, 8, 12));
	model->setMaterial("res/materials/genkiball.material#genkiball");
	_genkiBallNode->setModel(model);
	_genkiBallPE = ParticleEmitter::create("res/particles/genkiBall.particle");	
	_cameraNode->addChild(_genkiBallNode);
	_genkiBallNode->translateForward(10.f);
	_genkiBallNode->setScale(0.f);
	_genkiBallNode->setParticleEmitter(_genkiBallPE);

	// Using objects interaction
	_colTestNode = _scene->addNode("ColTestNode");
	_colTestNode->setModel(Model::create(createCubeMesh()));
	_colTestNode->getModel()->setMaterial("res/materials/genkiball.material#genkiball");

	// Preload audio
	_genkiBallChargeSound = AudioSource::create("res/audio/basicbeam_charge.ogg");
	_genkiBallChargeSound->setGain(0.3f);
	_genkiBallLaunchSound = AudioSource::create("res/audio/basicbeam_fire.ogg");
	_genkiBallLaunchSound->setGain(0.3f);

	// Used in universe for checking stuff
	_playerNode->setTag("StelClass", "StelPlayer");
	_playerNode->setUserPointer(this);
}

void StelPlayerCln::update(float elapsedTime)
{
	if(_health == 0) {
		//WE ARE NOT ALIVE -> STATUE
		//if(_playerController) {
		//	StelClient* game = (StelClient*) Game::getInstance();
		//	game->togglePlayer(); //TODO no vale con esto
		//}
		return;
	} 
		
	
	PlayerState oldState = _state;
	StelPlayer::update(elapsedTime);

	// Update character animation
	if (_currentDirection.isZero())
	{
		playAnim("idle", true);
	}
	else
	{
		bool running = (_currentDirection.lengthSquared() > 0.75f);
		playAnim(running ? "running" : "walking", true, 1.0f);
	}

	// Interpolate our values with the server's
	_position = _playerNode->getTranslation();
	_rotation= _playerNode->getRotation();
	_transformationHistory.read(&_position, &_velocity,_playerController? NULL: &_rotation, GetTimeMS() - DEFAULT_SERVER_MILLISECONDS_BETWEEN_UPDATES, GetTimeMS());
	switch(_state){
		case ON_PLANET: 
		{
			_playerPhysics->setVerticalVelocity(_velocity);
			break;
		}
		case JETPACK: 
		{
			_jetPhysics->setVelocity(_velocity);
			break;
		}
	}
	//Things to do when the _nextState is  different, for instance fix the fucking camera
	if(_state == JETPACK && oldState==ON_PLANET) {
		Quaternion cameraQ = _cameraNode->getRotation();
		_rotation.multiply(cameraQ);
		_cameraNode->setRotation(Quaternion::identity());
	}
	if(_state == ON_PLANET && oldState==JETPACK) {
	}
	
	_playerNode->setTranslation(_position);
	_playerNode->setRotation(_rotation);


	// GenkiBall
	_genkiBallPE->update(elapsedTime);
	if (_chargingGenkiBall)
	{
		//StelGenkiBall::charge(_genkiBallPE, elapsedTime); TODO
		//if (_genkiBallNode->getScale().lengthSquared() < 5.0f)
			_genkiBallNode->setScale(_chargeTime * 0.0005);
	}

	if (_isUsing)
	{
		_pickRay.setDirection(_cameraNode->getForwardVectorWorld());
		_pickRay.setOrigin(_cameraNode->getTranslationWorld());

		// using debug
		PhysicsController::HitResult hitResult;
		if (Game::getInstance()->getPhysicsController()->rayTest(_pickRay, PLAYER_USE_DIST, &hitResult))
		{
			_colTestNode->setTranslation(hitResult.point);
			_colTestNode->setScale(0.5f);
		}
		else
		{
			_colTestNode->setScale(0.f);
		}
	}
}

void StelPlayerCln::startCharging()
{
	StelPlayer::startCharging();

	_genkiBallPE->start();
	playSoundCharge();
}

void StelPlayerCln::playSoundPositional(const char* sound)
{
	AudioSource* audioSrc = AudioSource::create(sound);
	_playerNode->setAudioSource(audioSrc);
	audioSrc->setGain(5.0f);
	audioSrc->play();
	SAFE_RELEASE(audioSrc);
}

void StelPlayerCln::stopCharging()
{
	StelPlayer::stopCharging();

	_genkiBallNode->setScale(0.f);
	_genkiBallPE->stop();
}

void StelPlayerCln::playSoundCharge()
{
	_genkiBallNode->setAudioSource(_genkiBallChargeSound);
	_genkiBallChargeSound->play();
}

void StelPlayerCln::playSoundLaunch()
{
	_genkiBallNode->setAudioSource(_genkiBallLaunchSound);
	_genkiBallLaunchSound->play();
}

void StelPlayerCln::startUsingObj()
{
	_isUsing = true;

	// show lazor effect
	_pickRay.setDirection(_cameraNode->getForwardVectorWorld());
	_pickRay.setOrigin(_cameraNode->getTranslationWorld());

	//2: Do raytest when click comes, if hit usable, save it and call startUsing()
	PhysicsController::HitResult hitResult;
	if (Game::getInstance()->getPhysicsController()->rayTest(_pickRay, PLAYER_USE_DIST, &hitResult))
	{
		_colTestNode->setScale(0.5f);
		_colTestNode->setTranslation(hitResult.point);
	}
}

void StelPlayerCln::stopUsingObj()
{
	_isUsing = false;
	_colTestNode->setScale(0.f);
}

bool StelPlayerCln::DeserializeConstruction(BitStream *constructionBitstream, Connection_RM3 *sourceConnection)
{
	constructionBitstream->ReadAlignedBytes((unsigned char*)&_guid, sizeof(_guid));
	char str[64];
	StringCompressor::Instance()->DecodeString(str, 64, constructionBitstream);
	_name = str;

	_isBat = constructionBitstream->ReadBit();

	initialize();

	//Maybe not the cleanest way
	StelClient* game = (StelClient*) Game::getInstance();
	game->spawnPlayer(this);
	
	return true;
}

void StelPlayerCln::Deserialize(DeserializeParameters *deserializeParameters)
{
	btQuaternion oldRot(_rotation.x, _rotation.y, _rotation.z, _rotation.w);

	int oldHealth = _health;

	deserializeParameters->serializationBitstream[0].ReadAlignedBytes((unsigned char*)&_position, sizeof(_position));
	deserializeParameters->serializationBitstream[0].ReadAlignedBytes((unsigned char*)&_rotation, sizeof(_rotation));
	deserializeParameters->serializationBitstream[0].ReadAlignedBytes((unsigned char*)&_velocity, sizeof(_velocity));
	bool updateEnergyHealth = deserializeParameters->serializationBitstream[0].ReadBit();
	if (updateEnergyHealth)
	{
		deserializeParameters->serializationBitstream[0].ReadBitsFromIntegerRange(_health, 0, PLAYER_MAX_HEALTH);
		deserializeParameters->serializationBitstream[0].ReadFloat16(_energy, 0.0f, PLAYER_MAX_ENERGY);
	}
	bool charging = deserializeParameters->serializationBitstream[0].ReadBit();

	// TODO: use player states DEAD etc
	if (updateEnergyHealth && oldHealth < _health){ // means we died, very ugly hack
		playSoundPositional("res/audio/fu_asshole.ogg");
		_playerPhysics->setVelocity(Vector3::zero());
		_transformationHistory.clear();
		_transformationHistory.write(_position, _velocity, _rotation, RakNet::GetTimeMS());
		return;
	}

	// If were're not possessed do this manually, // TODO handle deaths and other possibilities (aka use states)
	if (!_playerController)
	{
		if (charging)
		{
			// If we weren't already charging then start
			if (!_chargingGenkiBall)
				startCharging();
		}
		else
		{
			// If we were charging stop charging
			if (_chargingGenkiBall)
				stopCharging();
		}
	}
	else {
		//Update UI 
		StelUI::getInstance().setEnergyValue(_energy,  _energyInUse);
	}


	// Every time we get a network packet, we write it to the transformation history class.
	// This class, given a time in the past, can then return to us an interpolated position of where we should be in at that time
	_transformationHistory.write(_position, _velocity, _rotation, RakNet::GetTimeMS());

}

bool StelPlayerCln::DeserializeDestruction(BitStream *destructionBitstream, Connection_RM3 *sourceConnection)
{
	// Spawn explosion gfx sfx mammoth
	printf("Borrando player - client\n");//TODO
	//Maybe not the cleanest way
	StelClient* game = (StelClient*)Game::getInstance();
	game->getUniverse()->removePlayer(this);

	return true;
}