#include "StelParticleMgr.hpp"

StelParticleMgr::StelParticleMgr(Scene* scene) : _scene(scene)
{
}

void StelParticleMgr::addExplosion(Vector3 pos, bool repeat)
{
	StelParticle* particle = new StelParticle(this, repeat);
	particle->pNode = _scene->addNode("starExplosion");
	particle->pNode->setModel(createStarsModel());

	particle->pNode->setTranslation(pos);
	particle->pNode->getModel()->getMaterial()->getParameter("u_center")->setVector3(pos);
	
	particle->matParam = particle->pNode->getModel()->getMaterial()->getParameter("u_elapsedTime");
	particle->elapsedTime = 0.0f;
	particle->duration = 12.0f;
	particles.push_front(particle);
}

void StelParticleMgr::update(float elapsedTime)
{
	// update shader and remove if effect duration has completed
	particles.remove_if([elapsedTime](StelParticle* p)
	{
		p->elapsedTime += elapsedTime * 0.01;
		if (p->repeat)
			p->elapsedTime = fmod(p->elapsedTime, p->duration);
		p->matParam->setValue(p->elapsedTime);

		if (p->elapsedTime > p->duration)	// better this than 2 update loops
			p->pMgr->getScene()->removeNode(p->pNode);

		return p->elapsedTime > p->duration;
	});
}

static unsigned int seed = 0x13371337;

static inline float random_float()
{
	float res;
	unsigned int tmp;

	seed *= 16807;

	tmp = seed ^ (seed >> 4) ^ (seed << 15);

	*((unsigned int *)&res) = (tmp >> 9) | 0x3F800000;

	return (res - 1.0f);
}

Model* StelParticleMgr::createStarsModel(unsigned int starCount, float radius, bool isStatic)
{
	const unsigned int pointCount = starCount;
	const unsigned int verticesSize = starCount * (3 + 3);  // (3 (position(xyz) + 3 color(rgb))

	std::vector<float> vertices;
	vertices.resize(verticesSize);

	Vector3 pos, col;
	unsigned int vertexCounter = -1; // lolololo esto es un poco asco
	for (unsigned int i = 0; i < pointCount; i++)
	{
		// position
		/*
		vertices[++vertexCounter] = (random_float() * 2.0f - 1.0f) * 100.0f;
		vertices[++vertexCounter] = (random_float() * 2.0f - 1.0f) * 100.0f;
		vertices[++vertexCounter] = random_float();
		*/

		/*
		float angle = random_float() * MATH_PIX2;
		vertices[++vertexCounter] = cosf(angle)*radius;
		vertices[++vertexCounter] = sinf(angle)*radius;
		vertices[++vertexCounter] = random_float();
		*/

		float phi = MATH_RANDOM_0_1() * MATH_PIX2;
		float costheta = MATH_RANDOM_MINUS1_1();
		float u = MATH_RANDOM_0_1();

		float theta = acos(costheta);
		float r = radius * cbrt(u);

		vertices[++vertexCounter] = r * sin(theta) * cos(phi);
		vertices[++vertexCounter] = r * sin(theta) * sin(phi);
		vertices[++vertexCounter] = r * cos(theta);

		// color
		vertices[++vertexCounter] = 0.9f + random_float() * 0.2f;
		vertices[++vertexCounter] = 0.9f + random_float() * 0.2f;
		vertices[++vertexCounter] = 0.9f + random_float() * 0.2f;
	}

	VertexFormat::Element elements[] =
	{
		VertexFormat::Element(VertexFormat::POSITION, 3),
		VertexFormat::Element(VertexFormat::COLOR, 3)
	};
	Mesh* mesh = Mesh::createMesh(VertexFormat(elements, 2), pointCount, false);
	if (mesh == NULL)
	{
		return NULL;
	}
	mesh->setPrimitiveType(Mesh::POINTS);
	mesh->setVertexData(&vertices[0], 0, pointCount);

	mesh->setBoundingBox(BoundingBox(Vector3(-radius, -radius, -radius), Vector3(radius, radius, radius)));
	mesh->setBoundingSphere(BoundingSphere(Vector3::zero(), radius));

	Model* model = Model::create(mesh);
	if (isStatic)
		model->setMaterial("res/materials/star.material#star_static");
	else
		model->setMaterial("res/materials/star.material#star");
	SAFE_RELEASE(mesh);
	return model;
}