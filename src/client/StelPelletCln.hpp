#ifndef STELPELLET_CLN_H_
#define STELPELLET_CLN_H_

#include "../StelPellet.hpp"

class StelPelletCln : public StelNetObjCln, public StelPellet
{
public:
	StelPelletCln();
	~StelPelletCln();

	void initialize();

	void update(float elapsedTime);

protected:
	ParticleEmitter* _particleEmitter;
	
private:

	// Replica3
	void Deserialize(DeserializeParameters *deserializeParameters);
	bool DeserializeConstruction(BitStream *constructionBitstream, Connection_RM3 *sourceConnection);
	bool DeserializeDestruction(BitStream *destructionBitstream, Connection_RM3 *sourceConnection);
	
};

#endif
