
#ifndef STELDEBUGDRAWER_H
#define STELDEBUGDRAWER_H

#include "gameplay.h"

using namespace gameplay;

class StelDebugDrawer {

public:

	/**
	* DebugVertex.
	* @script{ignore}
	*/
	struct DebugVertex
	{
		/**
		* The x coordinate of the vertex.
		*/
		float x;

		/**
		* The y coordinate of the vertex.
		*/
		float y;

		/**
		* The z coordinate of the vertex.
		*/
		float z;

		/**
		* The red color component of the vertex.
		*/
		float r;

		/**
		* The green color component of the vertex.
		*/
		float g;

		/**
		* The blue color component of the vertex.
		*/
		float b;

		/**
		* The alpha component of the vertex.
		*/
		float a;
	};

	/**
	* Constructor.
	*/
	StelDebugDrawer();

	/**
	* Destructor.
	*/
	~StelDebugDrawer();

	void begin(const Matrix& viewProjection);
	void end();

	void drawLine(Vector3 from, Vector3 to, Vector3 fromColor, Vector3 toColor);
	void drawLine(Vector3 from, Vector3 to, Vector3 color);
	void drawContactPoint(Vector3 pointOnB, Vector3 normalOnB, float distance, int lifeTime, Vector3 color);

private:

	int _mode;
	MeshBatch* _meshBatch;
	int _lineCount;
};

#endif // STELDEBUGDRAWER_H
