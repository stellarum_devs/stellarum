#include "StelSun.hpp"

#define LENSFLARE_ENABLED 1
#define SUN_QUALITY .5f

StelSun::StelSun()
: _sunNode(NULL), _isSunVisible(false), _frameBuffer(NULL),
	_quadModel(NULL), _dirtTextureSampler(NULL), _sunPosProj(0.0, 0.0, 0.0, 0.0)
{
}

StelSun::~StelSun()
{
	SAFE_RELEASE(_depthTestMaterial);
	SAFE_RELEASE(_lensFlareMaterial);
	SAFE_RELEASE(_verticalBlurMaterial);
	SAFE_RELEASE(_horizontalBlurMaterial);
	
	for (int i = 0; i < 5; i++) {
		SAFE_RELEASE(_sunTextures[i]);
		SAFE_RELEASE(_sunTextureSamplers[i]);
	}
	SAFE_RELEASE(_dirtTextureSampler);
	
	SAFE_RELEASE(_quadModel);
	SAFE_RELEASE(_frameBuffer);
}

void StelSun::initialize(Node *sunNode, float sunRadius)
{
	_sunNode = sunNode;
	_sunRadius = sunRadius;
	
	Rectangle viewPort = Game::getInstance()->getViewport();
	_sceneWidth = viewPort.width;
	_sceneHeight = viewPort.height;
	_bufferWidth = _sceneWidth * SUN_QUALITY;
	_bufferHeight = _sceneHeight * SUN_QUALITY;
	_sceneAspectRatio = _bufferWidth / _bufferHeight;
	
	// Sun framebuffer
	_frameBuffer = FrameBuffer::create("SunEffectBuffer", _bufferWidth, _bufferHeight);
	
	// Textures
	Texture *dirtTexture = Texture::create("res/sun/lensdirt_lowc.png");
	_dirtTextureSampler = Texture::Sampler::create(dirtTexture);
	
	for (int i = 0; i < 5; i++) {
		std::ostringstream stringStream;
		stringStream << "sun" << i;
		std::string copyOfStr = stringStream.str();
		
		if (i != 4) {
			_sunTextures[i] = RenderTarget::create(copyOfStr.c_str(), _bufferWidth, _bufferHeight);
			
			_sunTextureSamplers[i] = Texture::Sampler::create(_sunTextures[i]->getTexture());
			_sunTextureSamplers[i]->setFilterMode(Texture::LINEAR, Texture::LINEAR);
			_sunTextureSamplers[i]->setWrapMode(Texture::CLAMP, Texture::CLAMP);
		} else {
			_sunTextures[i] = RenderTarget::create(copyOfStr.c_str(), _bufferWidth, _bufferHeight, true);
			
			_sunTextureSamplers[i] = Texture::Sampler::create(_sunTextures[i]->getTexture());
			_sunTextureSamplers[i]->setFilterMode(Texture::NEAREST, Texture::NEAREST);
			// If this fails, consider using setWrapMode((Texture::Wrap) GL_CLAMP, (Texture::Wrap) GL_CLAMP);
			_sunTextureSamplers[i]->setWrapMode(Texture::CLAMP, Texture::CLAMP);
		}
	}
	
	Mesh* mesh = Mesh::createQuadFullscreen();
	_quadModel = Model::create(mesh);
	SAFE_RELEASE(mesh);
	
	_depthTestMaterial = Material::create("res/sun/sun.material#depth_test");
	_depthTestMaterial->getParameter("u_sunTexture")->setValue(_sunTextureSamplers[1]);
	_depthTestMaterial->getParameter("u_sunDepthTexture")->setValue(_sunTextureSamplers[4]);
	
	_lensFlareMaterial = Material::create("res/sun/sun.material#lens_flare");
	_lensFlareMaterial->getParameter("u_lowBlurredSunTexture")->setValue(_sunTextureSamplers[1]);
	_lensFlareMaterial->getParameter("u_highBlurredSunTexture")->setValue(_sunTextureSamplers[2]);
	_lensFlareMaterial->getParameter("u_dirtTexture")->setValue(_dirtTextureSampler);
	
	_verticalBlurMaterial = Material::create("res/sun/sun.material#vertical_blur");
	_verticalBlurMaterial->getParameter("u_odh")->setValue(1 / (float) _bufferWidth);
	_horizontalBlurMaterial = Material::create("res/sun/sun.material#horizontal_blur");
	_horizontalBlurMaterial->getParameter("u_odw")->setValue(1 / (float) _bufferHeight);
	
	_quadModel->setMaterial(_depthTestMaterial);
	
	if (LENSFLARE_ENABLED)
		sunNode->setActive(false);
	
	_biasMatrix = Matrix(0.5f, 0.0f, 0.0f, 0.5f, 0.0f, 0.5f, 0.0f, 0.5f, 0.0f, 0.0f, 0.5f, 0.5f, 0.f, 0.f, 0.f, 1.0f);
}

void StelSun::computeEffect(Texture::Sampler* sceneColor, Texture::Sampler* sceneDepth)
{
	if (!LENSFLARE_ENABLED)
		return;

	// Camera projection position (projection cube goes from 0 to 1)
	Matrix cameraViewMatrix = _sunNode->getViewMatrix();
	Matrix sunViewMatrix = _sunNode->getMatrix();
	Matrix proj = _sunNode->getProjectionMatrix();
	Matrix viewMatrix = cameraViewMatrix * sunViewMatrix;
	Matrix projectionMatrix = _biasMatrix * proj;
	Matrix projectViewMatrix = projectionMatrix * viewMatrix;
	Vector4 sunPos = Vector4(0.f, 0.f, 0.f, 1.f);
	Vector4 sunPosProjTmp = projectViewMatrix * sunPos;
	sunPosProjTmp.x /= sunPosProjTmp.w;
	sunPosProjTmp.y /= sunPosProjTmp.w;
	sunPosProjTmp.z /= sunPosProjTmp.w;

	// Only check if sun has moved within the camera
	if (!(_sunPosProj.x == sunPosProjTmp.x && _sunPosProj.y == sunPosProjTmp.y && _sunPosProj.z == _sunPosProj.z)) {
		_sunPosProj = sunPosProjTmp;
		
		// Compute sun bounding box
		Vector4 sunViewPos = viewMatrix * sunPos;
		// This operation here assumes no scaling has been applied to the sun
		// (otherwise we would need to change this vector back to _sunNode's coordinates and
		// project from there)
		Vector4 sunUp = projectionMatrix * (sunViewPos + Vector4(0.f, _sunRadius, 0.f, 0.f));
		sunUp.x /= sunUp.w;
		sunUp.y /= sunUp.w;
		sunUp.z /= sunUp.w;
		float projR = (Vector3(sunUp.x, sunUp.y, sunUp.z) - Vector3(_sunPosProj.x, _sunPosProj.y, _sunPosProj.z)).length();
		// z is weirder because the radius changes (shorter on the back, longer on the front) because of the projection,
		// but since it will never be a problem, we ignore it
		float sunBBFront = _sunPosProj.z;
		float sunBBBack = _sunPosProj.z;
		float sunBBLeft = _sunPosProj.x - projR;
		float sunBBRight = _sunPosProj.x + projR;
		float sunBBTop = _sunPosProj.y - projR;
		float sunBBBottom = _sunPosProj.y + projR;
		// If bounding box is not onscreen
		_isSunVisible = max(sunBBLeft, 0.0f) <= min(sunBBRight, 1.0f) && max(sunBBFront, 0.0f) <= min(sunBBBack, 1.0f) && max(sunBBTop, 0.0f) <= min(sunBBBottom, 1.0f);
	}

	if (!_isSunVisible)
		return;

	Game::getInstance()->setViewport(Rectangle(_bufferWidth, _bufferHeight));
	// Draw sun
	FrameBuffer *previousFrameBuffer = _frameBuffer->bind();
	_frameBuffer->setRenderTarget(_sunTextures[1]);
	_frameBuffer->setDepthTarget(_sunTextures[4]);
	Game::getInstance()->clear(Game::CLEAR_COLOR_DEPTH, Vector4::zero(), 1.0f, 0);
	_sunNode->getModel()->draw();
	
	// Depth test
	_frameBuffer->setRenderTarget(_sunTextures[0]);
	_frameBuffer->setDepthTarget(NULL);
	_quadModel->setMaterial(_depthTestMaterial);
	_depthTestMaterial->getParameter("u_depthTexture")->setValue(sceneDepth);
	_quadModel->draw();
	
	// Soft blur
	_frameBuffer->setRenderTarget(_sunTextures[2]);
	_quadModel->setMaterial(_horizontalBlurMaterial);
	_quadModel->getMaterial()->getParameter("u_width")->setValue(1);
	_quadModel->getMaterial()->getParameter("u_texture")->setValue(_sunTextureSamplers[0]);
	_quadModel->draw();
	
	_frameBuffer->setRenderTarget(_sunTextures[1]);
	_quadModel->setMaterial(_verticalBlurMaterial);
	_quadModel->getMaterial()->getParameter("u_width")->setValue(1);
	_quadModel->getMaterial()->getParameter("u_texture")->setValue(_sunTextureSamplers[2]);
	_quadModel->draw();
	
	// Heavy blur
	_frameBuffer->setRenderTarget(_sunTextures[3]);
	_quadModel->setMaterial(_horizontalBlurMaterial);
	_quadModel->getMaterial()->getParameter("u_width")->setValue(10);
	_quadModel->getMaterial()->getParameter("u_texture")->setValue(_sunTextureSamplers[0]);
	_quadModel->draw();
	
	_frameBuffer->setRenderTarget(_sunTextures[2]);
	_quadModel->setMaterial(_verticalBlurMaterial);
	_quadModel->getMaterial()->getParameter("u_width")->setValue(10);
	_quadModel->getMaterial()->getParameter("u_texture")->setValue(_sunTextureSamplers[3]);
	_quadModel->draw();
	
	previousFrameBuffer->bind();
	
	Game::getInstance()->setViewport(Rectangle(_sceneWidth, _sceneHeight));
}

void StelSun::renderEffect()
{
	if (!LENSFLARE_ENABLED || !_isSunVisible)
		return;
	
	_quadModel->setMaterial(_lensFlareMaterial);
	_quadModel->getMaterial()->getParameter("u_sunPosProj")->setValue(Vector2(_sunPosProj.x, _sunPosProj.y));
	_quadModel->draw();
}
