#include "StelPelletCln.hpp"
#include "StelClient.hpp"
#include "../StelPrimitives.hpp"

StelPelletCln::StelPelletCln()
{	
}

void StelPelletCln::initialize()
{
	_node = Node::create();
	// Initialize emmitter
	_particleEmitter = ParticleEmitter::create("res/particles/energy.particle");
	// Start the emitter
	_node->setParticleEmitter(_particleEmitter); 
	_particleEmitter->start();
	StelPellet::initialize();
}

StelPelletCln::~StelPelletCln()
{
}

void StelPelletCln::update(float elapsedTime)
{
	StelPellet::update(elapsedTime);
	_particleEmitter->update(elapsedTime);
}

void StelPelletCln::Deserialize(DeserializeParameters *deserializeParameters)
{
	// Nunca se va a llamar
}

bool StelPelletCln::DeserializeConstruction(BitStream *constructionBitstream, Connection_RM3 *sourceConnection)
{
	constructionBitstream->ReadVector(_position.x, _position.y, _position.z);
	RakNet::NetworkID netId;
	constructionBitstream->Read(netId);
	
	StelClient* game = (StelClient*) Game::getInstance();
	initialize();
	game->getUniverse()->spawnPellet(this,netId);
	return true;
}

bool StelPelletCln::DeserializeDestruction(BitStream *destructionBitstream, Connection_RM3 *sourceConnection)
{
	StelClient* game = (StelClient*) Game::getInstance();
	game->getUniverse()->removePellet(this);
	return true;
}
