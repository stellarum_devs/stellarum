#ifndef STELPLAYER_CLN_H_
#define STELPLAYER_CLN_H_

#include "../StelPlayer.hpp"
#include "StelTransformationHistory.hpp"

/**
* Main game class.
*/
class StelPlayerCln : public StelNetObjCln, public StelPlayer
{
	public:
		StelPlayerCln();
		virtual ~StelPlayerCln();
		virtual void initialize();
		virtual void update(float elapsedTime) override final;

		virtual void startCharging();
		virtual void stopCharging();

		Node* getCameraNode() { return _cameraNode; }

		bool _enableInterpolation;
		StelTransformationHistory _transformationHistory;

		void playSoundPositional(const char* sound);
		void playSoundCharge();
		void playSoundLaunch();

		void startUsingObj() override;
		void stopUsingObj() override;

	private:
		Camera* _camera;
		Node* _genkiBallNode;
		ParticleEmitter* _genkiBallPE;

		bool _isBat; //TODO: change to modelID or something
		Node* _colTestNode;
		bool _isUsing;

		AudioSource* _genkiBallLaunchSound;
		AudioSource* _genkiBallChargeSound;
		
		// Replica3
		virtual void Deserialize(DeserializeParameters *deserializeParameters);
		virtual bool DeserializeConstruction(BitStream *constructionBitstream, Connection_RM3 *sourceConnection);
		virtual bool DeserializeDestruction(BitStream *destructionBitstream, Connection_RM3 *sourceConnection);
};

#endif
