#include "StelUI.hpp"

Vector4 StelUI::color_Green = Vector4(0.1f, 0.7f, 0.3f, 1.0f);
Vector4 StelUI::color_Yellow = Vector4(0.9f, 0.85f, 0.1f, 1.0f);
Vector4 StelUI::color_Red = Vector4(0.9f, 0.2f, 0.2f, 1.0f);

StelUI::StelUI() : _uiOverlay(NULL), _maxNumDbgMsgs(5), _currActiveDbgMsgs(0), _timeVisible(2000.f), _drawEnergy(false), _energyValue(0.0f), _energyInUse(0.0f), _teamId(0)
{
	_uiOverlay = Form::create("res/ui/uiOverlay.form");
	_uiOverlay->setVisible(false);
	Label* label;
	Container* msgContainer = static_cast<Container*>(_uiOverlay->getControl("msgContainer"));
	for (int i = 0; i < _maxNumDbgMsgs; i++)
	{
		label = Label::create("msgLabel");
		//label->setVisible(false);
		msgContainer->addControl(label);
		_dbgMsgLabelList.push_back(label);
		label->release(); //_uiOverlay still keeps a reference
	}

	// Load font, sprites
	_spriteBatch = SpriteBatch::create("res/ui/uipackSpace_sheet_metal.png");
	_font = Font::create("res/fonts/arial.gpb");
}

StelUI::~StelUI()
{
	SAFE_RELEASE(_uiOverlay);
}

void StelUI::showDebugMsg(std::string message, Vector3 textColour)
{
	Vector4 color = Vector4(textColour.x, textColour.y, textColour.z, 1.f);
	StelUI::showDebugMsg(message, color);
}

void StelUI::showDebugMsg(std::string message, Vector4 textColour)
{
	Label* lbl;
	// Save the message in the log
	_dbgMsgLogQueue.push(message);

	float from[] = { message.size() * -6.f };
	float to[] = { 0.f };

	lbl = _dbgMsgLabelList[0];
	std::string newTextValue = lbl->getText();
	Vector4 newColorValue = lbl->getTextColor();

	lbl->setOpacity(1.0f);
	lbl->setText(message.c_str());
	lbl->setTextColor(textColour);
	lbl->createAnimationFromTo("dbgMsgSlideIn", Form::ANIMATE_POSITION_X,
		from, to, Curve::LINEAR, 400)->getClip()->play();

	if (_currActiveDbgMsgs == 0)
	{
		_uiOverlay->setVisible(true);
		//lbl->setVisible(true);
	}

	if (_currActiveDbgMsgs < _maxNumDbgMsgs - 1)
	{
		++_currActiveDbgMsgs;
		//_dbgMsgLabelList[++_currActiveDbgMsgs]->setVisible(true);
	}

	std::string oldTextValue;
	Vector4 oldColorValue;
	for (int i = 1; i <= _currActiveDbgMsgs; i++)
	{
		lbl = _dbgMsgLabelList[i];
		oldTextValue = lbl->getText();
		oldColorValue = lbl->getTextColor();
		lbl->setOpacity(1.0f);
		lbl->setText(newTextValue.c_str());
		lbl->setTextColor(newColorValue);
		newTextValue = oldTextValue;
		newColorValue = oldColorValue;
	}

	// Add more time to fade msgs
	_fadeTimer = _timeVisible * 2;
}

void StelUI::updateDbgMessages(float elapsedTime)
{
	if (_currActiveDbgMsgs > 0)
	{
		_fadeTimer -= elapsedTime;

		if (_fadeTimer < 0)
		{
			Label* lbl;
			float from[] = { 1.f };
			float to[] = { 0.f };

			lbl = _dbgMsgLabelList[_currActiveDbgMsgs--];

			lbl->createAnimationFromTo("dbgMsgFadeOut", Form::ANIMATE_OPACITY,
				from, to, Curve::LINEAR, 1500.f)->getClip()->play();

			_fadeTimer = _timeVisible;
		}
	}
}

void StelUI::render(float elapsedTime)
{
	// Draw UI
	_uiOverlay->draw();

	if(_drawEnergy)
		drawEnergySlots();

	if (_drawEndGame)
		drawGameEndMsg();
}

void StelUI::update(float elapsedTime)
{
	/*_uiOverlay->update(elapsedTime);
	if (_fadeTimer > 0)
	{
		_fadeTimer -= elapsedTime;
		if (_fadeTimer <= 0)
		{
			_dbgMsgLabelList.front()->setVisible(false);
			//updateDbgMessages();
		}
	}		*/

	updateDbgMessages(elapsedTime);
}

void StelUI::drawEnergySlots()
{
	//TODO atributos de UI luego
	
	Vector4 color = _teamId == 1 ? Vector4(0.75f, 0.2f, 0.1f, 1.0f) : Vector4(0.1f, 0.2f, 0.75f, 1.0f) ;
	Vector4 colorInc =  _teamId == 1 ? Vector4(0.2f,0.12f,0.1f,1.0f) : Vector4(0.1f, 0.12f, 0.2f, 1.0f);
	int slotsFull = int(_energyValue / 10) % 10;
	float lastPercent =(int(_energyValue)%10) / 10.0;
	
	Rectangle squareEmpty(380, 236, 19, 26);
	Rectangle squareFull(384, 394, 19, 26);

	Rectangle glassPanel(0, 0, 100, 100);

	_spriteBatch->start();
	Rectangle dst(860, 630, 278, 35);
	_spriteBatch->draw(dst, glassPanel, Vector4(1, 1, 1, 0.5f));

	dst = Rectangle(875, 635, 19, 26);

	for(int i = 0; i < slotsFull; i++) {
		_spriteBatch->draw(dst, squareFull,color);
		dst.x += 25;
		color += colorInc;
	}
	//semi-filled one
	Rectangle dst_s = dst;
	dst_s.width = dst_s.width * lastPercent;
	_spriteBatch->draw(dst_s, squareFull,color);
	dst.x += 25;
	color += colorInc;
	
	for(int i = 0 ; i < 10-slotsFull+1; i++) {
		_spriteBatch->draw(dst, squareEmpty,Vector4(0.3f,0.3f,0.3f,1.0f));
		dst.x += 25;
	}
	//TODO refactorizar un pelin
	color =   Vector4(0.0f, 1.0f, 0.2f, 0.5f) ;
	colorInc =  Vector4(0.1f, 0.1f, 0.1f, 0.0f);
	slotsFull = int(_energyInUse / 10) % 10;
	lastPercent =(int(_energyInUse)%10) / 10.0;
	
	squareEmpty= Rectangle(380, 236, 19, 26);
	squareFull= Rectangle(384, 394, 19, 26);
	
	glassPanel= Rectangle(0, 0, 100, 100);
	
	dst= Rectangle(860, 630, 278, 35);
	_spriteBatch->draw(dst, glassPanel, Vector4(1, 1, 1, 0.5f));
	
	dst = Rectangle(875, 635, 19, 26);
	
	for(int i = 0; i < slotsFull; i++) {
		_spriteBatch->draw(dst, squareFull,color);
		dst.x += 25;
		color += colorInc;
	}
	//semi-filled one
	dst_s = dst;
	dst_s.width = dst_s.width * lastPercent;
	_spriteBatch->draw(dst_s, squareFull,color);
	
	
	_spriteBatch->finish();
}

void StelUI::showEndGameMsg(StelTeamMgr::TeamInfo winnerTeam)
{
	_winnerTName = "Team " + winnerTeam._teamName + " wins the game!";
	_winnerTColor = Vector4(winnerTeam._teamColor.x, winnerTeam._teamColor.y,
		winnerTeam._teamColor.z, 1.0f);
	_drawEndGame = true;
}

void StelUI::drawGameEndMsg()
{
	_font->start();
	_font->drawText(_winnerTName.c_str(), 250, 350, _winnerTColor, 30);
	_font->finish();
}
