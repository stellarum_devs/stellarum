#ifndef STELGHOSTCONTROLLER_H_
#define STELGHOSTCONTROLLER_H_

#include "gameplay.h"
#include "StelFPSCamera.hpp"

static const unsigned int MOVE_FORWARD	= (1 << 0);
static const unsigned int MOVE_BACKWARD = (1 << 1);
static const unsigned int MOVE_LEFT		= (1 << 2);
static const unsigned int MOVE_RIGHT	= (1 << 3);
static const unsigned int MOVE_UP		= (1 << 4);
static const unsigned int MOVE_DOWN		= (1 << 5);

using namespace gameplay;

class StelGhostController
{
	public:
		StelGhostController();
		~StelGhostController();

		void initialize();
		void setPosLookAt(Vector3 pos, Vector3 lookAt);

		void update(float elapsedTime);

		void keyEvent(Keyboard::KeyEvent evt, int key);
		bool mouseEvent(Mouse::MouseEvent evt, int x, int y, int wheelDelta);
		void touchEvent(Touch::TouchEvent evt, int x, int y, unsigned int contactIndex);

		inline Camera* getCamera(){ return _fpsCamera->getCamera(); };


		/**
		* Compositing blitter with a specified material/technique applied from a source buffer into the destination buffer.
		*
		* If destination buffer is NULL then it composites to the default frame buffer.
		*
		* Requried uniforms:
		* sampler2d u_texture - The input texture sampler
		*/
		class Compositor
		{
		public:

			static Compositor* create(FrameBuffer* srcBuffer, FrameBuffer* dstBuffer, const char* materialPath, const char* techniqueId);

			~Compositor();

			FrameBuffer* getSrcFrameBuffer() const;

			FrameBuffer* getDstFrameBuffer() const;

			const char* getTechniqueId() const;

			Material* getMaterial() const;

			void blit(const gameplay::Rectangle& dst);

		private:

			Compositor();

			Compositor(FrameBuffer* srcBuffer, FrameBuffer* dstBuffer, Material* material, const char* techniqueId);

			FrameBuffer* _srcBuffer;
			FrameBuffer* _dstBuffer;
			Material* _material;
			const char* _techniqueId;
		};


	private:
		void updateInput(float elapsedTime);
		StelFPSCamera* _fpsCamera;
		Scene* _scene;
		int _prevX, _prevY;
		float _moveSpeed, _upDownSpeed, _turboMove;
		unsigned int _keyFlags;
};

#endif
