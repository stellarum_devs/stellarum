#ifndef STELNETCUBE_CLN_H_
#define STELNETCUBE_CLN_H_

#include "../StelNetCube.hpp"
#include "StelNetworkCln.hpp"
#include "StelTransformationHistory.hpp"

class StelNetCubeCln : public StelNetCube, public StelNetObjCln
{
public:
	StelNetCubeCln();
	~StelNetCubeCln();

	void update(float elapsedTime);
	
	// Visible position is where we are interpolating at, which is behind the real position
	Vector3 _visiblePosition;
	Quaternion _visibleRotation;

	// Gameplay node
	Node* _netCubeNode;

private:
	// Network
	bool _enableInterpolation;
	StelTransformationHistory _transformationHistory;

	// Replica3
	void Deserialize(DeserializeParameters *deserializeParameters);
	bool DeserializeConstruction(BitStream *constructionBitstream, Connection_RM3 *sourceConnection){ return true; }
	bool DeserializeDestruction(BitStream *destructionBitstream, Connection_RM3 *sourceConnection){ return true; }
	void DeallocReplica(Connection_RM3 *sourceConnection){ delete this; };
};

#endif
