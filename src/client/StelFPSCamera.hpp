#ifndef STELFPSCAMERA_H_
#define STELFPSCAMERA_H_

#include "gameplay.h"

using namespace gameplay;

/**
 * StelFPSCamera controls a camera like a first person shooter game.
 */
class StelFPSCamera
{
public:
    StelFPSCamera();
    ~StelFPSCamera();

	// Initializes the first person camera. Should be called after the Game has been initialized.
    void initialize(float nearPlane = 1.0f, float farPlane = 100000.0f, float fov = 45.0f);

	// Gets root node. May be NULL if not initialized.
    Node* getRootNode();

	// Gets the camera. May be NULL.
    Camera* getCamera();

	// Sets the position of the camera.
    void setPosition(const Vector3& position);

	// Moves the camera forward in the direction that it is pointing. (Fly mode)
    void moveForward(float amount);

    // Moves the camera in the opposite direction that it is pointing.
    void moveBackward(float amount);

    // Strafes that camera left or right, which is perpendicular to the direction it is facing.    
    void moveLeft(float amount);
    void moveRight(float amount);
    void moveUp(float amount);
    void moveDown(float amount);

    /**
     * Rotates the camera in place in order to change the direction it is looking.
     * 
     * @param yaw Rotates the camera around the yaw axis in radians. Positive looks right, negative looks left.
     * @param pitch Rotates the camera around the ptich axis in radians. Positive looks up, negative looks down.
     */
    void rotate(float yaw, float pitch);

	void lookAt(Vector3 target);

private:

    Node* _pitchNode;
    Node* _rootNode;
};

#endif
