#include "StelGenkiBallCln.hpp"
#include "StelClient.hpp"

StelGenkiBallCln::StelGenkiBallCln()
{
}

void StelGenkiBallCln::initialize()
{
	StelGenkiBall::initialize(_position, _velocity, _chargeTime);
	float radius = _energy * radiusRatio;

	// Set a material to the sphere so we can see it
	_genkiBallNode->getModel()->setMaterial("res/materials/genkiball.material#genkiball");

	// Initialize emmitter
	ParticleEmitter* emitter = ParticleEmitter::create("res/particles/genkiBall.particle");
	emitter->setSize(radius, radius, radius * 2, radius * 2);
	//Color tweaks
	Vector4 color = Vector4(radius * 0.02, 0.0, radius * 0.01, 0.1f); // TODO: sacar color de radius (alguna func)
	emitter->setColor(emitter->getColorStart(), emitter->getColorStartVariance(), color, emitter->getColorEndVariance());

	// Start the emitter
	_genkiBallNode->setParticleEmitter(emitter); // _genkiBallNode keeps a reference
	emitter->start();

	_genkiBallNode->setCollisionObject(PhysicsCollisionObject::GHOST_OBJECT, PhysicsCollisionShape::sphere(_energy * radiusRatio)); //TODO remove when done debugging
	
	_genkiBallNode->setScale(_energy * radiusRatio);

	// Sound Fx
	AudioSource* travelSound = AudioSource::create("res/audio/beamhead.ogg");
	if (travelSound)
	{
		travelSound->setGain(25.0f);
		travelSound->setLooped(true);
		travelSound->play();
	}
	_genkiBallNode->setAudioSource(travelSound);
	travelSound->release();
}

StelGenkiBallCln::~StelGenkiBallCln()
{
}

void StelGenkiBallCln::update(float elapsedTime)
{
	StelGenkiBall::update(elapsedTime);
	
	_genkiBallNode->getParticleEmitter()->update(elapsedTime);
}

void StelGenkiBallCln::charge(ParticleEmitter* pe, float elapsedTime)
{
	// TODO Change particle scale

	//Color tweaks
	Vector4 newColorEnd = pe->getColorEnd();
	newColorEnd.x += elapsedTime * .0001f;
	newColorEnd.z += elapsedTime * .00001f;
	pe->setColor(pe->getColorStart(), pe->getColorStartVariance(), newColorEnd, pe->getColorEndVariance());
}

void StelGenkiBallCln::Deserialize(DeserializeParameters *deserializeParameters)
{
	// Nunca se va a llamar
}

bool StelGenkiBallCln::DeserializeConstruction(BitStream *constructionBitstream, Connection_RM3 *sourceConnection)
{
	constructionBitstream->ReadVector(_position.x, _position.y, _position.z);
	constructionBitstream->ReadVector(_velocity.x, _velocity.y, _velocity.z);
	constructionBitstream->ReadAlignedBytes((unsigned char*)&_chargeTime, sizeof(_chargeTime));
	//Maybe not the cleanest way
	StelClient* game = (StelClient*) Game::getInstance();
	initialize();
	game->getUniverse()->spawnGenkiBall(this);
	
	return true;
}

bool StelGenkiBallCln::DeserializeDestruction(BitStream *destructionBitstream, Connection_RM3 *sourceConnection)
{
	//Maybe not the cleanest way
	StelClient* game = (StelClient*) Game::getInstance();

	// Spawn explosion gfx sfx mammoth
	game->getUniverse()->partyMgr.addExplosion(_genkiBallNode->getTranslationWorld(), false);

	// Remove genki from scene ~ and hopefully everywhere
	game->getUniverse()->removeGenkiBall(this);

	return true;
}
