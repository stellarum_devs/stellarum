#ifndef __STELEFFECT_H__
#define __STELEFFECT_H__

#include "gameplay.h"
using namespace gameplay;

class StelEffect
{
public:
	StelEffect() {};
	~StelEffect() {};
	
	virtual void computeEffect(Texture::Sampler* sceneColor, Texture::Sampler* sceneDepth) = 0;
	virtual void renderEffect() = 0;
};

#endif
