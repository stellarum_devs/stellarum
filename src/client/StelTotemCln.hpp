#ifndef STELTOTEM_CLN_H_
#define STELTOTEM_CLN_H_

#include "../StelTotem.hpp"
#include "StelPlanetCln.hpp"

class StelTotemCln : public StelNetObjCln, public StelTotem
{
public:
	StelTotemCln();
	StelTotemCln(StelCaptureMode mode, StelPlanetCln* parentPlanetNode);
	~StelTotemCln();

	void update(float elapsedTime);
	void capChanged(StelTeamID id, StelCaptureMode mode);

	// Usable interface
	void startUsing(void* who){}
	void stopUsing(){}
	void lostUser(){}

protected:
	Node* _progNode;
	float _progRadius;
	float _progAngle;
	
private:

	// Replica3
	void Deserialize(DeserializeParameters *deserializeParameters);
	bool DeserializeConstruction(BitStream *constructionBitstream, Connection_RM3 *sourceConnection);
	bool DeserializeDestruction(BitStream *destructionBitstream, Connection_RM3 *sourceConnection);
	
};

#endif
