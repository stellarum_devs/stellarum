#ifndef STELCLIENT_H_
#define STELCLIENT_H_

#include "gameplay.h"
#include "StelUI.hpp"
#include "StelUniverseCln.hpp"
#include "StelPlayerCln.hpp"
#include "StelLocalController.hpp"
#include "StelGhostController.hpp"
#include "StelNetworkCln.hpp"
#include "../StelNetSetup.hpp"

using namespace gameplay;

/**
 * Stellarum main game class.
 */
class StelClient : public Game, public Control::Listener, public StelPackageListener
{
public:

	StelClient();
	~StelClient();

	void keyEvent(Keyboard::KeyEvent evt, int key);
	void touchEvent(Touch::TouchEvent evt, int x, int y, unsigned int contactIndex);
	bool mouseEvent(Mouse::MouseEvent evt, int x, int y, int wheelDelta);
	void gamepadEvent(Gamepad::GamepadEvent evt, Gamepad* gamepad);
	void controlEvent(Control* control, EventType evt);
	
	bool wireframeEnabled() { return _wireframe; }
	bool scoutEnabled() { return _scout;};
	bool ghostPlayerEnabled() { return !_usePlayer; }
	int debugMode() { return _drawDebug; }

	StelUniverseCln* getUniverse() {return _universe;}
	void addController(uint64_t guid, std::string name){/*SERVER ONLY*/ }
	void removeController(uint64_t guid){/*SERVER ONLY*/ }
	StelController* getController(uint64_t guid) { return _playerControllerMap[guid]; }
	inline StelLocalController* getActivePlayerController() { return _playerController; }
	
	void spawnPlayer(StelPlayerCln* player);
	void spawnGenkiBall(StelGenkiBall* genkiBall);
	void processCmds(ControllerCmdsPkg* cmdPkg){/*SERVER ONLY*/}
	void processTeamInfo(TeamInfoPkg* teamInfoPkg);
	void updateGameState(StelGameStateChangePkg* gamestatePkg);
	void togglePlayer();
	
protected:

	void initialize();
	void finalize();
	void update(float elapsedTime);
	void render(float elapsedTime);

private:
	void drawValue(Font* font, const Vector4& color, unsigned int x, unsigned int y, unsigned int value, const char* formatStr);


	
	void processNetQueue();

	StelUniverseCln* _universe;
	StelLocalController* _playerController;
	StelGhostController* _ghostController;
	
	std::map<uint64_t, StelLocalController*> _playerControllerMap;

	Font* _font;
	bool _wireframe;
	bool _scout;
	bool _usePlayer; // switch between player and ghost cam

	// User Interface
	StelUI* _ui;
	bool _menuVisible;
	bool _uiIsLocalhost;
	Form* _menuForm;

	// Physics stuff
	std::vector<const char*> _collisionObjectPaths;
	int _drawDebug;

	// Network stuff
	StelNetwork* _network;
	float cmd_time;
	
	//Game state
	StelGameState _gameState;
};


#endif
