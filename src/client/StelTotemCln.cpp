#include "StelTotemCln.hpp"
#include "StelClient.hpp"
#include "../StelPrimitives.hpp"

StelTotemCln::StelTotemCln() :
_progAngle(0.f),
_progRadius(TOTEM_CAPTURE_TIME* 0.001f),
_progNode(NULL)
{
	// Load totem mesh, phys
	Bundle* bundle = Bundle::create("res/models/totem.gpb");
	GP_ASSERT(bundle);
	_node = bundle->loadNode("StoneLantern");
	SAFE_RELEASE(bundle);

	_node->setCollisionObject(PhysicsCollisionObject::GHOST_OBJECT, PhysicsCollisionShape::box());
	
	// proggress indicator
	_progNode = Node::create("TotemProgNode");
	_progNode->setModel(Model::create(createCubeMesh(0.5f)));
	_progNode->getModel()->setMaterial("res/materials/unlit.material#unlit_orange");
	_node->addChild(_progNode);

	AudioSource* captureSound = AudioSource::create("res/audio/mine_now.ogg");
	if (captureSound)
	{
		captureSound->setGain(25.0f);
		captureSound->setLooped(false);
	}
	_node->setAudioSource(captureSound);
	captureSound->release();

	StelTotem::initialize();
}

StelTotemCln::~StelTotemCln()
{
}

void StelTotemCln::update(float elapsedTime)
{
	_progAngle += elapsedTime*0.1f;
	if (_progAngle > 360.f)
		_progAngle -= 360.f;

	_progRadius = (TOTEM_CAPTURE_TIME * 0.001f) - (_captureTime * 0.001f);
	_progNode->setTranslationX(sin(MATH_DEG_TO_RAD(_progAngle))* _progRadius);
	_progNode->setTranslationY(cos(MATH_DEG_TO_RAD(_progAngle))* _progRadius);
}

void StelTotemCln::capChanged(StelTeamID id, StelCaptureMode mode)
{
	// TODO: proper material with settable color: StelTeamMgr->getTeamInfo(id) ...
	if(id == UNASSIGNED_TEAMID)
		_progNode->getModel()->setMaterial("res/materials/unlit.material#unlit_orange");
	else if (id == 0)
		_progNode->getModel()->setMaterial("res/materials/unlit.material#unlit_blue");
	else 
		_progNode->getModel()->setMaterial("res/materials/unlit.material#unlit_red");

	// play capture sound
	if (id != UNASSIGNED_TEAMID && mode == _mode)
		_node->getAudioSource()->play();
}

bool StelTotemCln::DeserializeConstruction(BitStream *constructionBitstream, Connection_RM3 *sourceConnection)
{
	constructionBitstream->Read(_mode);

	RakNet::NetworkID netId;
	constructionBitstream->Read(netId);

	StelClient* game = (StelClient*)Game::getInstance();

	// find our planet and add our crap
	for (StelPlanet* p : *game->getUniverse()->getPlanets()) {
		if (p->GetNetworkID() == netId) {
			p->getNode()->addChild(_node);
			initPosition(); // must do this after addChild because transforms are reset
			if (_mode == MODE_ATTACK)
				_node->getModel()->setMaterial("res/materials/unlit.material#unlit_red");
			else if (_mode == MODE_ENERGIZE)
				_node->getModel()->setMaterial("res/materials/unlit.material#unlit_green");
			else
				_node->getModel()->setMaterial("res/materials/unlit.material#unlit_blue");
			p->setTotem(_mode, this);
			break;
		}
	}
	return true;
}

void StelTotemCln::Deserialize(DeserializeParameters *deserializeParameters)
{
	deserializeParameters->serializationBitstream[0].Read(_inUse);
	deserializeParameters->serializationBitstream[0].Read(_capturingTeamID);
	deserializeParameters->serializationBitstream[0].Read(_captureTime);
}

bool StelTotemCln::DeserializeDestruction(BitStream *destructionBitstream, Connection_RM3 *sourceConnection)
{
	return true;
}
