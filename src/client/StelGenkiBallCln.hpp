#ifndef STELGENKIBALL_CLN_H_
#define STELGENKIBALL_CLN_H_

#include "../StelGenkiBall.hpp"

class StelGenkiBallCln : public StelNetObjCln, public StelGenkiBall
{
public:
	StelGenkiBallCln();
	~StelGenkiBallCln();

	void initialize();

	void update(float elapsedTime);
	
	static void charge(ParticleEmitter* pe, float elapsedTime);
	
private:

	// Replica3
	void Deserialize(DeserializeParameters *deserializeParameters);
	bool DeserializeConstruction(BitStream *constructionBitstream, Connection_RM3 *sourceConnection);
	bool DeserializeDestruction(BitStream *destructionBitstream, Connection_RM3 *sourceConnection);
	
protected:
	
};

#endif
