#ifndef STELUI_H_
#define STELUI_H_

#include "gameplay.h"
#include "../StelDefines.hpp"

using namespace gameplay;

class StelUI
{
public:

	void update(float elapsedTime);
	void render(float elapsedTime);

	void showDebugMsg(std::string message, Vector3 textColor);
	void showDebugMsg(std::string message, Vector4 textColor = Vector4(1.f, 1.f, 1.f, 1.f));

	static Vector4 color_Green, color_Yellow, color_Red;

	void drawEnergySlots();
	void showEndGameMsg(StelTeamMgr::TeamInfo winnerTeam);

	// SINGLETON
	static StelUI& getInstance()
	{
		static StelUI instance; // Guaranteed to be destroyed.                     
		return instance; // Instantiated on first use.
	}
	void setTeam(StelTeamID team) {_teamId = team;}
	void enableDrawEnergy(bool b) {_drawEnergy = b;}
	void setEnergyValue(float energy, float energyInUse) {
		_energyValue = energy; _energyInUse = energyInUse;
	}

private:

	// Debug msgs stuff
	int _maxNumDbgMsgs;
	int _currActiveDbgMsgs;
	float _fadeTimer;
	float _timeVisible;

	void updateDbgMessages(float elapsedTime);
	std::vector<Label*> _dbgMsgLabelList;
	std::queue<std::string> _dbgMsgLogQueue; // do it like strings internally?

	// Spritebatch for drawing imgs
	SpriteBatch* _spriteBatch;

	// Font for writing crap
	Font* _font;
	
	// Forms stuff
	Form* _uiOverlay;
	
	// end game msg TODO make this nicer
	void drawGameEndMsg();
	bool _drawEndGame;
	std::string _winnerTName;
	Vector4 _winnerTColor;
	
	//Energy Stuff
	bool _drawEnergy;
	float _energyValue;
	float _energyInUse;
	StelTeamID _teamId;


	// Singleton stuff
	StelUI();
	~StelUI();
	// Dont forget to declare these two. You want to make sure they
	// are unaccessable otherwise you may accidently get copies of
	// your singleton appearing.
	StelUI(StelUI const&);    // Don't Implement
	void operator=(StelUI const&); // Don't implement
};

#endif
