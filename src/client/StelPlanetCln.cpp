#include "StelPlanetCln.hpp"
#include "StelClient.hpp"
#include "../StelPrimitives.hpp"
#include "StelTotemCln.hpp"

StelPlanetCln::StelPlanetCln()
{
	// Buffer up for 500 mseconds if we were to get 1 update per 50 msecond
	_transformationHistory.init(25, 500);
}

StelPlanetCln::~StelPlanetCln()
{
}

void StelPlanetCln::initialize()
{
	StelPlanet::initialize();
	_node->getModel()->setMaterial(_materialPath.c_str());
	_turretNode->getModel()->setMaterial("res/materials/arrowkun.material#arrowkun");
}

void StelPlanetCln::update(float elapsedTime)
{
	//StelPlanet::update(elapsedTime);

	_transformationHistory.read(&_position, NULL, &_rotation, GetTimeMS() - DEFAULT_SERVER_MILLISECONDS_BETWEEN_UPDATES, GetTimeMS());
	_node->setTranslation(_position);
	_node->setRotation(_rotation);

	for (unsigned int t = 0; t < NUM_MODES - 1; t++)
	{
		_totems[t]->update(elapsedTime);
	}
}

void StelPlanetCln::Deserialize(DeserializeParameters *deserializeParameters)
{
	//deserializeParameters->serializationBitstream[0].ReadFloat16(_angle, 0.0f, 7.0f); // 0..2pi;

	deserializeParameters->serializationBitstream[0].ReadAlignedBytes((unsigned char*)&_position, sizeof(_position));
	deserializeParameters->serializationBitstream[0].ReadAlignedBytes((unsigned char*)&_rotation, sizeof(_rotation));
	
	// if capTeamID changed
	if (deserializeParameters->serializationBitstream[0].ReadBit())
	{
		deserializeParameters->serializationBitstream[0].Read(_captureTeamID);
		deserializeParameters->serializationBitstream[0].Read(_captureMode);
		for (StelTotem* totem : _totems)
		{
			((StelTotemCln*)totem)->capChanged(_captureTeamID, _captureMode);
		}

		// Change orbit colour // todo get colour from TeamMgr
		if (_captureTeamID == 0)
			_orbitNode->getModel()->setMaterial("res/materials/unlit.material#unlit_blue");
		else if (_captureTeamID == 1)
			_orbitNode->getModel()->setMaterial("res/materials/unlit.material#unlit_red");			
	}

	// Every time we get a network packet, we write it to the transformation history class.
	// This class, given a time in the past, can then return to us an interpolated position of where we should be in at that time
	_transformationHistory.write(_position, Vector3::zero(), _rotation, RakNet::GetTimeMS());
}

bool StelPlanetCln::DeserializeConstruction(BitStream *bs, Connection_RM3 *sourceConnection)
{
	bs->Read(_angle);
	bs->Read(_sphereRadius);
	RakNet::RakString rs;
	bs->Read(rs);
	_materialPath = std::string(rs.C_String());
	bs->Read(_orbitCenter);
	bs->Read(_orbitRadius);
	bs->Read(_axis);
	bs->Read( _velocity);
	bs->Read( _rotationVelocity);
	bs->Read(_gravityRadius);
	initialize();
	StelClient* game = (StelClient*) Game::getInstance();
	game->getUniverse()->spawnPlanet(this);
	
	return true;
}

bool StelPlanetCln::DeserializeDestruction(BitStream *destructionBitstream, Connection_RM3 *sourceConnection)
{
	return true;
}


Model* StelPlanetCln::createOrbitLineModel(unsigned int numVertices)
{
	std::vector<float> vertices;
	unsigned int verticesSize = numVertices * 6;
	vertices.resize(verticesSize);

	float phi = 0;
	float step = MATH_PIX2 / numVertices;
	for (unsigned int i = 0; i < verticesSize; ++i)
	{
		Vector4 color(0.15f, 0.15f, 0.15f, 1.0f);	
	
		Vector3 point;
		
		Matrix m; 
        Vector3 z = Vector3(0,0,1);
        rotationAlign(_axis,z,&m);
        point = Vector3(_orbitRadius*cos(phi),_orbitRadius*sin(phi),0);
		m.transformVector(&point);

		vertices[i] = point.x;
		vertices[++i] = point.y;
		vertices[++i] = point.z;
		
		vertices[++i] = color.x;
		vertices[++i] = color.y;
		vertices[++i] = color.z;
		phi+=step;
	}
		
	VertexFormat::Element elements[] =
	{
		VertexFormat::Element(VertexFormat::POSITION, 3),
		VertexFormat::Element(VertexFormat::COLOR, 3)
	};
	Mesh* mesh = Mesh::createMesh(VertexFormat(elements, 2), numVertices, false);
	if (mesh == NULL)
	{
		return NULL;
	}
	mesh->setPrimitiveType(Mesh::LINES);
	mesh->setVertexData(&vertices[0], 0, numVertices);
	
	if (!mesh)
		return NULL;
	
	mesh->setBoundingBox(BoundingBox(Vector3(-_orbitRadius, -_orbitRadius, -_orbitRadius), Vector3(_orbitRadius, _orbitRadius, _orbitRadius))); // todo
	mesh->setBoundingSphere(BoundingSphere(Vector3::zero(), _orbitRadius));

	Model* model = Model::create(mesh);
	mesh->release();
	assert(model);
	model->setMaterial("res/materials/grid.material");
	return model;
}

