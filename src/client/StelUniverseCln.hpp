#ifndef STELUNIVERSE_CLN_H_
#define STELUNIVERSE_CLN_H_

#include "gameplay.h"
#include "../StelUniverse.hpp"
#include "StelPlanetCln.hpp"
#include "StelParticleMgr.hpp"
#include "StelDebugDrawer.hpp"

class StelGenkiBallCln;
class StelPlayerCln;
class StelSun;
class StelPelletCln;

using namespace gameplay;

/**
 * Stellarum main game class.
 */
class StelUniverseCln : public StelUniverse
{
public:

	StelUniverseCln();
	~StelUniverseCln();
	void initialize();
	void finalize();
	
	void update(float elapsedTime);
	
	 /**
	 * Renders the current scene (nodes and whatnot) onto the render target
	 */
	void render( float elapsedTime, int debug_mode, bool wireframe, bool scout);
	
	/**
	 * Renders the final scene (stored in the render target) onto the screen,
	 * applying any effects needed
	 */
	void commitRender(float elapsedTime);
	
	inline Scene* getScene(){ return _scene; };
	inline FrameBuffer *getSceneBuffer() { return _sceneFrameBuffer; }
	inline Node* getMainLight() {return _lightNode;}
	
	/* called by game , nework stuff */
	void spawnPlanet(StelPlanetCln* planet);
	void spawnPellet(StelPelletCln* pellet, NetworkID netId);

	void removePellet(StelPellet* pellet);
	void removeGenkiBall(StelGenkiBall* genki);

	// Special particles
	StelParticleMgr partyMgr;
	
	void setDarkSun(bool b);
	
protected:

	//internal methods
	//TODO, for now the parent class does everything
	//void initializePlanets();
	bool drawScene(Node* node,bool wireframe);
	
	//Scout stuff
	StelDebugDrawer _ddrawer;

	// Sun effects
	Model* _sceneQuadModel;
	FrameBuffer* _sceneFrameBuffer;
	Texture::Sampler* _sceneColorSampler;
	Texture::Sampler* _sceneDepthSampler;
	
	Node* _lightNode;
	Node* _lightNodeBase1;
	Node* _lightNodeBase2;
	StelSun* _sun;
	bool _darkSun;
	
};

#endif
