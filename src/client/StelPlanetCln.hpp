#ifndef STELPLANET_CLN_H_
#define STELPLANET_CLN_H_

#include "../StelPlanet.hpp"
#include "StelTransformationHistory.hpp"

class StelPlanetCln : public StelNetObjCln, public StelPlanet
{
public:
	StelPlanetCln();
	~StelPlanetCln();


	void update(float elapsedTime);
	
	//Create a model based on this planet's orbit
	Model* createOrbitLineModel(unsigned int numVertices);
	inline void setOrbitNode(Node* orbitNode) { _orbitNode = orbitNode; }

protected:
	void initialize();

	StelTransformationHistory _transformationHistory;
	Node* _orbitNode;

private:

	// Replica3
	void Deserialize(DeserializeParameters *deserializeParameters);
	bool DeserializeConstruction(BitStream *constructionBitstream, Connection_RM3 *sourceConnection);
	bool DeserializeDestruction(BitStream *destructionBitstream, Connection_RM3 *sourceConnection);
	
};

#endif
