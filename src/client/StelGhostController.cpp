#include "StelGhostController.hpp"

StelGhostController::StelGhostController() 
	: _fpsCamera(NULL), _moveSpeed(100.0f), _upDownSpeed(90.0), _turboMove(200.0f), _keyFlags(0)
{
}

StelGhostController::~StelGhostController()
{
	SAFE_DELETE(_fpsCamera); // puede q sea mejor usar SAFE_RELEASE
}

void StelGhostController::initialize()
{
	_scene = Scene::getScene();

	// Camera -----------------------------------

	// Load camera script
	//Game::getInstance()->getScriptController()->loadScript("res/camera.lua");
	_fpsCamera = new StelFPSCamera();
	_fpsCamera->initialize();
	_scene->addNode(_fpsCamera->getRootNode());
	_scene->setActiveCamera(_fpsCamera->getCamera());
	//_fpCamera.rotate(MATH_DEG_TO_RAD(180), 0);    ESTO JODE MAZO LAS LUCES no se porq
}

void StelGhostController::update(float elapsedTime)
{
	//Game::getInstance()->getScriptController()->executeFunction<void>("camera_update", "f", elapsedTime);
	updateInput(elapsedTime);
}

void StelGhostController::setPosLookAt(Vector3 pos, Vector3 lookAt)
{
	_fpsCamera->setPosition(pos);
	_fpsCamera->lookAt(lookAt);
}

void StelGhostController::keyEvent(Keyboard::KeyEvent evt, int key)
{
	if (evt == Keyboard::KEY_PRESS) {
		switch (key) {
		case Keyboard::KEY_W:
			_keyFlags |= MOVE_FORWARD;
			break;
		case Keyboard::KEY_S:
			_keyFlags |= MOVE_BACKWARD;
			break;
		case Keyboard::KEY_A:
			_keyFlags |= MOVE_LEFT;
			break;
		case Keyboard::KEY_D:
			_keyFlags |= MOVE_RIGHT;
			break;

		case Keyboard::KEY_Q:
			_keyFlags |= MOVE_DOWN;
			break;
		case Keyboard::KEY_E:
			_keyFlags |= MOVE_UP;
			break;
		case Keyboard::KEY_PG_UP:
			_fpsCamera->rotate(0, MATH_PIOVER4);
			break;
		case Keyboard::KEY_PG_DOWN:
			_fpsCamera->rotate(0, -MATH_PIOVER4);
			break;
		case Keyboard::KEY_SHIFT:
			_moveSpeed += _turboMove;
			break;
		}
	}
	else if (evt == Keyboard::KEY_RELEASE) {
		switch (key) {
		case Keyboard::KEY_W:
			_keyFlags &= ~MOVE_FORWARD;
			break;
		case Keyboard::KEY_S:
			_keyFlags &= ~MOVE_BACKWARD;
			break;
		case Keyboard::KEY_A:
			_keyFlags &= ~MOVE_LEFT;
			break;
		case Keyboard::KEY_D:
			_keyFlags &= ~MOVE_RIGHT;
			break;
		case Keyboard::KEY_Q:
			_keyFlags &= ~MOVE_DOWN;
			break;
		case Keyboard::KEY_E:
			_keyFlags &= ~MOVE_UP;
			break;
		case Keyboard::KEY_SHIFT:
			_moveSpeed -= _turboMove;
			break;
		}
	}

	//Game::getInstance()->getScriptController()->executeFunction<void>("camera_keyEvent", "[Keyboard::KeyEvent][Keyboard::Key]", evt, key);
}

bool StelGhostController::mouseEvent(Mouse::MouseEvent evt, int x, int y, int wheelDelta)
{
	switch (evt) {
	case Mouse::MOUSE_WHEEL:
		_fpsCamera->moveForward(wheelDelta * _moveSpeed / 2.0f);
		return true;
	default:
		return false;
	}
}

void StelGhostController::touchEvent(Touch::TouchEvent evt, int x, int y, unsigned int contactIndex)
{
	switch (evt) {
	case Touch::TOUCH_PRESS:
		_prevX = x;
		_prevY = y;
		break;
	case Touch::TOUCH_RELEASE:
		_prevX = 0;
		_prevY = 0;
		break;
	case Touch::TOUCH_MOVE:
		int deltaX = x - _prevX;
		int deltaY = y - _prevY;
		_prevX = x;
		_prevY = y;
		float pitch = -MATH_DEG_TO_RAD(deltaY * 0.5f);
		float yaw = MATH_DEG_TO_RAD(deltaX * 0.5f);
		_fpsCamera->rotate(yaw, pitch);
		break;
	};
}

void StelGhostController::updateInput(float elapsedTime)
{
	float time = (float)elapsedTime / 1000.0f;
	Vector2 move;

	if (_keyFlags != 0) {
		// Forward motion
		if (_keyFlags & MOVE_FORWARD) {
			move.y = 1;
		}
		else if (_keyFlags & MOVE_BACKWARD) {
			move.y = -1;
		}
		// Strafing
		if (_keyFlags & MOVE_LEFT) {
			move.x = 1;
		}
		else if (_keyFlags & MOVE_RIGHT) {
			move.x = -1;
		}
		move.normalize();

		// Up and down
		if (_keyFlags & MOVE_UP) {
			_fpsCamera->moveUp(time * _upDownSpeed);
		}
		else if (_keyFlags & MOVE_DOWN) {
			_fpsCamera->moveDown(time * _upDownSpeed);
		}
		if (!move.isZero()) {
			move.scale(time * _moveSpeed);
			_fpsCamera->moveForward(move.y);
			_fpsCamera->moveLeft(move.x);
		}
	}
}
