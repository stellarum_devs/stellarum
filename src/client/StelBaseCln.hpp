#ifndef STELBASECLN_H_
#define STELBASECLN_H_

#include "../StelBase.hpp"

class StelBaseCln : public StelNetObjCln, public StelBase
{
public:
	StelBaseCln();
	virtual ~StelBaseCln();

	virtual void update(float elapsedTime);

	void initialize(StelTeamID teamID, int hitPoints, Vector3 pos, const char* baseType);

protected:

	Node* _toriiNodes[3];

	// Replica3
	void Deserialize(DeserializeParameters *deserializeParameters);
	bool DeserializeConstruction(BitStream *constructionBitstream, Connection_RM3 *sourceConnection);
	bool DeserializeDestruction(BitStream *destructionBitstream, Connection_RM3 *sourceConnection);

};

#endif
