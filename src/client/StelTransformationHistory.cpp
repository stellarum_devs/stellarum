#include "StelTransformationHistory.hpp"
#include "RakAssert.h"

TransformationHistoryCell::TransformationHistoryCell()
{}

TransformationHistoryCell::TransformationHistoryCell(RakNet::TimeMS t, const Vector3& pos, const Vector3& vel, const Quaternion& quat  ) :
time(t), velocity(vel), position(pos), orientation(quat)
{
}

void StelTransformationHistory::init(RakNet::TimeMS maxWriteInterval, RakNet::TimeMS maxHistoryTime)
{
	writeInterval=maxWriteInterval;
	maxHistoryLength = maxHistoryTime/maxWriteInterval+1;
	history.ClearAndForceAllocation(maxHistoryLength+1, _FILE_AND_LINE_ );
	RakAssert(writeInterval>0);
}

void StelTransformationHistory::write(const Vector3 &position, const Vector3 &velocity, const Quaternion &orientation, RakNet::TimeMS curTimeMS)
{
	if (history.Size()==0)
	{
		history.Push(TransformationHistoryCell(curTimeMS,position,velocity,orientation), _FILE_AND_LINE_ );
	}
	else
	{
		const TransformationHistoryCell &lastCell = history.PeekTail();
		if (curTimeMS-lastCell.time>=writeInterval)
		{
			history.Push(TransformationHistoryCell(curTimeMS,position,velocity,orientation), _FILE_AND_LINE_ );
			if (history.Size()>maxHistoryLength)
				history.Pop();
		}
	}	
}
void StelTransformationHistory::overwrite(const Vector3 &position, const Vector3 &velocity, const Quaternion &orientation, RakNet::TimeMS when)
{
	int historySize = history.Size();
	if (historySize==0)
	{
		history.Push(TransformationHistoryCell(when,position,velocity,orientation), _FILE_AND_LINE_ );
	}
	else
	{
		// Find the history matching this time, and change the values.
		int i;
		for (i=historySize-1; i>=0; i--)
		{
			TransformationHistoryCell &cell = history[i];
			if (when >= cell.time)
			{
				if (i==historySize-1 && when-cell.time>=writeInterval)
				{
					// Not an overwrite at all, but a new cell
					history.Push(TransformationHistoryCell(when,position,velocity,orientation), _FILE_AND_LINE_ );
					if (history.Size()>maxHistoryLength)
						history.Pop();
					return;
				}

				cell.time=when;
				cell.position=position;
				cell.velocity=velocity;
				cell.orientation=orientation;
				return;
			}
		}
	}	
}
StelTransformationHistory::ReadResult StelTransformationHistory::read(Vector3 *position, Vector3 *velocity, Quaternion *orientation,
								 RakNet::TimeMS when, RakNet::TimeMS curTime)
{
	int historySize = history.Size();
	if (historySize==0)
	{
		return VALUES_UNCHANGED;
	}

	int i;
	for (i=historySize-1; i>=0; i--)
	{
		const TransformationHistoryCell &cell = history[i];
		if (when >= cell.time)
		{
			if (i==historySize-1)
			{
				if (curTime<=cell.time)
					return VALUES_UNCHANGED;

				float divisor = (float)(curTime-cell.time);
				RakAssert(divisor!=0.0f);
				float lerp = (float)(when - cell.time) / divisor;
				if (position)
					*position=cell.position + (*position-cell.position) * lerp;
				if (velocity)
					*velocity=cell.velocity + (*velocity-cell.velocity) * lerp;
				if (orientation)
					//*orientation = Quaternion::slerp(lerp, cell.orientation, *orientation);
					Quaternion::slerp(cell.orientation, *orientation, lerp, orientation);
			}
			else
			{
				const TransformationHistoryCell &nextCell = history[i+1];
				float divisor = (float)(nextCell.time-cell.time);
				RakAssert(divisor!=0.0f);
				float lerp = (float)(when - cell.time) / divisor;
				if (position)
					*position=cell.position + (nextCell.position-cell.position) * lerp;
				if (velocity)
					*velocity=cell.velocity + (nextCell.velocity-cell.velocity) * lerp;
				if (orientation)
					//*orientation = Quaternion::slerp(lerp, cell.orientation, nextCell.orientation);
					Quaternion::slerp(cell.orientation, nextCell.orientation, lerp, orientation);
			}
			return INTERPOLATED;
		}
	}

	// Return the oldest one
	const TransformationHistoryCell &cell = history.Peek();
	if (position)
		*position=cell.position;
	if (orientation)
		*orientation=cell.orientation;
	if (velocity)
		*velocity=cell.velocity;
	return READ_OLDEST;
}
void StelTransformationHistory::clear(void)
{
	history.Clear(_FILE_AND_LINE_);
}