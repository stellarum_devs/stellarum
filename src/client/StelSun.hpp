#ifndef __STELSUN_H__
#define __STELSUN_H__

#include "gameplay.h"
#include "StelEffect.hpp"

using namespace gameplay;


class StelSun : public StelEffect
{
public:
	StelSun();
	~StelSun();
	
	void initialize(Node* sunNode, float sunRadius);
	void computeEffect(Texture::Sampler* sceneColor, Texture::Sampler* sceneDepth);
	void renderEffect();
	
	Node* getNode() { return _sunNode; }
	
protected:
	Node* _sunNode;
	
	int _bufferWidth;
	int _bufferHeight;
	int _sceneWidth;
	int _sceneHeight;
	int _sceneAspectRatio;

	float _sunRadius;
	Vector4 _sunPos;
	Vector4 _sunPosProj;
	Matrix _biasMatrix;
	
	FrameBuffer* _frameBuffer;
	
	Model* _quadModel;
	
	Texture::Sampler* _dirtTextureSampler;
	RenderTarget* _sunTextures[5];
	Texture::Sampler* _sunTextureSamplers[5];
	
	Material* _depthTestMaterial;
	Material* _verticalBlurMaterial;
	Material* _horizontalBlurMaterial;
	Material* _lensFlareMaterial;
	
	bool _isSunVisible;
};

#endif
