#include "StelBaseCln.hpp"
#include "StelClient.hpp"
#include "StringCompressor.h"

StelBaseCln::StelBaseCln()
{
}

StelBaseCln::~StelBaseCln()
{
}

void StelBaseCln::initialize(StelTeamID teamID, int hitPoints, Vector3 pos, const char* baseType)
{
	StelBase::initialize(teamID, hitPoints);

	Scene* scene = Scene::getScene();

	std::string modelPath = "res/models/";
	modelPath.append(baseType);

	// load the model and spawn locations
	Bundle* bundle = Bundle::create(modelPath.c_str());
	GP_ASSERT(bundle);

	_baseNode = bundle->loadNode("base");
	_baseNode->getModel()->setMaterial("res/materials/arrowkun.material#arrowkun");
	PhysicsRigidBody::Parameters p;
	p.kinematic = true;
	p.mass = 100.f;
	p.friction = 0.5f;
	p.restitution = 0.5f;
	p.linearDamping = 0.1f;
	p.angularDamping = 0.5f;
	_baseNode->setCollisionObject(PhysicsCollisionObject::RIGID_BODY, PhysicsCollisionShape::mesh(_baseNode->getModel()->getMesh()), &p);
	
	_baseNode->setTag("StelClass", "StelBase");
	
	// position and rotation
	scene->addNode(_baseNode);
	_baseNode->translate(pos);
	Matrix m;
	Quaternion q;
	//Matrix::createLookAt(pos, Vector3::zero(), Vector3::unitY(), &m);
	//m.transpose();
	//m.getRotation(&q);
	//_baseNode->setRotation(q);
	

	// Esto habra q hacerlos children de base, y ser auto nombrados en plan "decoration_X" TODO
	Node* node = bundle->loadNode("torii_1");
	_toriiNodes[0] = node;
	scene->addNode(node);
	node->translate(pos);

	node = bundle->loadNode("torii_2");
	_toriiNodes[1] = node;
	scene->addNode(node);
	node->translate(pos);

	node = bundle->loadNode("torii_3");
	_toriiNodes[2] = node;
	scene->addNode(node);
	node->translate(pos);

	SAFE_RELEASE(bundle);
}

void StelBaseCln::update(float elapsedTime)
{

}

bool StelBaseCln::DeserializeConstruction(BitStream *constructionBitstream, Connection_RM3 *sourceConnection)
{
	Vector3 pos;
	StelTeamID id;
	int hitPoints;
	std::string baseType;
	char str[32];

	constructionBitstream->ReadAlignedBytes((unsigned char*)&id, sizeof(id));
	constructionBitstream->ReadVector(pos.x, pos.y, pos.z);
	constructionBitstream->ReadAlignedBytes((unsigned char*)&hitPoints, sizeof(hitPoints));
	StringCompressor::Instance()->DecodeString(str, 32, constructionBitstream); baseType = str;

	//Maybe not the cleanest way
	StelClient* game = (StelClient*)Game::getInstance();
	initialize(id, hitPoints, pos, baseType.c_str());
	game->getUniverse()->spawnBase(this);

	// TODO: proper material with settable color: StelTeamMgr->getTeamInfo(id) ...
	if (id == UNASSIGNED_TEAMID){
		_toriiNodes[0]->getModel()->setMaterial("res/materials/unlit.material#unlit_orange");
		_toriiNodes[1]->getModel()->setMaterial("res/materials/unlit.material#unlit_orange");
		_toriiNodes[2]->getModel()->setMaterial("res/materials/unlit.material#unlit_orange");
	}
	else if (id == 0){
		_toriiNodes[0]->getModel()->setMaterial("res/materials/unlit.material#unlit_blue");
		_toriiNodes[1]->getModel()->setMaterial("res/materials/unlit.material#unlit_blue");
		_toriiNodes[2]->getModel()->setMaterial("res/materials/unlit.material#unlit_blue");
	}
	else {
		_toriiNodes[0]->getModel()->setMaterial("res/materials/unlit.material#unlit_red");
		_toriiNodes[1]->getModel()->setMaterial("res/materials/unlit.material#unlit_red");
		_toriiNodes[2]->getModel()->setMaterial("res/materials/unlit.material#unlit_red");
	}

	return true;
}

void StelBaseCln::Deserialize(DeserializeParameters *deserializeParameters)
{
	deserializeParameters->serializationBitstream[0].ReadBitsFromIntegerRange(_hitPoints, 0, MAX_HITPOINTS);
}

bool StelBaseCln::DeserializeDestruction(BitStream *destructionBitstream, Connection_RM3 *sourceConnection)
{
	return true;
}

