#ifndef STELLOCALCONTROLLER_H_
#define STELLOCALCONTROLLER_H_

#include "../StelController.hpp"
#include "StelPlayerCln.hpp"

using namespace gameplay;


class StelLocalController : public StelController
{
	
	public:
		StelLocalController(uint64_t guid);
		~StelLocalController(){};

		void keyEvent(Keyboard::KeyEvent evt, int key);
		void touchEvent(Touch::TouchEvent evt, int x, int y, unsigned int contactIndex);
		bool mouseEvent(Mouse::MouseEvent evt, int x, int y, int wheelDelta);
		void gamepadEvent(Gamepad::GamepadEvent evt, Gamepad* gamepad);
		void update(float elapsedTime);
		inline Gamepad* getGamepad(){ return _gamepad; }
		void fireGenkiBall() override;
		ControllerCmdsPkg* createCmdPkg() const;

	private:
		void updateInput(float elapsedTime);
		Gamepad* _gamepad;
		int _x,_y;
		//vars to check double jump (space)
		bool _checkTimeSincePressed;
		float _timeSincePressed;
};

#endif