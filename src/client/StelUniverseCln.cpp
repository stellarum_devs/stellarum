#include "StelUniverseCln.hpp"
#include "StelSun.hpp"
#include "../StelRandom.hpp"
#include "StelPlayerCln.hpp"
#include "StelGenkiBallCln.hpp"
#include "StelPelletCln.hpp"

StelUniverseCln::StelUniverseCln() : partyMgr(NULL), _darkSun(false)
{
}

StelUniverseCln::~StelUniverseCln()
{
	SAFE_RELEASE(_sceneColorSampler);
	SAFE_RELEASE(_sceneDepthSampler);
}

void StelUniverseCln::initialize()
{
	StelUniverse::initialize();

	// load the model and spawn locations
	Bundle* bundle = Bundle::create("res/models/base.gpb");
	GP_ASSERT(bundle);

	Node* _baseNode = bundle->loadNode("base");

	partyMgr.setScene(_scene);
	
	// Sun stuff -------------------------------
	Game* game = Game::getInstance();
				
	// Scene render target
	_sceneFrameBuffer = FrameBuffer::create("SceneBuffer", game->getViewport().width, game->getViewport().height);
	RenderTarget* depthTexture = RenderTarget::create("sunDepth", game->getViewport().width, game->getViewport().height, true);
	
	_sceneFrameBuffer->setDepthTarget(depthTexture);
	
	_sceneColorSampler = Texture::Sampler::create(_sceneFrameBuffer->getRenderTarget()->getTexture());
	_sceneDepthSampler = Texture::Sampler::create(_sceneFrameBuffer->getDepthTarget()->getTexture());
	_sceneDepthSampler->setFilterMode(Texture::NEAREST, Texture::NEAREST);
	// If this fails, consider using setWrapMode((Texture::Wrap) GL_CLAMP, (Texture::Wrap) GL_CLAMP);
	_sceneDepthSampler->setWrapMode(Texture::CLAMP, Texture::CLAMP);
	
	SAFE_RELEASE(depthTexture);
	
	// Scene quad (paints render target)
	Mesh* mesh = Mesh::createQuadFullscreen();
	_sceneQuadModel = Model::create(mesh);
	SAFE_RELEASE(mesh);
	
	// Scene render effect (none by default)
	Material *sceneMat = Material::create("res/materials/passthrough.material");
	sceneMat->getParameter("u_texture")->setValue(_sceneColorSampler);
	_sceneQuadModel->setMaterial(sceneMat);

	// Create lights
	Light* light = Light::createPoint(1.0f, 1.0f, 1.0f, 9000.0f);
	_lightNode = _scene->addNode("lightNode");
	_lightNode->setLight(light);
	SAFE_RELEASE(light);

	Light* lightBase1 = Light::createPoint(0.25f, 0.25f, 1.0f, 1300.0f);
	_lightNodeBase1 = _scene->addNode("lightNodeBase1");
	_lightNodeBase1->setLight(lightBase1);
	_lightNodeBase1->setTranslation(Vector3::one() * ((0.f * -3000.f) + 1500.f));
	SAFE_RELEASE(lightBase1);

	Light* lightBase2 = Light::createPoint(1.0f, 0.25f, 0.25f, 1300.0f);
	_lightNodeBase2 = _scene->addNode("lightNodeBase2");
	_lightNodeBase2->setLight(lightBase2);
	_lightNodeBase2->setTranslation(Vector3::one() * ((1.f * -3000.f) + 1500.f));
	SAFE_RELEASE(lightBase2);
	
	// Scene nodes -------------------------------

	// Initialize sunNode
	Node* sunNode = _scene->addNode("sunNode");
	float sunRadius = SUNRADIUS;
	Mesh* sunMesh = createSphereMesh(sunRadius);
	Model* sunModel = Model::create(sunMesh);
	sunNode->setModel(sunModel);
	Material* sunMaterial = sunModel->setMaterial("res/sun/sun.material#sun");
	sunMaterial->getParameter("u_ambientColor")->setValue(_scene->getAmbientColor());
	sunMaterial->getParameter("u_lightColor")->setValue(Vector3(1.f, 0.5f, 0.5f));
	sunMaterial->getParameter("u_lightDirection")->setValue(_lightNode->getForwardVectorView());
	_sun = new StelSun();
	_sun->initialize(sunNode, sunRadius);

	// Stars
	Node* starsNode = _scene->addNode("starsNode");
	starsNode->setModel(partyMgr.createStarsModel(400, 66000.f, true));
		
	// Release items
	SAFE_RELEASE(light);
	SAFE_RELEASE(sunMesh);
	SAFE_RELEASE(sunModel);
}

void StelUniverseCln::finalize()
{
}

void StelUniverseCln::update(float elapsedTime)
{
	StelUniverse::update(elapsedTime);

	for (StelPlanet* planet : _planets) {
		planet->update(elapsedTime);
	}
	
	for (StelPlayer* player : _players) {
		player->update(elapsedTime);
	}
	
	for (StelGenkiBall* genki : _genkiballs) {
		genki->update(elapsedTime);
	}
	
	for (StelPellet* pellet : _pellets) {
		pellet->update(elapsedTime);
	}

	partyMgr.update(elapsedTime);
}

void StelUniverseCln::render(float elapsedTime, int debug_mode, bool wireframe, bool scout)
{
	getScene()->visit(this, &StelUniverseCln::drawScene,wireframe);
	
	// Render wireframes
	if (debug_mode > 0)
	{
		// Draw the physics debug information
		switch (debug_mode)
		{
			case 1:
				Game::getInstance()->getPhysicsController()->drawDebug(getScene()->getActiveCamera()->getViewProjectionMatrix());
				break;

			// for future debug use
			case 2:
				//_universe->getScene()->drawDebug(Scene::DEBUG_BOXES);
				break;
			case 3:
				//_universe->getScene()->drawDebug(Scene::DEBUG_SPHERES);
				break;
		}
	}
	
	//Scout

	if (scout)
	{
		SpriteBatch* batch = SpriteBatch::create("res/ui/circle-04-whole.png");
		gameplay::Rectangle src = gameplay::Rectangle(256, 256);
		gameplay::Rectangle dst = gameplay::Rectangle(0, 0, 64, 64);
		Vector2 coord;
		gameplay::Rectangle rec = Game::getInstance()->getViewport();
		Camera* cam = _scene->getActiveCamera();
		batch->start();
		for (StelPlayer* p : _players)
		{
			if (p->getPlayerController() != NULL) continue; //skip local player
			Vector3 pPos = p->getPlayerNode()->getTranslationWorld();

			if (cam->getFrustum().intersects(pPos))
			{
				_scene->getActiveCamera()->project(rec, pPos, &coord);

				dst.x = coord.x - 32;
				dst.y = coord.y - 32;

				batch->draw(dst, src, Vector4(0, 1, 0, 1));
			}
		}
		batch->finish();

		//Debug lines
		// 	_ddrawer.begin(getScene()->getActiveCamera()->getViewProjectionMatrix());
		// 		//_ddrawer.drawLine(Vector3::zero(),p,Vector3::one());
		// 	_ddrawer.end();
	}	
}

void StelUniverseCln::commitRender(float elapsedTime)
{
	_sun->computeEffect(_sceneColorSampler, _sceneDepthSampler);
	
	Game::getInstance()->clear(Game::CLEAR_COLOR_DEPTH, Vector4::zero(), 1.0f, 0);
	_sceneQuadModel->draw();
	
	_sun->renderEffect();
}

bool StelUniverseCln::drawScene(Node* node,bool wireframe)
{
	// If the node visited contains a model, draw it
	Model* model = node->getModel();
	ParticleEmitter* pe = node->getParticleEmitter();
	if (node->isActive())
	{
		//if (node->getBoundingSphere().intersects(_scene->getActiveCamera()->getFrustum()))
		//TODO , esto estaría bien a este nivel, pero no va bien
		if (model && node->getBoundingSphere().intersects(_scene->getActiveCamera()->getFrustum()))
		{
			model->draw(wireframe);
		}

		if (pe && node->getBoundingSphere().intersects(_scene->getActiveCamera()->getFrustum()))
		{
			pe->draw();
		}
	}
	return true;
}

void StelUniverseCln::spawnPlanet(StelPlanetCln* planet) {
	//Add orbit and planet to this universe & scene;
	_planets.push_back(planet);
	_scene->addNode(planet->getNode());
	Model* orbitModel =  planet->createOrbitLineModel(100);
	Node* orbitNode = Node::create("orbit");
	orbitNode->setModel(orbitModel);
	SAFE_RELEASE(orbitModel);
	orbitNode->setTranslation(planet->getOrbitCenter());
	_scene->addNode(orbitNode);
	planet->setOrbitNode(orbitNode);

	//Light binding
	Material* mat = planet->getNode()->getModel()->getMaterial();
	mat->getTechnique()->getParameter("u_ambientColor")->setValue(Vector3(0.2f, 0.13f, 0.13f));
	mat->getTechnique()->getParameter("u_pointLightColor[0]")->setValue(Vector3(1.1f, 1.0f, 1.0f));
	mat->getTechnique()->getParameter("u_pointLightPosition[0]")->bindValue(_lightNode, &Node::getTranslationView);
	mat->getTechnique()->getParameter("u_pointLightRangeInverse[0]")->setValue(_lightNode->getLight()->getRangeInverse());

	mat->getTechnique()->getParameter("u_pointLightColor[1]")->setValue(Vector3(0.2f, 0.2f, 1.0f));
	mat->getTechnique()->getParameter("u_pointLightPosition[1]")->bindValue(_lightNodeBase1, &Node::getTranslationView);
	mat->getTechnique()->getParameter("u_pointLightRangeInverse[1]")->setValue(_lightNodeBase1->getLight()->getRangeInverse());

	mat->getTechnique()->getParameter("u_pointLightColor[2]")->setValue(Vector3(1.0f, 0.2f, 0.2f));
	mat->getTechnique()->getParameter("u_pointLightPosition[2]")->bindValue(_lightNodeBase2, &Node::getTranslationView);
	mat->getTechnique()->getParameter("u_pointLightRangeInverse[2]")->setValue(_lightNodeBase2->getLight()->getRangeInverse());
}

void StelUniverseCln::spawnPellet(StelPelletCln* pellet, NetworkID netId)
{
	_pellets.push_back(pellet);
	for (StelPlanet* p : _planets) {
		if (p->GetNetworkID() == netId) {
			p->getNode()->addChild(pellet->getNode());
			break;
		}
	}
}

void StelUniverseCln::removePellet(StelPellet* pellet)
{
	_pellets.remove(pellet);
	pellet->getNode()->getParent()->removeChild(pellet->getNode());
	_scene->removeNode(pellet->getNode());
	delete pellet;
}

void StelUniverseCln::removeGenkiBall(StelGenkiBall* genki)
{
	StelUniverse::removeGenkiBall(genki);
	_genkiballs.remove(genki);
}

void StelUniverseCln::setDarkSun (bool b)
{
	if(b)
	{
		_sun->getNode()->getModel()->getMaterial()->getParameter("u_lightColor")->setValue(Vector3(0.5f, 0.3f, 1.0f));
		_sun->getNode()->setScale(0.3f);
		_lightNode->getLight()->setColor(0.5f,0.5f,1.0f);
	}
}

