#ifndef STELNETWORK_CLN_H_
#define STELNETWORK_CLN_H_

#include "../StelNetwork.hpp"

class StelNetworkCln : public StelNetwork
{
public:
	StelNetworkCln(){};
	~StelNetworkCln(){};

	// Initilize network system
	void initNetwork(const char* ipAddress);
	// Update de la red
	void update(float elapsedTime);

private:
	
};

#endif