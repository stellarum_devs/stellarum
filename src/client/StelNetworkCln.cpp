#include "StelNetworkCln.hpp"
#include "StringTable.h"
#include "StelUI.hpp"
#include "../StelNetPackages.hpp"

void StelNetworkCln::initNetwork(const char* ipAddress)
{
	_peer = RakNet::RakPeerInterface::GetInstance();
	// For tweaking later
	//_peer->SetTimeoutTime(5000, UNASSIGNED_SYSTEM_ADDRESS);
	StartupResult sr;
	RakNet::SocketDescriptor sd;
	
	sd.port = 0;
	StelUI::getInstance().showDebugMsg("Starting the client...", StelUI::color_Yellow);
	sr = _peer->Startup(1, &sd, 1);
	RakAssert(sr == RAKNET_STARTED);
	StelUI::getInstance().showDebugMsg("Client Initialized!", StelUI::color_Green);
	StelUI::getInstance().showDebugMsg("Connecting to server...");
	RakNet::ConnectionAttemptResult cr = _peer->Connect(ipAddress, SERVER_PORT, 0, 0);
	RakAssert(cr == CONNECTION_ATTEMPT_STARTED);
	
	_peer->AttachPlugin(_replicaMgr);
	_replicaMgr->SetNetworkIDManager(_networkIdMgr);
	_replicaMgr->SetAutoSerializeInterval(DEFAULT_SERVER_MILLISECONDS_BETWEEN_UPDATES);
	_networkStarted = true;
}

void StelNetworkCln::update(float elapsedTime)
{
	RakNet::Packet *packet;

	if (!_networkStarted)
		return;

	for (packet = _peer->Receive(); packet; _peer->DeallocatePacket(packet), packet = _peer->Receive())
	{
		switch (packet->data[0])
		{
		// Our connection request to the server has been accepted.
		case ID_CONNECTION_REQUEST_ACCEPTED:
		{			
			StelUI::getInstance().showDebugMsg("Our connection has been accepted", StelUI::color_Green);
		}
			break;
		// Sent to the player when a connection request cannot be completed due to inability to connect. 
		case ID_CONNECTION_ATTEMPT_FAILED:
		{			
			StelUI::getInstance().showDebugMsg("Unable to connect to the server", StelUI::color_Red);
		}
		// The system we attempted to connect to is not accepting new connections.
		case ID_NO_FREE_INCOMING_CONNECTIONS:
			StelUI::getInstance().showDebugMsg("Unable to connect, the server is full", StelUI::color_Red);
			break;
		// The system specified in Packet::systemAddress has disconnected from us. For the client, this means the server has shutdown.
		case ID_DISCONNECTION_NOTIFICATION:
			StelUI::getInstance().showDebugMsg("We have been disconnected, the server shutdown", StelUI::color_Red);
			break;
		// Reliable packets cannot be delivered to the system specified in Packet::systemAddress. The connection to that system has been closed.
		case ID_CONNECTION_LOST:
			StelUI::getInstance().showDebugMsg("Connection to the server lost", StelUI::color_Red);
			break;
		// ConnectionGraph2 plugin - In a client/server environment, a client other than ourselves has disconnected gracefully.
		case ID_REMOTE_DISCONNECTION_NOTIFICATION:
			StelUI::getInstance().showDebugMsg("Remote client has disconnected", StelUI::color_Yellow);
		break;
		
		// Stel messages from the server
		case ID_STEL_GAME_MSG:
		{
			RakNet::RakString rs;
			RakNet::BitStream bsIn(packet->data, packet->length, false);
			bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
			StelNetPkgID id;
			bsIn.Read(id);
			StelNetPackage* p = getNetPckFactory(id)();
			p->read(bsIn);
			p->_guid = _peer->GetGuidFromSystemAddress(packet->systemAddress).g;

			//queue to be procesed by game
			_packageQueuein.push_back(std::make_pair(id, p));
		}
			break;
		default:
			//StelUI::getInstance().showDebugMsg("Unhandled net message has arrived... ", StelUI::color_Yellow);
			break;
		}
	}
	
	// Send pending packages to server
	sendOutPackages();
}