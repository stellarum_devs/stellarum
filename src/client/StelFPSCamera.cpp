#include "StelFPSCamera.hpp"

StelFPSCamera::StelFPSCamera()
    : _pitchNode(NULL), _rootNode(NULL)
{
}

StelFPSCamera::~StelFPSCamera()
{
    SAFE_RELEASE(_pitchNode);
    SAFE_RELEASE(_rootNode);
}

void StelFPSCamera::initialize(float nearPlane, float farPlane, float fov)
{
    SAFE_RELEASE(_pitchNode);
    SAFE_RELEASE(_rootNode);
    _rootNode = Node::create("StelFPSCamera_root");
    _pitchNode = Node::create("StelFPSCamera_pitch");
    _rootNode->addChild(_pitchNode);

    float aspectRatio = Game::getInstance()->getAspectRatio();
    assert(aspectRatio > 0.0f);
    Camera* camera = Camera::createPerspective(fov, aspectRatio, nearPlane, farPlane);
    _pitchNode->setCamera(camera);
    SAFE_RELEASE(camera);
}

Node* StelFPSCamera::getRootNode()
{
    return _rootNode;
}

Camera* StelFPSCamera::getCamera()
{
    if (_pitchNode)
        return _pitchNode->getCamera();
    return NULL;
}

void StelFPSCamera::setPosition(const Vector3& position)
{
    _rootNode->setTranslation(position);
}

void StelFPSCamera::moveForward(float amount)
{
    Vector3 v = _pitchNode->getForwardVectorWorld();
    v.normalize().scale(amount);
    _rootNode->translate(v);
}

void StelFPSCamera::moveBackward(float amount)
{
    moveForward(-amount);
}

void StelFPSCamera::moveLeft(float amount)
{
    _rootNode->translateLeft(amount);
}

void StelFPSCamera::moveRight(float amount)
{
    _rootNode->translateLeft(-amount);
}

void StelFPSCamera::moveUp(float amount)
{
    _rootNode->translateUp(amount);
}

void StelFPSCamera::moveDown(float amount)
{
    _rootNode->translateUp(-amount);
}

void StelFPSCamera::rotate(float yaw, float pitch)
{
    _rootNode->rotateY(-yaw);
    _pitchNode->rotateX(pitch);
}

void StelFPSCamera::lookAt(Vector3 target)
{
	Matrix m;
	Matrix::createLookAt(_rootNode->getTranslation(), target, Vector3::unitY(), &m);
	m.transpose();
	Quaternion q;
	m.getRotation(&q);
	_rootNode->setRotation(q);
}