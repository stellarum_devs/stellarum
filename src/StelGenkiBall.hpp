#ifndef STELGENKIBALL_H_
#define STELGENKIBALL_H_

#include "gameplay.h"
#include "StelNetObj.hpp"

using namespace gameplay;

class StelGenkiBall : virtual public StelNetObj
{
public:
	
	const static float energyRatio;
	const static float radiusRatio;
	const static float damageRatio;
	
	StelGenkiBall();
	virtual ~StelGenkiBall();

	virtual void initialize(Vector3 pos, Vector3 vel, float chargeTime);

	virtual void update(float elapsedTime);
	std::string getAllocID()const { return "GenkiBall"; }
	
	inline float getBallEnergy() { return  _energy; }
	inline float getBallDamage() { return _energy * damageRatio; }
	inline float getBallRadius() { return _energy * radiusRatio; }
	
	Node* getNode() const {return _genkiBallNode;}
	
protected:
	Vector3 _position;
	Vector3 _velocity;

	// Gameplay node
	Node* _genkiBallNode;
	
	// The "charge" ammount chargeTime * energyRatio
	float _chargeTime;
	float _energy;

	// GenkiBall explanation:
	//	1- We spawn and receive for how long we were charged in _chargeTime
	//	2- Convert that time into "energy": _energyRatio * _chargeTime
	//	3- Once we know our energy use radiusRatio and damageRatio to get
	//  our actual radius and damage values: _radiusRatio * _energy, etc
};

#endif
