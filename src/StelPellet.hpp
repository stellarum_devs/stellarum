#ifndef STELPELLET_H_
#define STELPELLET_H_

#include "gameplay.h"
#include "StelNetObj.hpp"

using namespace gameplay;

class StelPellet : virtual public StelNetObj
{
public:
	StelPellet();
	virtual ~StelPellet();

	virtual void update(float elapsedTime);
	std::string getAllocID()const { return "Pellet"; }

	Node* getNode() const {return _node;};
	
protected:
	virtual void initialize();
	Vector3 _position;

	// Gameplay node
	Node* _node;
};

#endif
