#ifndef STELNETCUBE_H_
#define STELNETCUBE_H_

#include "gameplay.h"
#include "StelNetObj.hpp"

using namespace gameplay;

// TODO: cant touch this, nanananaa, HAMMER TIME!!!
class StelNetCube : virtual public StelNetObj
{
public:
	StelNetCube();
	virtual ~StelNetCube();

	virtual void update(float elapsedTime) {} ;
	virtual std::string getAllocID()const { return "NetCube"; }

protected:
	Vector3 _position;
	Quaternion _rotation, _rotationalVelocity;
	Vector3 _velocity;

	// Gameplay node
	Node* _netCubeNode;

};

#endif
