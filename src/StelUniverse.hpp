#ifndef STELUNIVERSE_H_
#define STELUNIVERSE_H_

#include "gameplay.h"
#include "StelPrimitives.hpp"
#include "StelNetPackages.hpp"
#include "StelNetObj.hpp"
#include "StelBase.hpp"
#include "StelPlayer.hpp"
#include "StelPlanet.hpp"
#include "StelPellet.hpp"
#include "StelGenkiBall.hpp"

class StelPlayer;

using namespace gameplay;

/**
 * Stellarum main game class.
 */
class StelUniverse
{
public:

	StelUniverse();
	virtual ~StelUniverse();
	
	virtual void initialize();
	virtual void finalize();

	virtual void update(float elapsedTime);
	
	inline Scene* getScene(){ return _scene; }
	inline std::vector<StelBase*> getBases(){ return _bases; }
	
	std::vector<StelPlayer*>* getPlayers() { return &_players; }
	std::list<StelGenkiBall*>* getGenkiList() { return &_genkiballs; }
	std::vector<StelPlanet*>* getPlanets() { return &_planets; }
	
	// Spawn net Objects
	virtual void spawnBase(StelBase* base);
	virtual void spawnPlayer(StelPlayer* controller);
	virtual void spawnGenkiBall(StelGenkiBall* genkiBall);

	// Remove spawned net Objects [Remove from storage(list,vector...) + _scene]
	virtual void removePlayer(StelPlayer* player);
	virtual void removeGenkiBall(StelGenkiBall* genki);
	
protected:
	Scene* _scene;
	
	// Physics and stuff
	std::vector<const char*> _collisionObjectPaths;
	
	// Game entities
	std::vector<StelBase*> _bases;
	std::vector<StelPlayer*> _players;
	std::vector<StelPlanet*> _planets;
	std::list<StelGenkiBall*> _genkiballs;
	std::list<StelPellet*> _pellets;
		
};


#endif