#include "StelNetwork.hpp"
#include "StelNetPackages.hpp"

StelNetPackage::StelNetPackage(StelNetPkgID id, PacketPriority priority, PacketReliability reliability, bool toGuid, uint64_t guid)
	: _id(id), _priority(priority), _reliability(reliability), _sendToGuid(toGuid), _guid(guid)
{
}

void StelNetPackage::send(StelNetwork* network) 
{
	network->sendStelPackage(_id, this);
}


// Stel Game Pkg -----------------

void StelGameStateChangePkg::execute(StelPackageListener* listener)
{
	listener->updateGameState(this);
}


void StelGameStateChangePkg::read(BitStream& bs)
{
	bs.Read(_state);

	if (_state == GAME_OVER)
	{
		bs.Read(_winnerTeamID);
	}	
}

void StelGameStateChangePkg::write(BitStream& bs)
{
	bs.Write(_state);

	if (_state == GAME_OVER)
	{
		bs.Write(_winnerTeamID);
	}
}
