#ifndef STELPLANET_SRV_H_
#define STELPLANET_SRV_H_

#include "../StelPlanet.hpp"

using namespace gameplay;

class StelUniverseSrv;

class StelPlanetSrv : public StelNetObjSrv, public StelPlanet
{
public:
	StelPlanetSrv();
	virtual ~StelPlanetSrv() {};

	void initialize(const int radius, std::string materialPath, Vector3 orbitCenter, float orbitRadius, Vector3 axis, float velocity = 0.0001f, Vector3 rotationVelocity = Vector3(0, 0, 0), float gravityRadius = 40);

	void update(float elapsedTime);

	void capture(StelTeamID teamID, StelCaptureMode captureMode);

	inline void setCaptureTeamDirty() { _captureTeamDirty = true; }

protected:
	void fireVolley(Vector3 targetPos, Vector3 targetVel, float speed, float damage, StelUniverseSrv* uni);
	
private:

	// Replica3
	RM3SerializationResult Serialize(SerializeParameters *serializeParameters);
	void SerializeConstruction(BitStream *constructionBitstream, Connection_RM3 *destinationConnection);
	void SerializeDestruction(BitStream *destructionBitstream, Connection_RM3 *destinationConnection){}

	void PostSerializeConstruction(RakNet::BitStream *constructionBitstream, RakNet::Connection_RM3 *destinationConnection);
	
	// Actions: energize player, attack base, defend base
	float _lastActionTime;
	bool _captureTeamDirty;

};
#endif
