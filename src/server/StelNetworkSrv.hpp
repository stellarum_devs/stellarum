#ifndef STELNETWORK_SRV_H_
#define STELNETWORK_SRV_H_

#include "../StelNetwork.hpp"

#define MAX_CLIENTS 32

class StelNetworkSrv : public StelNetwork
{
public:
	StelNetworkSrv(){};
	virtual ~StelNetworkSrv(){};

	// Initilize network system
	virtual void initNetwork(const char* ipAddress);
	// Update de la red
	virtual void update(float elapsedTime);
	
	// Añadir nuevo objecto en red a actualizar
	void addNetObject(StelNetObj* obj);

};

#endif