#include "StelServer.hpp"

// Declare our game instance
static StelServer game;

StelServer::StelServer()
: _universe(NULL),
_network(NULL),
_lastAssignedTeam(0),
_gameState(NORMAL),
_upTime(0.0f),
_lateTime(0.0f),
_gameDuration(30000.0f)
{
}

StelServer::~StelServer()
{
}

void StelServer::initialize()
{
	//Network, factories & stuff (NetworkSetup.cpp)
	_network = new StelNetworkSrv();
	initAllNetworkFactories(_network);
	_network->initNetwork(NULL);

	// Create a scene with planets
	_universe = new StelUniverseSrv(_network);
	_universe->initialize();
	
	//read some server params
	// load info from server.config
	Properties* config = Game::getInstance()->getConfig()->getNamespace("server", true); // TODO check if this needs delete
	_gameDuration= config->getFloat("gameDuration");
}

void StelServer::finalize()
{
	_network->getPeer()->Shutdown(300);
	SAFE_DELETE(_universe);
}

void StelServer::update(float elapsedTime)
{
	_network->update(elapsedTime); // this will need to change... lower updt freq or not
	_universe->update(elapsedTime);
	processNetQueue();
	_upTime += elapsedTime;
	if(_upTime > _gameDuration) {
		if(_lateTime == 0.0f) {
			timeUp();
		}
		_lateTime += elapsedTime;
	}
	if (elapsedTime < 15.f)
	{
		Platform::sleep(15.f - elapsedTime); //esto va en ms
	}
}

void StelServer::addController(uint64_t guid, std::string name)
{
	auto i = _remoteControllerMap.find(guid);
	if (i == _remoteControllerMap.end())
	{
		StelRemoteController* controller = new StelRemoteController(guid);
		_remoteControllerMap[guid] = controller;

		// Assign a team and its base
		StelTeamID teamID = _lastAssignedTeam++ % _universe->getBases().size();
		controller->setTeamID(_universe->getBases()[teamID]->getTeamID());
		printf("Adding player, teamID: %d\n", teamID);

		// Send team info to the client
		controller->createTeamInfoPkg()->send(_network);

		// Create a new player
		StelPlayerSrv* player = new StelPlayerSrv();
		player->getPhysicsCharacter()->setEnabled(true);
		player->setName(name);

		// Possess and spawn in universe
		controller->possess(player);
		_universe->spawnPlayer(player);
	}
	else 
	{
		GP_ASSERT(false + "client id already exsits");
	}
}

void StelServer::removeController(uint64_t guid)
{
	auto i = _remoteControllerMap.find(guid);
	if (i == _remoteControllerMap.end())
	{
		GP_ASSERT(false); //TODO, cambiar por throw Exception bla bla
	}
	else 
	{
		StelRemoteController* controller = _remoteControllerMap[guid];
		StelPlayerSrv* player = (StelPlayerSrv*)controller->getPlayer();
		_universe->removePlayer(player);
		_remoteControllerMap.erase(guid);
		delete controller;
	}
}

void StelServer::processCmds(ControllerCmdsPkg* cmdPkg)
{
	_remoteControllerMap[cmdPkg->_guid]->processCmds(cmdPkg);
}

void StelServer::processNetQueue()
{
	std::vector<std::pair<int, StelNetPackage*> >* queue =  _network->getInQueue();
	while (!queue->empty()) {
		StelNetPackage* p = queue->back().second;
		//printf("Process pkg, id: %d\n", p->id);
		p->execute(this);
		delete p;
		queue->pop_back();
	}
}

void StelServer::baseDestroyed(StelTeamID team, StelTeamID byTeam)
{
	//TODO maybe send info about the winner / loser...
	_gameState = GAME_OVER;
	StelGameStateChangePkg* pkg = new StelGameStateChangePkg();
	pkg->_state = _gameState;
	pkg->_winnerTeamID = byTeam;
	pkg->send(_network);

}

void StelServer::playersDestroyed(StelTeamID loser)
{
	//TODO maybe send info about the winner / loser...
	_gameState = GAME_OVER;
	StelGameStateChangePkg* pkg = new StelGameStateChangePkg();
	pkg->_state = _gameState;
	pkg->_winnerTeamID = loser == 0 ? 1 : 0; //TODO esto solo vale para 2 teams
	pkg->send(_network);
	
}

void StelServer::timeUp()
{
	//TODO maybe send info about the winner / loser...
	_gameState = LATE_GAME;
	StelGameStateChangePkg* pkg = new StelGameStateChangePkg();
	pkg->_state = _gameState;
	pkg->_winnerTeamID = 0;
	pkg->send(_network);
	for(StelPlayer* player : *_universe->getPlayers()) {
		player->setState(StelPlayer::BLACK_HOLE);
	}

}


int StelServer::getNumPlayersByTeamId ( StelTeamID teamId , bool onlyAlive)
{
	int num = 0;
	
	for(auto x : _remoteControllerMap) {
		StelRemoteController* ctl =  x.second;
		if(ctl->getTeamID() == teamId && (!onlyAlive || ctl->getPlayer()->getHealth()>0)) {
			num++;
		}
	}
	return num;

}
