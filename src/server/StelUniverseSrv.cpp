#include "StelUniverseSrv.hpp"
#include "StelNetworkSrv.hpp"
#include "../StelRandom.hpp"
#include "StelBaseSrv.hpp"
#include "StelPlanetSrv.hpp"
#include "StelPelletSrv.hpp"

StelUniverseSrv::StelUniverseSrv(StelNetworkSrv* net) : _network(net)
{
}

StelUniverseSrv::~StelUniverseSrv()
{
}

void StelUniverseSrv::initialize()
{
	StelUniverse::initialize();
	//Sun node
	Node* sunNode = _scene->addNode("sunNode");
	sunNode->setTag("StelClass","StelSun");
	PhysicsCollisionObject* sunColl = sunNode->setCollisionObject(PhysicsCollisionObject::GHOST_OBJECT, PhysicsCollisionShape::sphere(SUNRADIUS));
	//sunColl->addCollisionListener(this);
	initBases();
	initializePlanets();
	initializePellets(_planets);
}

void StelUniverseSrv::initBases()
{
	// load info from server.config
	Properties* config = Game::getInstance()->getConfig()->getNamespace("server", true); // TODO check if this needs delete
	unsigned short numTeams = config->getInt("numTeams");
	GP_ASSERT(numTeams > 0);

	StelBaseSrv* base = NULL;
	for (unsigned short i = 0; i < numTeams; i++)
	{
		config = Game::getInstance()->getConfig(); // reset config reader
		std::string teamInfo = "team_";

		//teamName numbers start on 1 not 0
		teamInfo.append(std::to_string(i + 1));
		config = config->getNamespace(teamInfo.c_str(), true);
		
		const char* teamName = config->getString("teamName");
		Vector3 color; config->getVector3("color", &color);
		const char* baseType = config->getString("baseType");
		int hitPoints = config->getInt("hitPoints");

		StelTeamMgr::getInstance().setTeamInfo(i, teamName, color);

		base = new StelBaseSrv();
		base->initialize(i, hitPoints, Vector3::one() * ((((float)_bases.size()) * -3000)+1500), 3, baseType);
		printf("base pos: (%f,%f,%f)\n", base->getNode()->getTranslationX(),base->getNode()->getTranslationY(),base->getNode()->getTranslationZ() );
		spawnBase(base);
		printf("Adding base: %s\n", teamInfo.c_str());
	}
}

void localPlanetSystem(std::vector<StelPlanet*>* planets, Scene* scene, StelNetworkSrv* network, Vector3 translation_centre, float radius_multiplier=1.0f) {
	const int total_radius = 1000;
	const int num_planets = 3;
	const int different_models = 5;
	std::string materialPrefix = "res/materials/planets.material#";
	const int different_materials = 3;
	std::string material_roulette[] = {"earth","moon","titan"};
	const int radius_roulette[] = {50, 70, 80, 90, 100};
	const int sphere_sides = 32;
	for(unsigned int i = 0; i<num_planets; i++) {
		int choosen_random = floor(StelRandom::getInstance().getFloat0_1()*different_models);
		int radius = radius_roulette[choosen_random];
		choosen_random =  floor(StelRandom::getInstance().getFloat0_1()*different_materials);
		std::string material = (materialPrefix +material_roulette[choosen_random]);
		//planet params: //TODO Moar random!!
		int orbital_radius = (total_radius / num_planets)*(i+1)*radius_multiplier;
		Vector3 axis = Vector3(StelRandom::getInstance().getFloat0_1(),StelRandom::getInstance().getFloat0_1(),StelRandom::getInstance().getFloat0_1());
		float velocity = 0.000005f;
		Vector3 rotation_velocity = Vector3(0, 0.000000001f, 0);
		float gravity_radius = 10000.f;
		StelPlanetSrv* planet = new StelPlanetSrv();
		planet->initialize(radius,material,translation_centre,orbital_radius ,axis,velocity, rotation_velocity,gravity_radius);
		planets->push_back(planet);
		scene->addNode(planet->getNode());
		network->addNetObject(planet);
	}
}

void StelUniverseSrv::initializePlanets()
{	
	Vector3 translation_centre = Vector3(0,0,0);
	//--- Sizes, etc-------//
	const int total_radius = 5000;
	const int num_planets = 3;
	const int different_models = 5;
	std::string materialPrefix = "res/materials/planets.material#";
	const int different_materials = 3;
	std::string material_roulette[] = {"earth","moon","titan"};
	const int radius_roulette[] = {50, 100, 150, 175, 200};
	const int sphere_sides = 32;
	
	//--- Planet creation and positions ----//
	localPlanetSystem(&_planets,_scene,_network,translation_centre,1.5);
	translation_centre = _bases[0]->getNode()->getTranslation();
	localPlanetSystem(&_planets,_scene,_network,translation_centre);
	translation_centre = _bases[1]->getNode()->getTranslation();
	localPlanetSystem(&_planets,_scene,_network,translation_centre);
}
void StelUniverseSrv::initializePellets(std::vector<StelPlanet*>& planets)
{
	int num_balls = 50;
	for(unsigned int i = 0; i<planets.size(); i++) {
		StelPlanet* planet = planets[i];
		double rad = planet->getNode()->getBoundingSphere().radius+7;
		for (unsigned int j = 0; j<num_balls/planets.size(); j++) {
			double theta= MATH_RANDOM_0_1()*2*MATH_PI;
			double u = MATH_RANDOM_MINUS1_1();
			double x = sqrt(1-u*u)*cos(theta);	
			double y = sqrt(1-u*u)*sin(theta);	
			double z = u;
			StelPelletSrv* pellet = new StelPelletSrv(Vector3(x*rad,y*rad,z*rad),planet);
			_network->addNetObject(pellet);
			_pellets.push_back(pellet);
		}
	}
}

void StelUniverseSrv::pickUpEnergy(Node* energyNode)
{
	energyNode->getParent()->removeChild(energyNode);
}

void StelUniverseSrv::giveTeamEnergy(unsigned short teamID, float energy)
{
	for (StelPlayer* p : _players)
	{
		if (p->getPlayerController()->getTeamID() == teamID)
			((StelPlayerSrv*)p)->addEnergy(energy);
	}
}

void StelUniverseSrv::finalize()
{
	StelUniverse::finalize();
	//TODO cosas por borrar
}


void StelUniverseSrv::update(float elapsedTime)
{
	StelUniverse::update(elapsedTime);
	
	for (StelPlanet* planet : _planets) 
	{
		planet->update(elapsedTime);
	}
	for (StelPlayer* player : _players) 
	{
		player->update(elapsedTime);
	}
	for (StelGenkiBall* genki : _genkiballs)
	{
		genki->update(elapsedTime);
	}
	// esto se hace asi por los collision listeners
	_genkiballs.erase( std::remove_if( _genkiballs.begin(), _genkiballs.end(),
		[&](StelGenkiBall* genki) -> bool 
		{
			// Do "some stuff", then return true if element should be removed
			if (genki->getNode()->hasTag("toBeRemoved"))
			{
				removeGenkiBall(genki);
				return true;
			}
			return false;
		}
	), _genkiballs.end()
	);

	for (StelPellet* pellet: _pellets) 
	{
		if(pellet->getNode()->hasTag("toBeRemoved"))
		{
			_pellets_to_remove.push_back(pellet);
		}
	}
	while (!_pellets_to_remove.empty())
	{
		removePellet( _pellets_to_remove.back());
		_pellets_to_remove.pop_back();
	}
}

// wow kill me plz, so much shit in here
void StelUniverseSrv::collisionEvent(PhysicsCollisionObject::CollisionListener::EventType type,
	const PhysicsCollisionObject::CollisionPair& collisionPair,
	const Vector3& contactPointA,
	const Vector3& contactPointB)
{
	// NOTE: este if esta mal si necesitamos detectar NOT_COLLIDING someday
	if (collisionPair.objectA == NULL || collisionPair.objectB == NULL)
		return;

	if (collisionPair.objectA->getNode()->hasTag("StelClass"))
	{
		
		//  SUN COLLISION:
		// -------------------------
		if (strcmp(collisionPair.objectA->getNode()->getTag("StelClass"), "StelPlayer") == 0)
		{
			
			// Get the player
			StelPlayerSrv* player = (StelPlayerSrv*)collisionPair.objectA->getNode()->getUserPointer();
			
			if (type == PhysicsCollisionObject::CollisionListener::COLLIDING)
			{
				// Collided with sun	
				if (collisionPair.objectB->getNode()->hasTag("StelClass") &&
					strcmp(collisionPair.objectB->getNode()->getTag("StelClass"), "StelSun") == 0)
				{
					player->addEnergy(20.0f); //dejo esto aposta, mini trick
					player->takeDamage(999,0);
				}
			}
			return;
		}
		//  PLAYER COLLISION:
		// -------------------------
		if (strcmp(collisionPair.objectA->getNode()->getTag("StelClass"), "StelPlayer") == 0)
		{
			
			// Get the player
			StelPlayerSrv* player = (StelPlayerSrv*)collisionPair.objectA->getNode()->getUserPointer();

			if (type == PhysicsCollisionObject::CollisionListener::COLLIDING)
			{
				// Collided with a pellet	
				if (collisionPair.objectB->getNode()->hasTag("StelClass") &&
					strcmp(collisionPair.objectB->getNode()->getTag("StelClass"), "StelPellet") == 0)
				{
					StelPelletSrv* pellet = (StelPelletSrv*)collisionPair.objectB->getNode()->getUserPointer();
					player->addEnergy(20.0f);
					collisionPair.objectB->getNode()->setTag("toBeRemoved", "true");
				}
			}
			return;
		}

		//  GENKIBALL COLLISION:
		// -------------------------
		if (strcmp(collisionPair.objectA->getNode()->getTag("StelClass"), "StelGenkiBall") == 0)
		{

			// Get the genkiBall
			StelGenkiBallSrv* genki = (StelGenkiBallSrv*)collisionPair.objectA->getNode()->getUserPointer();
			//TODO dirty fix, to prevent collision with planet and avoid executing both player and genki listener
			if(genki->_lifeTime < 150.0f || genki->getNode()->hasTag("toBeRemoved") ) return;
			if (type == PhysicsCollisionObject::CollisionListener::COLLIDING)
			{
				// Collided with a player
				if (collisionPair.objectB->getNode()->hasTag("StelClass") &&
					strcmp(collisionPair.objectB->getNode()->getTag("StelClass"), "StelPlayer") == 0)
				{
					StelPlayerSrv* player = (StelPlayerSrv*)collisionPair.objectB->getNode()->getUserPointer();
					if (player->getPlayerController()->getTeamID() != genki->getOwnerTeamID())
					{
						player->takeDamage(1, genki->getOwnerGUID());
						printf("genkiball HIT Player!\n");
						genki->getNode()->setTag("toBeRemoved", "true");
					}
				}
				// Collided with a base
				else if (collisionPair.objectB->getNode()->hasTag("StelClass") &&
						strcmp(collisionPair.objectB->getNode()->getTag("StelClass"), "StelBase") == 0)
				{
					StelBaseSrv* base = (StelBaseSrv*)collisionPair.objectB->getNode()->getUserPointer();
					if (base->getTeamID() != genki->getOwnerTeamID())
					{
						printf("genkiball HIT a Base!\n");
						base->takeDamage(genki->getBallDamage(), genki->getOwnerTeamID(), genki->getOwnerGUID());
					}
					genki->getNode()->setTag("toBeRemoved", "true");
				}
				else
				{
					// Collided with something else that has a collision object
					printf("genkiball HIT something!\n");
					genki->getNode()->setTag("toBeRemoved", "true");
				}
			}
			else // NOT_COLLIDING ~ check NOTE above, we don't capture this
			{

			}
			return;
		}
	}
}

void StelUniverseSrv::placeOnBase(StelPlayerSrv* player)
{
	printf("Place on base: %d\n", player->getPlayerController()->getTeamID());
	((StelBaseSrv*)_bases[player->getPlayerController()->getTeamID()])->placeOnSpawn(player);
}

void StelUniverseSrv::spawnBase(StelBase* base)
{
	StelUniverse::spawnBase(base);
	_network->addNetObject(base);
	printf("Base spawned!\n");
}

void StelUniverseSrv::spawnPlayer(StelPlayer* player)
{
	StelUniverse::spawnPlayer(player);
	player->getPhysicsCharacter()->addCollisionListener(this);
	placeOnBase(((StelPlayerSrv*)player));
	_network->addNetObject(player);
	printf("Player spawned!\n");
}

void StelUniverseSrv::spawnGenkiBall(StelGenkiBall* genkiBall)
{
	StelUniverse::spawnGenkiBall(genkiBall);
	genkiBall->getNode()->getCollisionObject()->addCollisionListener(this);
	_network->addNetObject(genkiBall);
	printf("GenkiBall spawned!\n");
}

void StelUniverseSrv::removePlayer(StelPlayer* player)
{
	StelUniverse::removePlayer(player);
	player->BroadcastDestruction();
	delete(player);
	printf("Player destruction!\n");
}

void StelUniverseSrv::removeGenkiBall(StelGenkiBall* genkiBall)
{
	StelUniverse::removeGenkiBall(genkiBall);
	genkiBall->BroadcastDestruction();
	delete(genkiBall);
	printf("GenkiBall destruction!\n");
}

void StelUniverseSrv::removePellet(StelPellet* pellet)
{
	_scene->removeNode(pellet->getNode());
	_pellets.remove(pellet);
	pellet->BroadcastDestruction();
	printf("Pellet destruction!\n");
}
