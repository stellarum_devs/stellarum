#ifndef STELTOTEM_SRV_H_
#define STELTOTEM_SRV_H_

#include "../StelTotem.hpp"
#include "StelPlanetSrv.hpp"

class StelTotemSrv : public StelNetObjSrv, public StelTotem
{
public:
	StelTotemSrv() {};  // Mandatory for replicas, not used by the server
	StelTotemSrv(StelCaptureMode mode, StelPlanetSrv* parentPlanetNode);
	virtual ~StelTotemSrv();

	void update(float elapsedTime);

	inline StelPlanetSrv* getPlanet(){ return _planet; }
	
	// Usable interface
	void startUsing(void* who) override;
	void stopUsing() override;
	void lostUser() override;
	
protected:
	StelPlanetSrv* _planet;
	
private:
	// Replica3
	RM3SerializationResult Serialize(SerializeParameters *serializeParameters);
	void SerializeConstruction(BitStream *constructionBitstream, Connection_RM3 *destinationConnection);
	void SerializeDestruction(BitStream *destructionBitstream, Connection_RM3 *destinationConnection){}

};
#endif
