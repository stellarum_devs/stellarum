﻿#include "StelPlayerSrv.hpp"
#include "StelServer.hpp"
#include "StringCompressor.h"

StelPlayerSrv::StelPlayerSrv() :
_healthDirty(true),
_energyDirty(true),
_usedObject(NULL),
_usedObjectNode(NULL),
_lastUsedCheck(0.f)
{
	StelPlayerSrv::initialize();
	_playerNode->setTag("StelClass", "StelPlayer");
	_playerNode->setUserPointer(this);
}

StelPlayerSrv::~StelPlayerSrv()
{
}

void StelPlayerSrv::initialize()
{	
	// Load player mesh, phys, anims...
	_playerNode = Node::create("micro_orc");
	_playerNode->setCollisionObject("res/physics/orc.physics#orc"); //TODO, hardcoded orc is sad
	_playerPhysics = static_cast<PhysicsCharacter*>(_playerNode->getCollisionObject());
	_jetPhysics = new StelJetPhysics(_playerNode);
	_playerPhysics->setPhysicsEnabled(false);
	GP_ASSERT(_playerPhysics);
	_scene->addNode(_playerNode);

	_playerPhysics->setGravityParams(0, 1000, true); // TODO: remove this cuando gravitySource este init a NULL
	_usableHF.setIgnoredNode(_playerNode);

	_position = _playerNode->getTranslation();
	_rotation = _playerNode->getRotation();
	_velocity.x = _currentDirection.x;
	_velocity.y = _currentDirection.y;

	_cameraNode = Node::create("playerCameraNode");
	_playerNode->addChild(_cameraNode);
	_cameraNode->setTranslationY(PLAYER_CAM_OFFSET);
}

void StelPlayerSrv::update(float elapsedTime)
{
	if(_health == 0) {
		//WE ARE NOT ALIVE -> STATUE
		if(_playerController) {
			StelServer* game = (StelServer*) Game::getInstance();
			if(game->getNumPlayersByTeamId(_playerController->getTeamID(),true ==0) && game->getGameState() !=GAME_OVER) {
				game->playersDestroyed(_playerController->getTeamID());
			}
		}
		return;
	} 
	StelPlayer::update(elapsedTime);

	// Set values for client replication 
	_position = _playerNode->getTranslation();
	_rotation = _playerNode->getRotation();
	
	switch(_state){
		case ON_PLANET: 
		{
			_velocity = _playerPhysics->getVerticalVelocity();
			break;
		}
		case JETPACK: 
		{
			_velocity = _jetPhysics->getVelocity();
			break;
		}
	}

	if (_usedObject != NULL)
	{
		_lastUsedCheck += elapsedTime;
		if (_lastUsedCheck > 250.f)
		{
			checkUsedNodeInRange();
			_lastUsedCheck = 0.f;
		}
	}
}

void StelPlayerSrv::addEnergy(float ammount)
{
	_energy = std::max(0.f, std::min(_energy + ammount, PLAYER_MAX_ENERGY));
	_energyDirty = true; 
}

void StelPlayerSrv::takeDamage(int dmg, uint64_t dmgGUID)
{
	// Care, don't let _health take values outside range or rakky will go boom
	if (_health - dmg <= 0)
	{
		// yo dead nigga, play sounds & fx crap, add points to KILLA
		_health = PLAYER_INITIAL_HEALTH;
		_healthDirty = true;
		_energy = PLAYER_INITIAL_ENERGY;
		
		printf("You got PWNED by: %s\n", "jenova");//TODO game->controllers(dmgGUID)->getName
		((StelServer*)(Game::getInstance()))->getUniverse()->placeOnBase(this);
		if(_state == BLACK_HOLE) {
			_health = 0; //health 0 means the player will not respawn as usual
		}
	}
	else
	{
		_health -= dmg;
	}
	_healthDirty = true;
}

void StelPlayerSrv::animationEvent(AnimationClip* clip, AnimationClip::Listener::EventType type)
{
	/*if (clip == some_clip)
	{
	 do something related to some_clip
	}
	else
	{
		clip->crossFade(_currentAnimClip, 150);
	}*/
}

void StelPlayerSrv::startUsingObj()
{
	if (!_usingObject)
	{
		_usingObject = true;
		if (_usedObject != NULL)
		{
			printf("Something weird happened using an object!\n");
			_usedObject = NULL;
		}

		//1: Ray - have a ray pointing in the cam dir
		_pickRay.setDirection(_cameraForward);
		_pickRay.setOrigin(_cameraNode->getTranslationWorld());

		//2: Do raytest when click comes, if hit usable, save it and call startUsing()
		PhysicsController::HitResult hitResult;
		if (Game::getInstance()->getPhysicsController()->rayTest(_pickRay, PLAYER_USE_DIST, &hitResult, &_usableHF))
		{
			// Using something
			_usedObject = ((StelUsable*)hitResult.object->getNode()->getUserPointer());
			_usedObject->startUsing(this);

			_usedObjectNode = hitResult.object->getNode();
			printf("Start using an object!\n");
		}
	}
}

void StelPlayerSrv::stopUsingObj()
{
	// UnClick! - if we have something stored forget it = NULL and call stopUsing()
	if (_usedObject != NULL)
	{
		_usedObject->stopUsing();
		_usedObject = NULL;
		printf("Stop using an object!\n");
	}

	_usingObject = false;
}

bool StelPlayerSrv::checkUsedNodeInRange()
{
	_pickRay.setDirection(_cameraForward);
	_pickRay.setOrigin(_cameraNode->getTranslationWorld());

	PhysicsController::HitResult hitResult;
	// If not hitting the object we were using -> we lost it!
	if (!(Game::getInstance()->getPhysicsController()->rayTest(_pickRay, PLAYER_USE_DIST, &hitResult, &_usableHF)
		&& hitResult.object->getNode() == _usedObjectNode))
	{
		_usedObject->lostUser(); // TODO: remember to call this when player dies
		_usedObject = NULL;
		printf("Used object was lost!\n");
		return false;
	}

	printf("Used object check went OK!\n");
	return true;
}

void StelPlayerSrv::SerializeConstruction(BitStream *constructionBitstream, Connection_RM3 *destinationConnection)
{
	constructionBitstream->WriteAlignedBytes((const unsigned char*)&_guid, sizeof(_guid));
	const char* name = _name.c_str();
	StringCompressor::Instance()->EncodeString(name, 64, constructionBitstream);
	if (_playerController->getTeamID() == 0) //TODO decent character model picker
		constructionBitstream->Write1(); // its a bat
	else
		constructionBitstream->Write0();
}

RM3SerializationResult StelPlayerSrv::Serialize(SerializeParameters *serializeParameters)
{
	serializeParameters->pro->priority = HIGH_PRIORITY;
	serializeParameters->pro->reliability = UNRELIABLE_SEQUENCED;

	//printf("mandando player vars %f, %f, %f\n", _position.x, _position.y, _position.z);
	// Autoserialize causes a network packet to go out when any of these member variables change.
	serializeParameters->outputBitstream[0].WriteAlignedBytes((const unsigned char*)&_position, sizeof(_position));
	serializeParameters->outputBitstream[0].WriteAlignedBytes((const unsigned char*)&_rotation, sizeof(_rotation));
	serializeParameters->outputBitstream[0].WriteAlignedBytes((const unsigned char*)&_velocity, sizeof(_velocity));
	if (_energyDirty || _healthDirty)
	{
		serializeParameters->outputBitstream[0].Write1();
		serializeParameters->outputBitstream[0].WriteBitsFromIntegerRange(_health, 0, PLAYER_MAX_HEALTH);
		serializeParameters->outputBitstream[0].WriteFloat16(_energy, 0.0f, PLAYER_MAX_ENERGY);
	}
	else
		serializeParameters->outputBitstream[0].Write0();

	if (_chargingGenkiBall)
		serializeParameters->outputBitstream[0].Write1();
	else
		serializeParameters->outputBitstream[0].Write0();
	return RM3SR_BROADCAST_IDENTICALLY;
}
