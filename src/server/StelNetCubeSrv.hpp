#ifndef STELNETCUBE_SRV_H_
#define STELNETCUBE_SRV_H_

#include "../StelNetCube.hpp"

class StelNetCubeSrv : public StelNetObjSrv, public StelNetCube
{
public:
	StelNetCubeSrv();
	~StelNetCubeSrv();

	void update(float elapsedTime);

private:

	// Replica3
	RM3SerializationResult Serialize(SerializeParameters *serializeParameters);
	void SerializeConstruction(BitStream *constructionBitstream, Connection_RM3 *destinationConnection){}
	void SerializeDestruction(BitStream *destructionBitstream, Connection_RM3 *destinationConnection){}
};

#endif
