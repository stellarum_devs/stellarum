#ifndef STELPELLET_SRV_H_
#define STELPELLET_SRV_H_

#include "../StelPellet.hpp"

class StelPlanet;

class StelPelletSrv : public StelNetObjSrv, public StelPellet
{
public:
	StelPelletSrv() {};  // Mandatory for replicas, not used by server
	StelPelletSrv(Vector3 pos, StelPlanet* parentPlanetNode);
	virtual ~StelPelletSrv();

	void update(float elapsedTime);

	StelPlanet* getPlanet(){ return _planet; }
	
	
protected:
	StelPlanet* _planet;
	
private:
	// Replica3
	RM3SerializationResult Serialize(SerializeParameters *serializeParameters);
	void SerializeConstruction(BitStream *constructionBitstream, Connection_RM3 *destinationConnection);
	void SerializeDestruction(BitStream *destructionBitstream, Connection_RM3 *destinationConnection){}

};
#endif
