#include "StelGenkiBallSrv.hpp"
#include "StelPlayerSrv.hpp"

const float StelGenkiBallSrv::MAX_TIME = 30000.f;

StelGenkiBallSrv::StelGenkiBallSrv() :
	_lifeTime(0.0f),
	_ownerTeamID(-1)
{
	_genkiBallNode->setTag("StelClass", "StelGenkiBall");
	_genkiBallNode->setUserPointer(this);
}

StelGenkiBallSrv::~StelGenkiBallSrv()
{
}

void StelGenkiBallSrv::initialize(Vector3 pos, Vector3 vel, float chargeTime)
{
	StelGenkiBall::initialize(pos, vel, chargeTime);
	_genkiBallNode->setCollisionObject(PhysicsCollisionObject::GHOST_OBJECT, PhysicsCollisionShape::sphere(_energy * radiusRatio));
}

void StelGenkiBallSrv::update(float elapsedTime)
{
	StelGenkiBall::update(elapsedTime);
	
	_lifeTime += elapsedTime;
	if (_lifeTime > MAX_TIME)
	{
		// time's up
		_genkiBallNode->setTag("toBeRemoved", "true");
	}
}

void StelGenkiBallSrv::SerializeConstruction(BitStream *constructionBitstream, Connection_RM3 *destinationConnection)
{
	constructionBitstream->WriteVector(_position.x, _position.y, _position.z);
	constructionBitstream->WriteVector(_velocity.x, _velocity.y, _velocity.z);
	constructionBitstream->WriteAlignedBytes((const unsigned char*)&_chargeTime, sizeof(_chargeTime));
}

RM3SerializationResult StelGenkiBallSrv::Serialize(SerializeParameters *serializeParameters)
{
	return RM3SR_NEVER_SERIALIZE_FOR_THIS_CONNECTION;
}
