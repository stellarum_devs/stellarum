#include "StelBaseSrv.hpp"
#include "../StelPrimitives.hpp"
#include "StringCompressor.h"
#include "StelServer.hpp"

StelBaseSrv::StelBaseSrv() :
_lastSpawnLocUsed(0)
{
}

StelBaseSrv::~StelBaseSrv()
{
}

void StelBaseSrv::initialize(StelTeamID teamID, int hitPoints, Vector3 pos, int numSpawnLocs, const char* baseType)
{
	StelBase::initialize(teamID, hitPoints);
	
	std::string modelPath = "res/models/";
	modelPath.append(baseType);
	_baseType = baseType;

	// load the model and spawn locations
	Bundle* bundle = Bundle::create(modelPath.c_str());
	GP_ASSERT(bundle);

	_baseNode = bundle->loadNode("base");
	PhysicsRigidBody::Parameters p;
	p.kinematic = true;
	p.mass = 100.f;
	p.friction = 0.5f;
	p.restitution = 0.5f;
	p.linearDamping = 0.1f;
	p.angularDamping = 0.5f;
	_baseNode->setCollisionObject(PhysicsCollisionObject::RIGID_BODY, PhysicsCollisionShape::mesh(_baseNode->getModel()->getMesh()), &p);
	
	_baseNode->setTag("StelClass", "StelBase");
	_baseNode->setUserPointer(this);
	
	Scene::getScene()->addNode(_baseNode);
	
	// position and rotation
	_baseNode->translate(pos);
	Matrix m;
	Quaternion q;
	//Matrix::createLookAt(pos, Vector3::zero(), Vector3::unitY(), &m);
	//m.transpose();
	//m.getRotation(&q);
	//_baseNode->setRotation(q);

	//------------------------

	// load spawn locations, default "spawn_0" should ALWAYS exist
	std::string spawnPrefix;
	Node* spawnDummyNode = NULL;
	std::string nodeName;

	// load "spawn_0"
	spawnDummyNode = bundle->loadNode("spawn_0");
	GP_ASSERT(spawnDummyNode);
	_spawnLocations.push_back(std::make_pair(spawnDummyNode->getTranslation(), Quaternion::identity()));

	// load any other spawns
	for (unsigned short i = 1; i <= numSpawnLocs; i++)
	{
		spawnPrefix = "spawn_";
		nodeName = spawnPrefix.append(std::to_string(i));
		spawnDummyNode = bundle->loadNode(nodeName.c_str());
		spawnDummyNode->translate(pos);
		
		Matrix::createLookAt(spawnDummyNode->getTranslation(), _spawnLocations[0].first, Vector3::unitY(), &m);
		m.transpose();
		m.getRotation(&q);
		_spawnLocations.push_back(std::make_pair(spawnDummyNode->getTranslation(), q));
	}
	SAFE_RELEASE(bundle);
}

void StelBaseSrv::update(float elapsedTime)
{

}

void StelBaseSrv::takeDamage(int dmg, StelTeamID byTeamID, uint64_t byGUID)
{
	// Care, don't let _hitPoints take values outside range or rakky will go boom
	if (_hitPoints - dmg <= 0)
	{
		printf("Base destroyed! teamID: %d%s\n", this->_teamID, " end game");
		StelServer* server = (StelServer*) Game::getInstance();
		server->baseDestroyed(this->_teamID, byTeamID);
	}
	else
	{
		_hitPoints -= dmg;
		printf("Base hit!, hitpoints left: %d\n", _hitPoints);
	}
}

void StelBaseSrv::placeOnSpawn(StelPlayerSrv* player)
{
	for (unsigned int i = 0; i < _spawnLocations.size(); i++)
	{
		// rotate spawn locs skipping 0
		unsigned int loc = _lastSpawnLocUsed++ % (_spawnLocations.size()-1) + 1;
		printf("Try placing on spawn location: %d\n", loc);
		if (true) // TODO: do ray test to see if anything is blocking the spawn location
		{
			player->_position = _spawnLocations[loc].first;
			player->_rotation = _spawnLocations[loc].second;
			player->_velocity = Vector3::zero();
			player->getPlayerNode()->setTranslation(player->_position);
			player->getPlayerNode()->setRotation(player->_rotation);
			player->getPhysicsCharacter()->setVelocity(player->_velocity);
				
			// All done! we can leave now
			return;
		}		
	}

	// Couldn't find a decent spawn loc, so use default
	printf("All spawn locations failed! using default\n");
	player->_position = _spawnLocations[0].first;
	player->_rotation = _spawnLocations[0].second;
	player->_velocity = Vector3::zero();
	player->getPhysicsCharacter()->setVelocity(player->_velocity);
}

void StelBaseSrv::SerializeConstruction(BitStream *constructionBitstream, Connection_RM3 *destinationConnection)
{
	Vector3 pos = _baseNode->getTranslation();
	const char* baseType = _baseType.c_str();

	constructionBitstream->WriteAlignedBytes((const unsigned char*)&_teamID, sizeof(_teamID));
	constructionBitstream->WriteVector(pos.x, pos.y, pos.y);
	constructionBitstream->WriteAlignedBytes((const unsigned char*)&_hitPoints, sizeof(_hitPoints));	
	StringCompressor::Instance()->EncodeString(baseType, 32, constructionBitstream);
}

RM3SerializationResult StelBaseSrv::Serialize(SerializeParameters *serializeParameters)
{
	serializeParameters->outputBitstream[0].WriteBitsFromIntegerRange(_hitPoints, 0, MAX_HITPOINTS);
	return RM3SR_BROADCAST_IDENTICALLY;
}
