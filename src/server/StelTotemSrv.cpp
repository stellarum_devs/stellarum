#include "StelTotemSrv.hpp"
#include "StelPlayerSrv.hpp"
#include "../StelController.hpp"

StelTotemSrv::StelTotemSrv(StelCaptureMode mode, StelPlanetSrv* parentPlanetNode)
{
	_mode = mode;
	_planet = parentPlanetNode;

	// Load totem mesh, phys
	Bundle* bundle = Bundle::create("res/models/totem.gpb");
	GP_ASSERT(bundle);
	_node = bundle->loadNode("StoneLantern");
	SAFE_RELEASE(bundle);

	_node->setTag("StelClass", "StelTotem");
	_node->setUserPointer(((StelUsable*)this));
	//TODO dont load the tottem mesh -> set bounds manually Vector3(5.f,5.f,5.f)
	_node->setCollisionObject(PhysicsCollisionObject::GHOST_OBJECT, PhysicsCollisionShape::box());

	parentPlanetNode->getNode()->addChild(_node);
	initPosition(); // must do this after addChild because transforms are reset
	
	StelTotem::initialize();
}

StelTotemSrv::~StelTotemSrv()
{
}

void StelTotemSrv::update(float elapsedTime)
{
	if (_capturingTeamID != UNASSIGNED_TEAMID)
	{
		_captureTime += elapsedTime;

		if (_captureTime >= TOTEM_CAPTURE_TIME)
		{
			_captureTime = 0.f;
			_planet->capture(_capturingTeamID, _mode);
			// stopUsing() is called by planet on all its totems on capture(...)
			printf("Planet captured!\n");
		}
	}
	else
	{
		if (_captureTime > 0.f)
		{
			_captureTime = std::fmax(0.f, _captureTime - (elapsedTime * 1.5f));
		}
	}
}

void StelTotemSrv::startUsing(void* who)
{
	if (_inUse)
	{
		stopUsing();
		return;
	}

	StelTeamID playerTeamID = ((StelPlayerSrv*) who)->getPlayerController()->getTeamID();

	if (_planet->getCaptureTeamID() != playerTeamID ||
	   (_mode != _planet->getCaptureMode() && _planet->getCaptureTeamID() == playerTeamID))
	{
		// Begin capture
		printf("Began capture...\n");
		_inUse = true;
		_capturingTeamID = playerTeamID;
	}
	else
	{
		if (_mode != _planet->getCaptureMode())
		{
			// Begin switch capture
			_inUse = true;
		}
	}
}

void StelTotemSrv::stopUsing()
{
	StelTotem::stopUsing();
}

void StelTotemSrv::lostUser()
{
	StelTotem::lostUser();
}

void StelTotemSrv::SerializeConstruction(BitStream *constructionBitstream, Connection_RM3 *destinationConnection)
{
	constructionBitstream->Write(_mode);
	constructionBitstream->Write(_planet->GetNetworkID());

	_planet->setCaptureTeamDirty();	// do this to syncronize captureTeam on late joining players
}

RM3SerializationResult StelTotemSrv::Serialize(SerializeParameters *serializeParameters)
{
	serializeParameters->outputBitstream[0].Write(_inUse);
	serializeParameters->outputBitstream[0].Write(_capturingTeamID);
	serializeParameters->outputBitstream[0].Write(_captureTime);
	return RM3SR_BROADCAST_IDENTICALLY;
}