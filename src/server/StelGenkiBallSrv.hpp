#ifndef STELGENKIBALL_SRV_H_
#define STELGENKIBALL_SRV_H_

#include "../StelGenkiBall.hpp"

class StelGenkiBallSrv : public StelNetObjSrv, public StelGenkiBall
{
public:
	StelGenkiBallSrv();
	virtual ~StelGenkiBallSrv();

	virtual void initialize(Vector3 pos, Vector3 vel, float chargeTime);

	void update(float elapsedTime);
	
	inline uint64_t getOwnerGUID(){ return _ownerGUID; }
	inline void setOwnerGUID(uint64_t g){ _ownerGUID = g; }
	inline unsigned short getOwnerTeamID(){ return _ownerTeamID; }
	inline void setOwnerTeamID(unsigned short tID){ _ownerTeamID = tID; }
	
	static const float MAX_TIME;
	float _lifeTime;

private:
	uint64_t _ownerGUID;
	unsigned short _ownerTeamID;

	// Replica3
	RM3SerializationResult Serialize(SerializeParameters *serializeParameters);
	void SerializeConstruction(BitStream *constructionBitstream, Connection_RM3 *destinationConnection);
	void SerializeDestruction(BitStream *destructionBitstream, Connection_RM3 *destinationConnection){}

};
#endif
