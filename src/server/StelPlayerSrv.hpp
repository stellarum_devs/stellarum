#ifndef STELPLAYER_SRV_H_
#define STELPLAYER_SRV_H_

#include "../StelPlayer.hpp"
#include "StelRemoteController.hpp"
#include "StelUniverseSrv.hpp"
#include "StelGenkiBallSrv.hpp"

/**
* Main game class.
*/
class StelPlayerSrv : public StelNetObjSrv, public StelPlayer
{
	public:
		StelPlayerSrv();
		~StelPlayerSrv();

		void initialize();
		void update(float elapsedTime);

		void animationEvent(AnimationClip* clip, AnimationClip::Listener::EventType type);

		void addEnergy(float ammount);
		void takeDamage(int dmg, uint64_t dmgGUID);
		void setCameraRot(Vector3 cam) {_cameraForward = cam;}
		Vector3 getCameraFw() {return _cameraForward;}

		void startUsingObj() override;
		void stopUsingObj() override;
		bool checkUsedNodeInRange();
		
	private:
		bool _healthDirty, _energyDirty;
		Vector3 _cameraForward;

		// Usable interaction stuff
		UsableHitFilter _usableHF;
		StelUsable* _usedObject;
		float _lastUsedCheck;
		Node* _usedObjectNode; // es un poco mierder tener esto..
		bool _usingObject;

		// Replica3
		virtual RM3SerializationResult Serialize(SerializeParameters *serializeParameters);
		virtual void SerializeConstruction(BitStream *constructionBitstream, Connection_RM3 *destinationConnection);
		virtual void SerializeDestruction(BitStream *destructionBitstream, Connection_RM3 *destinationConnection){}		
};

#endif
