#include "StelRemoteController.hpp"
#include "StelServer.hpp"
#include "StelPlayerSrv.hpp"

StelRemoteController::StelRemoteController(uint64_t guid) : StelController(guid)
{
}

void StelRemoteController::processCmds(ControllerCmdsPkg* pkg)
{
	_keyFlags = pkg->flags;

	_player->getPlayerNode()->setRotation(pkg->rot);

	if (_keyFlags & StelKeyFlags::FIRE || _keyFlags & StelKeyFlags::ALT_FIRE)
		((StelPlayerSrv*)_player)->setCameraRot(pkg->cam);

	if (_keyFlags & StelKeyFlags::FIRE)
	{
		_player->startCharging();
	}
	else
	{
		_player->stopCharging();
	}

	if (_keyFlags & StelKeyFlags::ALT_FIRE)
	{
		_player->startUsingObj();
	}
	else
	{
		_player->stopUsingObj();
	}
}

TeamInfoPkg* StelRemoteController::createTeamInfoPkg() const {
	TeamInfoPkg* p = new TeamInfoPkg();

	p->_myID = _teamID;
	p->_teamMap = StelTeamMgr::getInstance().getTeamMap();
	p->_guid = _guid;
	return p;
}

void StelRemoteController::fireGenkiBall()
{
	StelGenkiBallSrv* ball = new StelGenkiBallSrv();
	ball->initialize(
		_player->getPlayerNode()->getTranslationWorld(),
		((StelPlayerSrv*)_player)->getCameraFw().normalize()*10,
		_player->getChargeTimeGenki());

	// set for checking colls against our player
	ball->setOwnerGUID(_guid);
	ball->setOwnerTeamID(_teamID);

	//Maybe not the cleanest way
	StelServer* game = (StelServer*)Game::getInstance();
	float spentE = ball->getBallEnergy();
	_player->decreaseEnergy(spentE);
	game->getUniverse()->spawnGenkiBall(ball);
}