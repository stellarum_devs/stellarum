#ifndef STELREMOTECONTROLLER_H_
#define STELREMOTECONTROLLER_H_

#include "../StelController.hpp"
#include "StelPlayerSrv.hpp"

class StelRemoteController : public StelController
{
	public:

		StelRemoteController(uint64_t guid);
		
		void fireGenkiBall() override;

		void processCmds(ControllerCmdsPkg* pkgs) override;

		TeamInfoPkg* createTeamInfoPkg() const;

	private:

		// Server does processCmds instead of these
		void keyEvent(Keyboard::KeyEvent evt, int key){}
		void touchEvent(Touch::TouchEvent evt, int x, int y, unsigned int contactIndex){}
};


#endif