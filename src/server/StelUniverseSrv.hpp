#ifndef STEL_UNIVERSE_SRV_H
#define STEL_UNIVERSE_SRV_H

#include "../StelUniverse.hpp"
#include "StelGenkiBallSrv.hpp"
#include "StelPlayerSrv.hpp"

class StelNetworkSrv;
class StelPlayerSrv;

/**
 * Stellarum main game class.
 */
class StelUniverseSrv : public StelUniverse, public PhysicsCollisionObject::CollisionListener
{
public:

	StelUniverseSrv(StelNetworkSrv* net);
	~StelUniverseSrv();

	void initialize() override;
	void finalize() override;
	
	void update(float elapsedTime) override;

	// collision listener stuff
	void collisionEvent(PhysicsCollisionObject::CollisionListener::EventType type,
		const PhysicsCollisionObject::CollisionPair& collisionPair,
		const Vector3& contactPointA,
		const Vector3& contactPointB);

	/**
	 * Method to be called by the player when collides with a pellet.
	 */ 
	void pickUpEnergy(Node* energyNode);

	void giveTeamEnergy(unsigned short teamID, float energy);

	// Moves player to free spawn location
	void placeOnBase(StelPlayerSrv* player);

	// Spawn net Objects [Add them to network system]
	void spawnBase(StelBase* base);
	void spawnPlayer(StelPlayer* controller);
	void spawnGenkiBall(StelGenkiBall* genkiBall);

	// Remove spawned net Objects [Broadcast destruction]
	void removePlayer(StelPlayer* player);
	void removeGenkiBall(StelGenkiBall* genkiBall);
	void removePellet(StelPellet* pellet);
	
protected:

	//internal methods-------
	// Creates numBases on XZ plane, at distance radius around the sun
	void initBases();
	//for now this method contains all planets configuration
	void initializePlanets();
	//method to spawn energy pellets in the universe
	void initializePellets(std::vector<StelPlanet*>& planets);
	std::vector<StelPellet*> _pellets_to_remove;
	
	StelNetworkSrv* _network;
};

#endif
