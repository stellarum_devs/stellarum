#include "StelPlanetSrv.hpp"
#include "../StelRandom.hpp"
#include "StelGenkiBallSrv.hpp"
#include "StelTotemSrv.hpp"
#include "StelServer.hpp"

StelPlanetSrv::StelPlanetSrv() :
	_lastActionTime(0.0f),
	_captureTeamDirty(false)
{
	_angle = StelRandom::getInstance().getFloat0_1()*MATH_PIX2;

}

void StelPlanetSrv::initialize(const int radius, std::string materialPath, Vector3 orbitCenter, float orbitRadius, Vector3 axis, float velocity, Vector3 rotationVelocity, float gravityRadius)
{
	StelPlanet::initialize(radius, materialPath, orbitCenter, orbitRadius, axis, velocity, rotationVelocity, gravityRadius);

	StelUniverseSrv* universe = ((StelServer*)Game::getInstance())->getUniverse();

	// Create capture totems
	_totems[MODE_ATTACK] = new StelTotemSrv(MODE_ATTACK, this);
	_totems[MODE_DEFEND] = new StelTotemSrv(MODE_DEFEND, this);
	_totems[MODE_ENERGIZE] = new StelTotemSrv(MODE_ENERGIZE, this);
}

void StelPlanetSrv::update(float elapsedTime)
{
	StelPlanet::update(elapsedTime);

	// update position, this is auto replicated
	_position = _node->getTranslation();
	_rotation = _node->getRotation();

	if (_captureMode != MODE_STATIC)
	{
		_lastActionTime += elapsedTime;

		if (_lastActionTime > 10000.0f)
		{
			StelUniverseSrv* universe = ((StelServer*)Game::getInstance())->getUniverse();
			switch (_captureMode)
			{
				case MODE_ENERGIZE:
				{
					universe->giveTeamEnergy(_captureTeamID, 2.f);
					_lastActionTime = 0.f;
					break;
				}
				case MODE_ATTACK:
				{
					Vector3 target = universe->getBases()[(_captureTeamID + 1) % 2]->getNode()->getTranslationWorld(); // TODO: loopo enemy bases ids
					fireVolley(target, Vector3::zero(), 1.f, 4000.f, universe);
					_lastActionTime = 0.f;
					break;
				}
				case MODE_DEFEND:
				{
					for (StelPlayer* p : *universe->getPlayers())
					{
						if (p->getPlayerController()->getTeamID() != _captureTeamID
							&& p->getState() == StelPlayer::PlayerState::JETPACK)
						{
							float dist = p->getPlayerNode()->getTranslationWorld().distance(_node->getTranslationWorld());
							printf("Check near player distance: %f\n", dist);
							if (dist < 1000.f && dist > 250.f) // TODO: depends on planet radius
							{
								fireVolley(p->getPlayerNode()->getTranslationWorld(),
									p->getJetPhysics()->getVelocity() * 2.f, 4.f, 600.f, universe);
								_lastActionTime = 0.f;
							}
						}
					}
					break;
				}
			}
		}
	}
}

void StelPlanetSrv::capture(StelTeamID teamID, StelCaptureMode captureMode)
{
	_captureTeamID = teamID;
	_captureMode = captureMode;
	for (unsigned int t = 0; t < NUM_MODES - 1; t++)
	{
		_totems[t]->stopUsing();
	}

	_captureTeamDirty = true;
}

void StelPlanetSrv::fireVolley(Vector3 targetPos, Vector3 targetVel, float speed, float damage, StelUniverseSrv* uni)
{
	// AArrrr!! ye pirates! get to the CHOPPAAAAAA!
	Vector3 origin = _turretNode->getTranslationWorld();
	Vector3 aim = aimTarget(origin, targetPos, targetVel, speed);

	StelGenkiBallSrv* genki = new StelGenkiBallSrv();
	genki->initialize(origin, aim, damage);

	// set for checking colls against our player
	genki->setOwnerGUID(UNASSIGNED_TEAMID); // good enough for now
	genki->setOwnerTeamID(_captureTeamID);

	uni->spawnGenkiBall(genki);
}

void StelPlanetSrv::SerializeConstruction(BitStream *bs, Connection_RM3 *destinationConnection)
{
	bs->Write((float) _angle);
	bs->Write((float) _sphereRadius);
	RakString tempRs = RakString(_materialPath.c_str());
	bs->Write((RakNet::RakString) tempRs);
	bs->Write((Vector3) _orbitCenter);
	bs->Write((float) _orbitRadius);
	bs->Write((Vector3) _axis);
	bs->Write((float) _velocity);
	bs->Write((Vector3) _rotationVelocity);
	bs->Write((float) _gravityRadius);
}

void StelPlanetSrv::PostSerializeConstruction(RakNet::BitStream *constructionBitstream, RakNet::Connection_RM3 *destinationConnection)
{
	// Add totems to network system after the planets were serialized
	StelServer* game = ((StelServer*)Game::getInstance());
	for (unsigned int t = 0; t < NUM_MODES - 1; t++)
	{
		game->getNetwork()->addNetObject(_totems[t]);
	}
}

RM3SerializationResult StelPlanetSrv::Serialize(SerializeParameters *serializeParameters)
{
	serializeParameters->pro->priority = HIGH_PRIORITY;
	serializeParameters->pro->reliability = UNRELIABLE_SEQUENCED;

	serializeParameters->outputBitstream[0].WriteAlignedBytes((const unsigned char*)&_position, sizeof(_position));
	serializeParameters->outputBitstream[0].WriteAlignedBytes((const unsigned char*)&_rotation, sizeof(_rotation));
	
	if (_captureTeamDirty)
	{
		serializeParameters->outputBitstream[0].Write1();
		serializeParameters->outputBitstream[0].Write(_captureTeamID);
		serializeParameters->outputBitstream[0].Write(_captureMode);
		_captureTeamDirty = false;
	}
	else
	{
		serializeParameters->outputBitstream[0].Write0();
	}

	return RM3SR_BROADCAST_IDENTICALLY;
}
