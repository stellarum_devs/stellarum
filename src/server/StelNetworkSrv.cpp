#include "StelNetworkSrv.hpp"
#include "StringTable.h"
#include "../StelNetPackages.hpp"

void StelNetworkSrv::initNetwork(const char* ipAddress)
{
	_peer = RakNet::RakPeerInterface::GetInstance();
	// For tweaking later
	//_peer->SetTimeoutTime(5000, UNASSIGNED_SYSTEM_ADDRESS);
	StartupResult sr;
	RakNet::SocketDescriptor sd;
	sd.port = SERVER_PORT;

	printf("Starting the server...\n");
	sr = _peer->Startup(MAX_CLIENTS, &sd, 1);
	RakAssert(sr == RAKNET_STARTED);
	printf("Server Initialized!\n");
	// We need to let the server accept incoming connections from the clients
	_peer->SetMaximumIncomingConnections(MAX_CLIENTS);

	_peer->AttachPlugin(_replicaMgr);
	_replicaMgr->SetNetworkIDManager(_networkIdMgr);
	_replicaMgr->SetAutoSerializeInterval(DEFAULT_SERVER_MILLISECONDS_BETWEEN_UPDATES);

	//_peer->AttachPlugin(&_pktLogger);
	//pktLogger.StartLog("stelLOG");
	//pktLogger.WriteMiscellaneous("INFO", "we are retards");
	_networkStarted = true;
}

void StelNetworkSrv::addNetObject(StelNetObj* obj)
{
	// Tell the replication system about this new Replica instance
	_replicaMgr->Reference(obj);
}

void StelNetworkSrv::update(float elapsedTime)
{
	RakNet::Packet *packet;

	if (!_networkStarted)
		return;

	for (packet = _peer->Receive(); packet; _peer->DeallocatePacket(packet), packet = _peer->Receive())
	{
		switch (packet->data[0])
		{		
		// A client has successfully connected.
		case ID_NEW_INCOMING_CONNECTION:
		{
			printf("New client connected\n");
			// Save new client's info
			StelNetPackage* p = new StelClientJoinedPackage();
			p->_guid = _peer->GetGuidFromSystemAddress(packet->systemAddress).g;
			//queue to be procesed by game
			_packageQueuein.push_back(std::make_pair(0, p));
			break;
		}
		// The system specified in Packet::systemAddress has disconnected from us.
		case ID_DISCONNECTION_NOTIFICATION:
		{
			printf("A client has disconnected\n");
			StelNetPackage* p = new StelClientLeftPackage();
			p->_guid = packet->guid.g;
			//queue to be procesed by game
			_packageQueuein.push_back(std::make_pair(0, p));
			break;
		}
		// The connection to that system has been lost
		case ID_CONNECTION_LOST:
		{
			printf("A client has lost connection\n");
			StelNetPackage* p = new StelClientLeftPackage();
			p->_guid = packet->guid.g;
			//queue to be procesed by game
			_packageQueuein.push_back(std::make_pair(0, p));
			break;
		}
		
		// Stel messages from clients
		case ID_STEL_GAME_MSG:
		{
			RakNet::RakString rs;
			RakNet::BitStream bsIn(packet->data, packet->length, false);
			bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
			StelNetPkgID id;
			bsIn.Read(id);
			StelNetPackage* p = getNetPckFactory(id)();
			p->read(bsIn);
			p->_guid = _peer->GetGuidFromSystemAddress(packet->systemAddress).g;

			//queue to be procesed by game
			_packageQueuein.push_back(std::make_pair(id, p));
		}
			break;
		default:
			printf("Unhandled net message has arrived, id: %d\n", packet->data[0]);
			break;
		}
	}

	// Send pending packages to clients
	sendOutPackages();
}