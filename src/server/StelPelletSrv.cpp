#include "StelPelletSrv.hpp"
#include "../StelPlayer.hpp"
#include "../StelPlanet.hpp"

StelPelletSrv::StelPelletSrv(Vector3 pos, StelPlanet* parentPlanetNode)
{
	_node = Node::create();
	_node->setTag("StelClass", "StelPellet");
	_node->setUserPointer(this);
	_planet = parentPlanetNode;
	_position = pos;
	parentPlanetNode->getNode()->addChild(_node);
	_node->setCollisionObject(PhysicsCollisionObject::GHOST_OBJECT, PhysicsCollisionShape::sphere(_node->getBoundingSphere().radius),NULL,STEL_PLAYER_COLLMASK);
	initialize();
}

StelPelletSrv::~StelPelletSrv()
{
}

void StelPelletSrv::update(float elapsedTime)
{
	StelPellet::update(elapsedTime);
}

void StelPelletSrv::SerializeConstruction(BitStream *constructionBitstream, Connection_RM3 *destinationConnection)
{
	constructionBitstream->WriteVector(_position.x, _position.y, _position.z);
	constructionBitstream->Write(_planet->GetNetworkID());
}

RM3SerializationResult StelPelletSrv::Serialize(SerializeParameters *serializeParameters)
{
	return RM3SR_NEVER_SERIALIZE_FOR_THIS_CONNECTION;
}