#ifndef STEL_SERVER_H_
#define STEL_SERVER_H_

#include "gameplay.h"
#include "StelUniverseSrv.hpp"
#include "StelNetworkSrv.hpp"
#include "StelRemoteController.hpp"
#include "../StelNetSetup.hpp"

using namespace gameplay;

/**
 * Stellarum main game class.
 */
class StelServer : public Game, public StelPackageListener
{
public:

	StelServer();
	~StelServer();

	StelUniverseSrv* getUniverse() {return _universe;}
	void addController(uint64_t guid, std::string name);
	void removeController(uint64_t guid);
	StelRemoteController* getController(uint64_t guid) { return _remoteControllerMap[guid]; }
	int getNumPlayersByTeamId( StelTeamID teamId, bool onlyAlive=false);
	void processCmds (ControllerCmdsPkg* cmdPkg);
	void processTeamInfo(TeamInfoPkg* teamInfoPkg){/*CLIENT ONLY*/};
	void  updateGameState ( StelGameStateChangePkg* gamestatePkg ) {/*CLIENT ONLY*/};

	inline StelNetworkSrv* getNetwork(){ return _network; }
	
	StelGameState getGameState(){return _gameState;}
	void baseDestroyed(StelTeamID team, StelTeamID byTeam);
	void playersDestroyed(StelTeamID loser);
	void timeUp();
	
protected:

	void initialize();
	void finalize();
	void update(float elapsedTime);

private:
	unsigned int _lastAssignedTeam;

	void processNetQueue();

	StelUniverseSrv* _universe;
	std::map<uint64_t, StelRemoteController*> _remoteControllerMap;

	// Network stuff
	StelNetworkSrv* _network;

	// Not used on the server
	void render(float elapsedTime){};
	
	//Server / Game state
	float _gameDuration;
	float _upTime;
	float _lateTime;
	StelGameState _gameState;

	
};

#endif
