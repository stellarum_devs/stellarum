#ifndef STELBASESRV_H_
#define STELBASESRV_H_

#include "../StelBase.hpp"
#include "StelPlayerSrv.hpp"

class StelBaseSrv : public StelNetObjSrv, public StelBase
{
public:
	StelBaseSrv();
	virtual ~StelBaseSrv();

	void update(float elapsedTime);

	void initialize(StelTeamID teamID, int hitPoints, Vector3 pos, int numSpawnLocs, const char* baseType);

	void placeOnSpawn(StelPlayerSrv* player);

	void takeDamage(int dmg, StelTeamID byTeamID, uint64_t byGUID);

protected:
	std::vector<std::pair<Vector3, Quaternion>> _spawnLocations;
	std::string _baseType;

	unsigned int _lastSpawnLocUsed;

	// Replica3
	RM3SerializationResult Serialize(SerializeParameters *serializeParameters);
	void SerializeConstruction(BitStream *constructionBitstream, Connection_RM3 *destinationConnection);
	void SerializeDestruction(BitStream *destructionBitstream, Connection_RM3 *destinationConnection){}

};

#endif
