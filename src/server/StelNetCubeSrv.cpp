#include "StelNetCubeSrv.hpp"
#include "../StelRandom.hpp"
#include "../StelPrimitives.hpp"
#include "StelUniverseSrv.hpp"

static const float DOWNWARD_ACCELERATION = -15.0f;
static const float POSITION_VARIANCE = 100.0f;

StelNetCubeSrv::StelNetCubeSrv()
{
	//copied from old factory
	StelRandom* rand = &StelRandom::getInstance();
	// Server sets up initial positions, etc
	_position.x = -POSITION_VARIANCE / 2.0f + rand->getFloat1_2()*POSITION_VARIANCE;
	_position.y = 0.0f;
	_position.z = -POSITION_VARIANCE / 2.0f + rand->getFloat1_2()*POSITION_VARIANCE;
	Quaternion::createFromAxisAngle(Vector3(-1.0f + rand->getFloat1_2()*2.0f, -1.0f + rand->getFloat1_2()*2.0f, -1.0f + rand->getFloat1_2()*2.0f).normalize(), rand->getFloat1_2()*6.28f, &_rotation);
	Quaternion::createFromAxisAngle(Vector3(-1.0f + rand->getFloat1_2()*2.0f, -1.0f + rand->getFloat1_2()*2.0f, -1.0f + rand->getFloat1_2()*2.0f).normalize(), rand->getFloat1_2()*6.28f, &_rotationalVelocity);
}

StelNetCubeSrv::~StelNetCubeSrv()
{
}

void StelNetCubeSrv::update(float elapsedTime)
{
	// Only the server is doing physics
	float timeElapsedSec = elapsedTime * .001f;
	_position += _velocity * timeElapsedSec + .5f * Vector3(0.0f, DOWNWARD_ACCELERATION, 0.0f) * timeElapsedSec*timeElapsedSec;
	_velocity += Vector3(0.0f, DOWNWARD_ACCELERATION, 0.0f) * timeElapsedSec;
	Quaternion dest = _rotation * _rotationalVelocity;
	Quaternion::slerp(_rotation, dest, timeElapsedSec, &_rotation);
	_netCubeNode->setTranslation(_position);
	_netCubeNode->setRotation(_rotation);
}

RM3SerializationResult StelNetCubeSrv::Serialize(SerializeParameters *serializeParameters)
{
	// Autoserialize causes a network packet to go out when any of these member variables change.
	serializeParameters->outputBitstream[0].WriteVector(_position.x, _position.y, _position.z);
	serializeParameters->outputBitstream[0].WriteVector(_rotation.x, _rotation.y, _rotation.z);
	serializeParameters->outputBitstream[0].WriteVector(_velocity.x, _velocity.y, _velocity.z);
	return RM3SR_BROADCAST_IDENTICALLY;
}