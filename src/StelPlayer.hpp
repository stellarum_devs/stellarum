#ifndef STELPLAYER_H_
#define STELPLAYER_H_

#include "gameplay.h"
#include "StelJetPhysics.hpp"
#include "StelUniverse.hpp"
#include "StelGenkiBall.hpp"

using namespace gameplay;

class StelController;

static const int STEL_PLAYER_COLLMASK = 3;

const int PLAYER_INITIAL_HEALTH = 3;
const float PLAYER_INITIAL_ENERGY = 50.f;
const int PLAYER_MAX_HEALTH = 10;
const float PLAYER_MAX_ENERGY = 100.f;
const float PLAYER_USE_DIST = 100.f;
const float PLAYER_CAM_OFFSET = 3.5f;

class StelPlayer : virtual public StelNetObj, public AnimationClip::Listener
{	  
	friend class StelController;

	public:
		enum PlayerState{
			JETPACK,
			ON_PLANET,
			BLACK_HOLE
		};
		
		StelPlayer();
		virtual ~StelPlayer();
		virtual void initialize();
		void playAnim(const char* id, bool repeat, float speed = 1.0f);
		virtual void update(float elapsedTime);
		void animationEvent(AnimationClip* clip, AnimationClip::Listener::EventType type);

		uint64_t getGuid() {return _guid;}

		inline Node* getPlayerNode(){ return _playerNode; }
		inline StelController* getPlayerController(){ return _playerController; }
		inline PhysicsCharacter* getPhysicsCharacter(){return _playerPhysics;}
		inline StelJetPhysics* getJetPhysics(){ return _jetPhysics; }
		void setGravityParams(Node* gravitySource, float gravityFieldRadius, bool inAGravityField = true);

		inline float getEnergy() const { return _energy; }
		inline float getEnergyInUse() const {return _energyInUse;}
		inline int getHealth() const { return _health; }

		inline void setName(std::string name){ _name = name; }
		inline std::string getName(){ return _name; }

		inline float getChargeTimeGenki() { return _chargeTime; }
		inline bool getChargingGenki() { return _chargingGenkiBall; }
		inline void setChargeTimeGenki(float time) { _chargeTime; }
		inline void setChargingGenki(bool b) { _chargingGenkiBall = b; }
		virtual void startCharging();
		virtual void stopCharging();
		void setState(PlayerState state);
		PlayerState getState() {return _state;}
		void decreaseEnergy ( float spentE ) {
			_energy = spentE >= _energy ? 0.0f : _energy-spentE;
		}

		virtual void startUsingObj() = 0;
		virtual void stopUsingObj() = 0;

		// Network stuff
		std::string getAllocID()const { return "Player"; }
	
		Vector3 _position;
		Vector3 _velocity;
		Quaternion _rotation;

	protected:
		Scene* _scene;
		void setupOnPlanetNode(float elapsedTime);
		void followPlanet(float elapsedTime);
		void alignToSurface(float elapsedTime);
		void currentDirectionFromInput();
		void updateVelocity();

		Node* _playerNode;
		Node* _onPlanetNode; //auxiliar Node for weird stuff
		Node* _cameraNode; // only used on the server for usable raypick
		Vector3 _onPlanetNodeLast;
		Node* _currentGravitySource;
		unsigned int _controllerFlags;
		PhysicsCharacter* _playerPhysics;
		StelJetPhysics* _jetPhysics;
		StelController* _playerController;
		Vector2 _currentDirection;
		Animation* _playerAnimation;
		AnimationClip* _currentAnimClip;
		AnimationClip* _jumpClip;
		PlayerState _state;

		// Ray for picking usable objects
		Ray _pickRay;
		
		// Player vars
		int _health;
		float _energy;
		std::string _name;

		//GenkiBall stuff
		bool _chargingGenkiBall;
		float _chargeTime;
		float _energyInUse;

		// Network
		uint64_t _guid;
};

#endif
