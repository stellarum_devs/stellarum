#include "StelBase.hpp"

const int StelBase::MAX_HITPOINTS = 500;

StelBase::StelBase() :
_teamID(UNASSIGNED_TEAMID),
_hitPoints(MAX_HITPOINTS)
{
}

StelBase::~StelBase()
{
}

void StelBase::initialize(StelTeamID teamID, int hitPoints)
{
	_teamID = teamID;
	_hitPoints = hitPoints;
}

void StelBase::update(float elapsedTime)
{

}
