﻿#include "StelPlayer.hpp"
#include "StelController.hpp"

class PlanetRayFilter : public PhysicsController::HitFilter
{
public:
	PhysicsCollisionObject* _target;
	 PlanetRayFilter(PhysicsCollisionObject* target): _target(target) {}
	
	bool filter ( PhysicsCollisionObject* object ) {
		return object != _target;
	}
	
};


StelPlayer::StelPlayer()
: StelNetObj(),
_playerController(NULL),
_currentDirection(Vector2::zero()),
_currentGravitySource(NULL),
_currentAnimClip(NULL),
_jumpClip(NULL),
_playerNode(NULL),
_jetPhysics(NULL),
_guid(0),
_name("bitch"),
_controllerFlags(0),
_velocity(Vector3::zero()),
_position(Vector3::zero()),
_rotation(Quaternion::identity()),
_state(JETPACK),
_chargingGenkiBall(false),
_chargeTime(0),
_energyInUse(0),
_onPlanetNode(NULL),
_health(PLAYER_INITIAL_HEALTH),
_energy(PLAYER_INITIAL_ENERGY),
_cameraNode(NULL)
{
	initialize();
}

StelPlayer::~StelPlayer()
{
	SAFE_RELEASE(_playerNode);
	SAFE_RELEASE(_onPlanetNode);
	SAFE_RELEASE(_cameraNode);

	delete _jetPhysics;
}

void StelPlayer::initialize()
{
	_scene = Scene::getScene();
}

void StelPlayer::currentDirectionFromInput()
{
	_currentDirection.set(Vector2::zero());
	if (_currentDirection.isZero())
	{
		// Construct direction vector from keyboard input
		if (_controllerFlags & StelKeyFlags::MOVE_FORWARD)
			_currentDirection.y = 1;
		else if (_controllerFlags & StelKeyFlags::MOVE_BACKWARD)
			_currentDirection.y = -1;
		else
			_currentDirection.y = 0;

		if (_controllerFlags & StelKeyFlags::MOVE_LEFT)
			_currentDirection.x = -1;
		else if (_controllerFlags & StelKeyFlags::MOVE_RIGHT)
			_currentDirection.x = 1;
		else
			_currentDirection.x = 0;

		_currentDirection.normalize();		
	}
}

void StelPlayer::updateVelocity()
{
	// Update velocity
	if (_currentDirection.isZero())
	{
		_playerPhysics->setVelocity(Vector3::zero());
	}
	else
	{
		if ((_controllerFlags & StelKeyFlags::MOVE_RUN))
			_currentDirection *= 0; // esto es wip

		bool running = (_currentDirection.lengthSquared() > 0.75f);
		float speed = running ? _playerController->getRunSpeed() : _playerController->getWalkSpeed();

		// Update the character's velocity
		Vector3 velocity = _playerNode->getForwardVectorWorld()*_currentDirection.y;
		velocity += _playerNode->getRightVectorWorld()*_currentDirection.x;
		velocity.normalize();
		velocity *= speed;
		_playerPhysics->setVelocity(velocity);
	}
}


void StelPlayer::alignToSurface(float elapsedTime) {
	if(_currentGravitySource) {
		Vector3 v = _playerNode->getTranslationWorld();
		Vector3 l = _playerNode->getLeftVector().normalize();
		Vector3 t = _currentGravitySource->getTranslationWorld();
		
		Vector3 direction =t-v; //TODO 
		if(direction.normalize().length() <= 0) return;
		direction =direction.normalize();
		
		Vector3 start = _playerNode->getTranslationWorld()-direction*30;;
		//TODO alguna cosa de aquí se podra dejar fuera ya preparada
		Ray ray;
		ray.setOrigin(start);
		ray.setDirection(direction);
		PhysicsController::HitResult hitResult;
		PlanetRayFilter filter(_currentGravitySource->getCollisionObject());
		bool hit = Game::getInstance()->getPhysicsController()->rayTest(ray,70,&hitResult,&filter);
		if(!hit) return;
		
		Vector3 normal = hitResult.normal.normalize();
		_playerPhysics->setOverrideGravity(-normal*10);
		Vector3 y, right, fw;
		
		y = Vector3(0,1,0);
		if( fabsf( normal.y ) - fabsf( y.x ) < 0.01f )
		{ 		
			//if normal is almost equal to (0,1,0) then set y to (0,0,1)
			y = Vector3( 0, 0, 1 );
		}
		Vector3::cross(normal, l, &fw);
		Vector3::cross(normal, fw, &right);
		
		Matrix m = Matrix(
			right.x, 	normal.x, fw.x, 	0,
			right.y, 	normal.y, fw.y, 	0,
			right.z, 	normal.z, fw.z, 	0,
			0,		0,		0,		1);
		
		Quaternion b = _playerNode->getRotation();
		
		Quaternion q;
		m.getRotation(&q);
		q.normalize();
		//descomentar y jugar con esto si saltos etc...
		b.slerp(b, q, std::fmin(elapsedTime*0.001f, 0.999f), &q);
		_playerNode->setRotation(q);
	}
}

void StelPlayer::followPlanet ( float elapsedTime )
{
	if(_onPlanetNode) {
		Vector3 dif = _onPlanetNode->getTranslationWorld()-_onPlanetNodeLast;
		_onPlanetNodeLast = _onPlanetNode->getTranslationWorld();
		Vector3 pos = _playerNode->getTranslationWorld();
		Vector3 dest = pos+ dif;
		//pos.smooth(dest,elapsedTime,10);
		_playerNode->setTranslation(dest);
	}
}


void StelPlayer::setupOnPlanetNode ( float elapsedTime )
{
	if(_onPlanetNode == NULL) {
		_onPlanetNode = Node::create("aux");
		_onPlanetNode->setTranslation(_playerNode->getTranslation());
		_onPlanetNode->setRotation(_playerNode->getRotation());
			
		Quaternion q = _currentGravitySource->getRotation();
		Quaternion o = _onPlanetNode->getRotation();
		q.conjugate();
		Matrix m = Matrix::identity();
		m.rotate(q);
		Vector3 pos = _onPlanetNode->getTranslation()-_currentGravitySource->getTranslation();
		_currentGravitySource->addChild(_onPlanetNode);
		m.transformVector(&pos);
		_onPlanetNode->setTranslation(pos);
		q = q*o;
		_onPlanetNode->setRotation(q);
		_onPlanetNodeLast = _onPlanetNode->getTranslationWorld();
		
	}
}


void StelPlayer::update(float elapsedTime)
{	
	_controllerFlags = _playerController? _playerController->getKeyFlags(): 0;
	currentDirectionFromInput();
	updateVelocity();
	//TODO debug
	//printf("State:%s, PhysEnabled:%d\n",_state==ON_PLANET?"on_planet" : "jetpack", _playerPhysics->isPhysicsEnabled());
	//printf("Distance:%f, GravityR:%f\n",_playerPhysics->getDistanceToGravityC(), _playerPhysics->getGravityFieldRadius());
	//printf("Planet radius:%f\n",_currentGravitySource->getBoundingSphere().radius);
	followPlanet(elapsedTime);
	switch(_state){
	  case ON_PLANET: 
	  {
		//state actions -------
		_playerPhysics->setPhysicsEnabled(true); //TO be sure
		if (_controllerFlags & StelKeyFlags::JUMP && _playerPhysics->getVerticalVelocity().lengthSquared() < FLT_EPSILON) {
			_playerPhysics->jump(50.f);
		}
		if (_controllerFlags & StelKeyFlags::GAS) {
			_playerPhysics->jump(0.05f);
		}
		alignToSurface(elapsedTime);
		
		//next state actions -------
		if (_playerPhysics->getDistanceToGravityC() > _playerPhysics->getGravityFieldRadius()) {
			_playerPhysics->setPhysicsEnabled(false);
			Vector3 v = _playerPhysics->getVerticalVelocity(); //TODO velocidad absurda!
			v.scale(0.001f);
			_jetPhysics->setVelocity(v);
			_playerPhysics->setVelocity(0.f, 0.f, 0.f);
			_playerPhysics->setVerticalVelocity(Vector3::zero());
			_state = JETPACK;
			_currentGravitySource->removeChild(_onPlanetNode);
			_onPlanetNode = NULL;
			_playerPhysics->clearOverrideGravity();
		}
		
		break;
	  }
	  case JETPACK:  
	  {
		//state actions -------
		_jetPhysics->toggleGas(_controllerFlags & StelKeyFlags::JUMP);
		_jetPhysics->toggleBrake(_controllerFlags & StelKeyFlags::MOVE_BACKWARD);
		if(_controllerFlags & StelKeyFlags::MOVE_LEFT || _controllerFlags & StelKeyFlags::MOVE_RIGHT) {
			_jetPhysics->setRoll(_controllerFlags & StelKeyFlags::MOVE_LEFT ? -1 : 1);
		}
		else {
			_jetPhysics->setRoll(0);
		}
		_jetPhysics->update(elapsedTime);
		//next state actions -------
		if (_playerPhysics->getDistanceToGravityC() <= _playerPhysics->getGravityFieldRadius()) {
			//TODO esto en principio sería bueno, pero no
			Vector3 v = _jetPhysics->getVelocity();
			v.scale(200);
			_playerPhysics->setVerticalVelocity(v);
			//_playerPhysics->setVelocity(v);
			_state=ON_PLANET;
			setupOnPlanetNode(elapsedTime);
		}
		
		break;
	  }
	  case BLACK_HOLE:
	  {
		//state actions -------
		_jetPhysics->toggleGas(_controllerFlags & StelKeyFlags::JUMP);
		_jetPhysics->toggleBrake(_controllerFlags & StelKeyFlags::MOVE_BACKWARD);
		if(_controllerFlags & StelKeyFlags::MOVE_LEFT || _controllerFlags & StelKeyFlags::MOVE_RIGHT) {
			_jetPhysics->setRoll(_controllerFlags & StelKeyFlags::MOVE_LEFT ? -1 : 1);
		}
		else {
			_jetPhysics->setRoll(0);
		}

		_jetPhysics->update(elapsedTime);
		  
	  }
	}

	if (_chargingGenkiBall && _energyInUse <= _energy)
	{
		_chargeTime += elapsedTime;
		_energyInUse = _chargeTime*StelGenkiBall::energyRatio;
	}
}


void StelPlayer::playAnim(const char* id, bool repeat, float speed)
{
	AnimationClip* clip = _playerAnimation->getClip(id);

	// Set clip properties
	clip->setSpeed(speed);
	clip->setRepeatCount(repeat ? AnimationClip::REPEAT_INDEFINITE : 1);

	// Is the clip already playing?
	if (clip == _currentAnimClip && clip->isPlaying())
		return;

	if (_jumpClip->isPlaying())
	{
		_currentAnimClip = clip;
		return;
	}

	// If a current clip is playing, crossfade into the new one
	if (_currentAnimClip && _currentAnimClip->isPlaying())
	{
		_currentAnimClip->crossFade(clip, 150);
	}
	else
	{
		clip->play();
	}
	_currentAnimClip = clip;
}

void StelPlayer::animationEvent(AnimationClip* clip, AnimationClip::Listener::EventType type)
{
	/*if (clip == some_clip)
	{
	 do something related to some_clip
	}
	else
	{
		clip->crossFade(_currentAnimClip, 150);
	}*/
}

void StelPlayer::startCharging() 
{
	if (!_chargingGenkiBall)
	{
		_chargingGenkiBall = true;
		_chargeTime = 0;
	}
}

void StelPlayer::stopCharging() 
{
	if (_chargingGenkiBall)
	{
		_chargingGenkiBall = false;
		if (_playerController && _chargeTime >= 3000)
			_playerController->fireGenkiBall();
		_chargeTime = 0;
		_energyInUse = 0.0f;
	}
}

void StelPlayer::setGravityParams(Node* gravitySource, float gravityFieldRadius, bool inAGravityField) 
{
	_currentGravitySource = gravitySource;
	getPhysicsCharacter()->setGravityParams(gravitySource, gravityFieldRadius, inAGravityField);
}

void StelPlayer::setState ( StelPlayer::PlayerState state )
{
	_state = state;
	if(_state == BLACK_HOLE) {
		_playerPhysics->setVerticalVelocity(Vector3::zero());
		_playerPhysics->setVelocity(Vector3::zero());
		_playerPhysics->setPhysicsEnabled(false);
		_jetPhysics->setBlackHole(true);
	}

}

