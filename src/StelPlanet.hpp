#ifndef STELPLANET_H
#define STELPLANET_H

#include "gameplay.h"
#include "StelDefines.hpp"
#include "StelNetObj.hpp"
#include "StelTotem.hpp"

using namespace gameplay;

//collision group
static const int STEL_PLANET_COLLMASK = 1;

class StelPlanet : virtual public StelNetObj
{

public:
	StelPlanet();
	virtual ~StelPlanet();
	/*does nothing, only assing values and call initialize() */
	virtual void initialize(const int radius,std::string materialPath, Vector3 orbitCenter, float orbitRadius, Vector3 axis, float velocity = 0.0001f, Vector3 rotationVelocity = Vector3(0, 0, 0), float gravityRadius = 40);

	virtual void update ( float elapsedTime );

	Vector3 aimTarget(Vector3 origin, Vector3 targetPos, Vector3 targetVel, float projSpeed);
	
	// Network
	std::string getAllocID()const { return "Planet"; }
	Vector3 _position;
	Quaternion _rotation;

	//testing
	Node* _turretNode;

	//Accesors -------------
	inline Node* getNode() const { return _node; }
	inline Vector3 getOrbitCenter() const { return _orbitCenter; }
	inline float getOrbitRadius() const { return _orbitRadius; }
	inline PhysicsCollisionObject* getCollisionObject() const{ return _collisionObject; }
	inline float getAngle() const { return _angle; }
	inline void setAngle(float f) { _angle = f; }
	inline float getVelocity() const { return _velocity; }
	inline Vector3 getRotationVelocity() const { return _rotationVelocity; }
	inline Vector3 getAxis() const { return _axis; }
	inline float getGravityRadius() const { return _gravityRadius; }
	inline const std::string getMaterialPath() const { return _materialPath; }
	inline float getSphereRadius() const { return _sphereRadius; }
	inline StelCaptureMode getCaptureMode(){ return _captureMode; }
	inline StelTeamID getCaptureTeamID(){ return _captureTeamID; }
	inline void setTotem(StelCaptureMode mode, StelTotem* totem) { _totems[mode] = totem; }
	

protected:
	/* this method is internal : do stuff with values passed in initialize and build the real Planet*/
	virtual void initialize();

	Node* _node;
	float _sphereRadius;
	Vector3 _orbitCenter;
	float _orbitRadius;
	PhysicsCollisionObject* _collisionObject;
	float _angle;
	float _velocity;
	Vector3 _rotationVelocity;
	Vector3 _axis;
	float _gravityRadius;
	std::string _materialPath;

	StelTeamID _captureTeamID;
	StelCaptureMode _captureMode;

	StelTotem* _totems[NUM_MODES-1];

};

#endif // STELPLANET_H
