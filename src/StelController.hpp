#ifndef STELCONTROLER_H_
#define STELCONTROLER_H_

#include "gameplay.h"
#include "StelPlayer.hpp"
#include "StelBase.hpp"
#include "StelNetPackages.hpp"

using namespace gameplay;

namespace StelKeyFlags
{
	static const unsigned int MOVE_FORWARD 		= (1 << 0);
	static const unsigned int MOVE_BACKWARD		= (1 << 1);
	static const unsigned int MOVE_LEFT			= (1 << 2);
	static const unsigned int MOVE_RIGHT		= (1 << 3);
	static const unsigned int MOVE_UP			= (1 << 4);
	static const unsigned int MOVE_DOWN			= (1 << 5);
	static const unsigned int MOVE_RUN			= (1 << 6);
	static const unsigned int GAS				= (1 << 7);
	static const unsigned int FIRE				= (1 << 8);
	static const unsigned int ALT_FIRE			= (1 << 9);
	static const unsigned int JUMP			= (1 << 10);
}

class StelPlayer;
class ControllerCmdsPkg;

class StelController
{	
public:
	StelController(uint64_t guid);
	virtual ~StelController();
	virtual void keyEvent(Keyboard::KeyEvent evt, int key) = 0;
	virtual void touchEvent(Touch::TouchEvent evt, int x, int y, unsigned int contactIndex) = 0;
	virtual void possess(StelPlayer* player);

	void updateInput(float elapsedTime);
	virtual void fireGenkiBall() = 0;

	//Only to be implemented in the server
	virtual void processCmds(ControllerCmdsPkg* pkgs) {}

	inline StelPlayer* getPlayer() { return _player; }
	inline unsigned int getKeyFlags() { return _keyFlags; }
	inline StelTeamID getTeamID() const { return _teamID; }
	inline float getWalkSpeed() { return _walkSpeed; }
	inline float getStrafeSpeed() { return _strafeSpeed; }
	inline float getRunSpeed() { return _runSpeed; }

	inline void setTeamID(StelTeamID team) { _teamID = team; }

protected:
	StelPlayer* _player;
	unsigned int _keyFlags;
	uint64_t _guid;
	StelTeamID _teamID;

	float _walkSpeed;
	float _strafeSpeed;
	float _runSpeed;
};

//---------- controller commands pkg --------------//

class ControllerCmdsPkg : public StelNetPackage {
public:
	ControllerCmdsPkg() : StelNetPackage(StelNetPkgID::CONTROLLER_CMDS, HIGH_PRIORITY, RELIABLE_SEQUENCED){}
	unsigned int flags;
	Quaternion rot;
	Vector3 cam;
	
	virtual void write(RakNet::BitStream& bs);
	virtual void read(RakNet::BitStream& bs);
	virtual void execute(StelPackageListener* listener) override;
};

//---------- Team info pkg --------------//

class TeamInfoPkg : public StelNetPackage {
public:
	TeamInfoPkg() : StelNetPackage(StelNetPkgID::TEAM_INFO, MEDIUM_PRIORITY, RELIABLE, true){}
	
	StelTeamID _myID;
	std::map<StelTeamID, StelTeamMgr::TeamInfo> _teamMap;

	virtual void write(RakNet::BitStream& bs);
	virtual void read(RakNet::BitStream& bs);
	virtual void execute(StelPackageListener* listener) override;
};

#endif