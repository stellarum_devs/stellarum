#include "StelGenkiBall.hpp"
#include "StelPrimitives.hpp"

const float StelGenkiBall::energyRatio = 0.005f;
const float StelGenkiBall::radiusRatio = 1.0f;
const float StelGenkiBall::damageRatio = 1.0f;

StelGenkiBall::StelGenkiBall() : 
	_genkiBallNode(NULL),
	_position(Vector3::zero()),
	_velocity(Vector3::zero()),
	_chargeTime(0.f),
	_energy(0.f)
{
	// Initialize genkiBall node
	_genkiBallNode = Scene::getScene()->addNode("genkiballNode");
	
	// Create the model
	Mesh* mesh = createSphereMesh(1.0, 4, 8);
	Model* model = Model::create(mesh);
	_genkiBallNode->setModel(model);
	
	SAFE_RELEASE(mesh);
	SAFE_RELEASE(model);
}

StelGenkiBall::~StelGenkiBall()
{
}

void StelGenkiBall::initialize(Vector3 pos, Vector3 vel, float chargeTime)
{
	_chargeTime = chargeTime;
	_energy = _chargeTime * energyRatio;
	_position = pos;
	_velocity = vel;
}

void StelGenkiBall::update(float elapsedTime)
{
	_position += _velocity * elapsedTime * .1f;
	_genkiBallNode->setTranslation(_position);
}
