#include "StelController.hpp"
#include "StelPlayer.hpp"
#include "StringCompressor.h"

StelController::StelController(uint64_t guid) :
_player(NULL),
_keyFlags(0),
_guid(guid),
_walkSpeed(20.0f),
_strafeSpeed(1.5f),
_runSpeed(50.0f),
_teamID(UNASSIGNED_TEAMID)
{
}

StelController::~StelController()
{
}

void StelController::possess(StelPlayer* player) 
{
	_player = player;
	_player->_playerController = this;
	_player->_guid = this->_guid;
}


//----------  ControllerCmdsPkg Pckg --------------//

void ControllerCmdsPkg::write(RakNet::BitStream& bs)
{
	bs.Write(flags);
	bs.Write(rot.x);
	bs.Write(rot.y);
	bs.Write(rot.z);
	bs.Write(rot.w);
	if(flags & StelKeyFlags::FIRE || flags & StelKeyFlags::ALT_FIRE) {
		bs.Write(cam.x);
		bs.Write(cam.y);
		bs.Write(cam.z);
	}
};
	
void ControllerCmdsPkg::read(RakNet::BitStream& bs)
{
	bs.Read(flags);
	bs.Read(rot.x);
	bs.Read(rot.y);
	bs.Read(rot.z);
	bs.Read(rot.w);
	if(flags & StelKeyFlags::FIRE || flags & StelKeyFlags::ALT_FIRE) {
		bs.Read(cam.x);
		bs.Read(cam.y);
		bs.Read(cam.z);
	}
};

void ControllerCmdsPkg::execute(StelPackageListener* listener)
{
	listener->processCmds(this);
}

//---------- Team info pkg --------------//

void TeamInfoPkg::write(RakNet::BitStream& bs)
{
	bs.Write(_myID);
	unsigned short numTeams = _teamMap.size();
	bs.Write(numTeams);
	for (auto iter : _teamMap)
	{
		bs.Write(iter.first);
		bs.WriteVector(iter.second._teamColor.x, iter.second._teamColor.y, iter.second._teamColor.z);
		StringCompressor::Instance()->EncodeString(iter.second._teamName.c_str(), 64, &bs);
	}
};

void TeamInfoPkg::read(RakNet::BitStream& bs)
{
	bs.Read(_myID);
	unsigned short numTeams;
	bs.Read(numTeams);
	StelTeamID id;
	Vector3 color;
	char name[64];
	for (unsigned int t = 0; t < numTeams; t++)
	{
		bs.Read(id);
		bs.ReadVector(color.x, color.y, color.z);
		StringCompressor::Instance()->DecodeString(name, 64, &bs);
		_teamMap[id] = StelTeamMgr::TeamInfo(name, color);
	}
};

void TeamInfoPkg::execute(StelPackageListener* listener)
{
	// this will be executed on clients only
	listener->processTeamInfo(this);
}