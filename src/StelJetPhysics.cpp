
#include "StelJetPhysics.hpp"



StelJetPhysics::StelJetPhysics(Node* playerNode) 
: _velocity(0,0,0), _gasOn(false), _brakeOn(false), _blackHole(false), _blackHolePos(0.0f,0.0f,0.0f), _roll(0), _rotationV(0.0f,0.0f,0.0f)
{
	_node = playerNode;
}

StelJetPhysics::~StelJetPhysics()
{
	
}

void StelJetPhysics::toggleGas(bool enabled)
{
	_gasOn = enabled;
}

void StelJetPhysics::toggleBrake(bool enabled) 
{
	_brakeOn = enabled;
}

void StelJetPhysics::update(float elapsedTime) 
{
	
	if(_gasOn && !_brakeOn) {
		//sanity maximum: 100
		Vector3 increment = _node->getForwardVectorWorld();
		if(_velocity.lengthSquared() < 0.5 ) {
			increment.scale(0.001f);
			_velocity.add(increment);
		}
	}
	else {
		if(_brakeOn) {
			_velocity.smooth(Vector3::zero(),elapsedTime,1000);
		}
		else {
			if(_velocity.lengthSquared() > 0.005f ) {
				_velocity.smooth(Vector3::zero(), elapsedTime, 1000);
			}
		}
	}
	
	if(_blackHole) {
		Vector3 vhole = _blackHolePos-_node->getTranslationWorld().normalize();
		vhole.scale(0.0001f);
		_velocity+=vhole;
	}
	
	_node->setTranslation(_node->getTranslation()+_velocity*elapsedTime);
	
	
	//rotation update very dirty, rewrite plz!
	if(_roll > 0) {
		_rotationV.add(Vector3(0,0,0.0001f));
	}
	if(_roll < 0) {
		_rotationV.add(Vector3(0,0,-0.0001f));
	}
	if(_brakeOn) {
		_rotationV.smooth(Vector3::zero(),elapsedTime,100);
	}
	else {
		if(_rotationV.lengthSquared() > 0.0005f ) {
			_rotationV.smooth(Vector3::zero(), elapsedTime, 100);
		}
	}
	_node->rotateZ(_rotationV.z);
	
	
	
}

