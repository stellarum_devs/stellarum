#ifndef STEL_DEFINES_H
#define STEL_DEFINES_H

#include "gameplay.h"

using namespace gameplay;

enum StelNetPkgID
{
	CLIENT_JOINED,		// Used internally, isn't actually sent or received
	CLIENT_LEFT,		// Used internally, isn't actually sent or received
	CONTROLLER_CMDS,	// Conatains player input received from each client
	TEAM_INFO,			// Sends team info to a player after they join
	GAME_STATE			// Broadcasts game state changes
};

enum StelCaptureMode
{
	MODE_ATTACK = 0,
	MODE_DEFEND,
	MODE_ENERGIZE,
	MODE_STATIC, // Keep this one last!
	NUM_MODES
};



enum StelGameState
{
	NORMAL,
	LATE_GAME,
	GAME_OVER,
};


typedef unsigned short StelTeamID;
static const StelTeamID UNASSIGNED_TEAMID = 666;
static const int SUNRADIUS = 100;

// StelTeam esta aqui por ahora
class StelTeamMgr
{

public:
	// Team info container
	struct TeamInfo
	{
		TeamInfo() : _teamName("Error"), _teamColor(Vector3::zero()){}
		TeamInfo(std::string name, Vector3 color)
			: _teamName(name), _teamColor(color) {}
		Vector3 _teamColor;
		std::string _teamName;
	};

	std::map<StelTeamID, TeamInfo> getTeamMap(){ return _teamMap; }
	void setTeamMap(std::map<StelTeamID, TeamInfo> teams){ _teamMap = teams; }
	TeamInfo getTeamInfo(StelTeamID id) { return _teamMap[id]; }
	void setTeamInfo(StelTeamID id, std::string name, Vector3 color)
	{
		_teamMap[id] = TeamInfo(name, color);
	}
	/*
	StelTeamID registerTeam(std::string name, Vector3 color)
	{
		_teamMap[_lastUsedID++] = TeamInfo(name, color);
		return _lastUsedID;
	}*/

	// SINGLETON
	static StelTeamMgr& getInstance()
	{
		static StelTeamMgr instance; // Guaranteed to be destroyed.                     
		return instance; // Instantiated on first use.
	}

private:
	std::map<StelTeamID, TeamInfo> _teamMap;
	StelTeamID _lastUsedID = 0;

	// Construct destruct
	StelTeamMgr(){};
	~StelTeamMgr(){};
	// Dont forget to declare these two. You want to make sure they
	// are unaccessable otherwise you may accidently get copies of
	// your singleton appearing.
	StelTeamMgr(StelTeamMgr const&);    // Don't Implement
	void operator=(StelTeamMgr const&); // Don't implement
};

class StelUsable
{
public:
	StelUsable() : _inUse(false){}

	virtual void startUsing(void* who) { _inUse = true; }
	virtual void stopUsing() { _inUse = false; }
	virtual void lostUser() { _inUse = false; }

	bool _inUse;
};

// Retrun True if the object should be filtered out, 
// or false to include the object in the test(default).
class UsableHitFilter : public PhysicsController::HitFilter
{
public:
	UsableHitFilter() : _ignoredNode(NULL) {}

	inline void setIgnoredNode(Node* ignored) { _ignoredNode = ignored; }

	bool filter(PhysicsCollisionObject* object)
	{
		bool test = (_ignoredNode == object->getNode()) || !object->getNode()->hasTag("Usable");

		printf("TestCol: %s => %s\n", object->getNode()->getId(), test ? "ignored" : "usable!");

		// Filter out ignored node + all not usable
		return (_ignoredNode == object->getNode()) || !object->getNode()->hasTag("Usable");
	}

	Node* _ignoredNode;
};

#endif
