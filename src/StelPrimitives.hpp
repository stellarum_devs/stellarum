#ifndef STELPRIMITIVE_H_
#define STELPRIMITIVE_H_

#include "gameplay.h"

using namespace gameplay;
	
	// Create an incredibly nice sphere Mesh
	Mesh* createSphereMesh(float radius = 1.0, int nRings = 16, int nSlices = 24);

	Mesh* createCubeMesh(const float = 1.0f);

	Mesh* createPrimitive(std::string name);

	Node* createPlanetModel();
	
	/*sets Dst the values of the rotation matrix.
	 	d is the direction vector (tangent to the animation path for instance)
		z is the axis to be rotated (for instance the z -forward- vector of your model)
		dst is a pointer to the result matrix
	*/
	void rotationAlign(const Vector3 & d, const Vector3 & z, Matrix* dst);

#endif
