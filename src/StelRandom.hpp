#ifndef STELRANDOM_H_
#define STELRANDOM_H_

#include "tinymt32.h"
#include <stdio.h>
#include <time.h>

class StelRandom
{
	
	public:
	
	// SINGLETON
	static StelRandom& getInstance()
	{
		static StelRandom instance; // Guaranteed to be destroyed.                     
		return instance; // Instantiated on first use.
	}
	
	tinymt32_t tinymt;
	

	/**
	* This function outputs 32-bit unsigned integer from internal state.
	* @param random tinymt internal status
	* @return 32-bit unsigned integer r (0 <= r < 2^32)
	*/
	inline unsigned int getUnsignedInt0_N() { return tinymt32_generate_uint32(&tinymt); };

	/**
	* This function outputs floating point number from internal state.
	* This function is implemented using multiplying by 1 / 2^32.
	* floating point multiplication is faster than using union trick in
	* my Intel CPU.
	* @param random tinymt internal status
	* @return floating point number r (0.0 <= r < 1.0)
	*/
	inline float getFloat0_1() { return tinymt32_generate_float(&tinymt); };

	/**
	* This function outputs floating point number from internal state.
	* This function is implemented using union trick.
	* @param random tinymt internal status
	* @return floating point number r (1.0 <= r < 2.0)
	*/
	inline float getFloat1_2() { return tinymt32_generate_float12(&tinymt); };
	
	/**
	* This function outputs floating point number from internal state.
	* This function may return 1.0 and never returns 0.0.
	* @param random tinymt internal status
	* @return floating point number r (0.0 < r <= 1.0)
	*/
	inline float getFloatNoZero0_1() { return tinymt32_generate_floatOC(&tinymt); };

	/**
	* This function outputs floating point number from internal state.
	* This function returns neither 0.0 nor 1.0.
	* @param random tinymt internal status
	* @return floating point number r (0.0 < r < 1.0)
	*/
	inline float getFloatNoZeroNoOne0_1() { return tinymt32_generate_floatOO(&tinymt); };

	/**
	* This function outputs double precision floating point number from
	* internal state. The returned value has 32-bit precision.
	* In other words, this function makes one double precision floating point
	* number from one 32-bit unsigned integer.
	* @param random tinymt internal status
	* @return floating point number r (0.0 < r <= 1.0)
	*/
	inline double getDoubleNoZero0_1() { return tinymt32_generate_32double(&tinymt); };


	private:
		StelRandom() {
			/* Set the random seed based on time(NULL) */
			tinymt.mat1 = 0x8f7011ee;
			tinymt.mat2 = 0xfc78ff1f;
			tinymt.tmat = 0x3793fdff;
			unsigned int seed = time(NULL);
			tinymt32_init(&tinymt, seed);
		}
		~StelRandom() {};
		// Dont forget to declare these two. You want to make sure they
		// are unaccessable otherwise you may accidently get copies of
		// your singleton appearing.
		StelRandom(StelRandom const&);    // Don't Implement
		void operator=(StelRandom const&); // Don't implement

};

#endif
