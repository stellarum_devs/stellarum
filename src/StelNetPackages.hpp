#ifndef STELNETPACKAGES_H
#define STELNETPACKAGES_H

#include <vector>
#include <map>
#include <utility> 
#include "RakNetTypes.h"
#include "StelDefines.hpp"
#include "PacketPriority.h"

#include "StelRandom.hpp" // Todo remove later

/*
 *Clase base de todos los paquetes de red, los auto-deserializa 
 * 
 */

// Forward declaration
class StelNetwork;
class ControllerCmdsPkg;
class TeamInfoPkg;
class StelGameStateChangePkg;

//Interface-like parent to be implemented by StelClient or/and SteLServer

class StelPackageListener {
public:
	virtual void addController(uint64_t guid, std::string name) = 0;
	virtual void removeController(uint64_t guid) = 0;
	virtual void processCmds(ControllerCmdsPkg* cmdPkg) = 0;
	virtual void processTeamInfo(TeamInfoPkg* teamInfoPkg) = 0;
	virtual void  updateGameState ( StelGameStateChangePkg* gamestatePkg ) = 0;
};

class StelNetPackage{
public:
	StelNetPackage(StelNetPkgID newid, PacketPriority priority = PacketPriority::HIGH_PRIORITY, 
		PacketReliability reliability = PacketReliability::RELIABLE_ORDERED,
		bool toGuid = false, uint64_t guid = RakNet::UNASSIGNED_RAKNET_GUID.g);
	virtual void write(RakNet::BitStream& myBitStream) = 0;
	virtual void read(RakNet::BitStream& myBitStream) = 0;
	
	virtual void execute(StelPackageListener* listener) {};
	
	/* default send, broadcast */
	void send( StelNetwork* network );
	StelNetPkgID _id;
	uint64_t _guid;
	bool _sendToGuid;
	PacketPriority _priority;
	PacketReliability _reliability;
	
};


//---------- New Client joined Pkg --------------//

class StelClientJoinedPackage : public StelNetPackage{
public:
	StelClientJoinedPackage() : StelNetPackage(StelNetPkgID::CLIENT_JOINED){}

	virtual void write(RakNet::BitStream& bs){}
	virtual void read(RakNet::BitStream& bs){}
	virtual void execute(StelPackageListener* listener) {

		//- lols remove later
		std::string name;
		int namelen = 5 + (StelRandom::getInstance().getFloatNoZero0_1() * 7);
		const char* letters[2] = { "bcdfghjklmnpqrstvwxyz", "aeiouy" };
		size_t letterlen[2] = { strlen(letters[0]), strlen(letters[1]) };
		for (int i = 0; i<namelen; i++)
			name += letters[i % 2][StelRandom::getInstance().getUnsignedInt0_N() % letterlen[i % 2]];
		name[0] = toupper(name[0]);

		listener->addController(_guid, name);
	}
};

//---------- New Client left Pkg --------------//

class StelClientLeftPackage : public StelNetPackage{
public:
	StelClientLeftPackage() : StelNetPackage(StelNetPkgID::CLIENT_LEFT){}

	virtual void write(RakNet::BitStream& bs){}
	virtual void read(RakNet::BitStream& bs){}
	virtual void execute(StelPackageListener* listener) {
		listener->removeController(_guid);
	}
};

//---------- StelGameState Change Pkg --------------//


class StelGameStateChangePkg : public StelNetPackage{
public:
	StelGameState _state;
	StelTeamID _winnerTeamID;
	StelGameStateChangePkg() : StelNetPackage(StelNetPkgID::GAME_STATE){};
	
	virtual void write(RakNet::BitStream& bs);
	virtual void read(RakNet::BitStream& bs);
	virtual void execute(StelPackageListener* listener);
};	



#endif //STELNETPACKAGES_H
