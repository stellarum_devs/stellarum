
#include "StelPlanet.hpp"
#include "StelPrimitives.hpp"

StelPlanet::StelPlanet() :
_position(Vector3::zero()),
_rotation(Quaternion::identity()),
_node(NULL),
_collisionObject(NULL),
_turretNode(NULL),
_captureMode(MODE_STATIC),
_captureTeamID(UNASSIGNED_TEAMID)
{
}

StelPlanet::~StelPlanet()
{
	delete [] *_totems; // erm not sure of this
}

void StelPlanet::initialize(const int radius,std::string materialPath, Vector3 orbitCenter, float orbitRadius, Vector3 axis, float velocity, Vector3 rotationVelocity, float gravityRadius)
{
	_orbitCenter = orbitCenter;
	_orbitRadius = orbitRadius;
	_velocity = velocity;
	_rotationVelocity = rotationVelocity;
	_axis = axis.normalize();
	_gravityRadius = gravityRadius;
	_materialPath = materialPath;
	_sphereRadius = radius;
	initialize();
}

void StelPlanet::initialize() 
{
	_node = createPlanetModel();// Node::create("planet");
	//_node->setModel(Model::create(createPrimitive("Hedra_tetra")));
	//_node->setModel(createPlanetModel());
	//TODO ye old sphere Model* planetModel = Model::create(createSphereMesh(_sphereRadius));
	//_node->setModel(planetModel);
	//planetModel->setMaterial(materialPath.c_str());
	//Initialise physics
	//const char* physics_res_path = "res/physics/physics.physics#sphere";
	//printf("mi radio %f\n", _node->getBoundingSphere().radius);
	//_node->setScale(_sphereRadius*4);
	PhysicsRigidBody::Parameters p;
	p.kinematic = true;
	p.mass = 100.f;
	p.friction = 0.5f;
	p.restitution = 0.5f;
	p.linearDamping = 0.1f;
	p.angularDamping = 0.5f;
	_collisionObject = _node->setCollisionObject(PhysicsCollisionObject::RIGID_BODY, PhysicsCollisionShape::mesh(_node->getModel()->getMesh()),&p);
	PhysicsRigidBody* rigidBody = (PhysicsRigidBody*)_collisionObject;
	
	rigidBody->setKinematic(true);
	rigidBody->setGravity(0,0,0);
	//_collisionObject = this->_node->setCollisionObject(physics_res_path, STEL_PLANET_COLLMASK);

	_turretNode = Node::create("turret");
	_turretNode->setModel(Model::create(createCubeMesh()));
	_node->addChild(_turretNode);
	_turretNode->translateUp(-95.f);
}

void StelPlanet::update(float elapsedTime)
{	
	//Circular movement
	_angle = fmod ( _angle +elapsedTime * _velocity, MATH_PIX2 );

	Vector3 z = Vector3(0,0,1);
	Matrix m;
	rotationAlign(_axis,z,&m);
	Vector3 point = Vector3(_orbitRadius*cos(_angle),_orbitRadius*sin(_angle),0);
	m.transformVector(&point);
	_node->setTranslation(point+_orbitCenter);
	
	if(_rotationVelocity != Vector3(0,0,0)) {
		_node->rotate(_rotationVelocity, elapsedTime*_rotationVelocity.length());
	}

	for (unsigned int t = 0; t < NUM_MODES - 1; t++)
	{
		_totems[t]->update(elapsedTime);
	}
}

Vector3 StelPlanet::aimTarget(Vector3 origin, Vector3 targetPos, Vector3 targetVel, float projSpeed)
{
	Vector3 toTarget = targetPos - origin;

	float a = (projSpeed * projSpeed) - targetVel.dot(targetVel);
	float b = 2 * targetVel.dot(toTarget);
	float c = toTarget.dot(toTarget);

	float d = b*b + a*c;

	float t = 0;
	if (d >= 0)
	{
		t = (b + sqrt(d)) / a;
		if (t < 0)
			t = 0;
	}

	Vector3 aimSpot = targetPos + targetVel * t;
	Vector3 bulletPath = aimSpot - origin;

	return bulletPath * projSpeed * 0.01f; // TODO this is not nice
}

