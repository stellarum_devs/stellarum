#ifndef STELBASE_H_
#define STELBASE_H_

#include "gameplay.h"
#include "StelDefines.hpp"
#include "StelNetObj.hpp"

using namespace gameplay;

class StelBase : virtual public StelNetObj
{
public:
	const static int MAX_HITPOINTS;

	StelBase();
	virtual ~StelBase();

	virtual void update(float elapsedTime);

	std::string getAllocID()const { return "Base"; }
	
	inline StelTeamID getTeamID(){ return _teamID; }
	inline Node* getNode() const {return _baseNode;}

protected:

	// initialize protected so we DONT use it, use client's or server's
	void initialize(StelTeamID teamID, int hitPoints);
	StelTeamID _teamID;
	Node* _baseNode;
	int _hitPoints;
};

#endif
