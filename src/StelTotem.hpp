#ifndef STELTOTEM_H_
#define STELTOTEM_H_

#include "gameplay.h"
#include "StelNetObj.hpp"
#include "StelDefines.hpp"

using namespace gameplay;

static const float TOTEM_CAPTURE_TIME = 5000.f;

class StelTotem : virtual public StelNetObj, public StelUsable
{
public:
	StelTotem();
	virtual ~StelTotem();

	virtual void update(float elapsedTime) = 0;
	
	std::string getAllocID()const { return "Totem"; }

	Node* getNode() const {return _node;}

	// Usable interface
	virtual void startUsing(void* who);
	virtual void stopUsing();
	virtual void lostUser();


protected:
	virtual void initialize();
	void initPosition();

	// Capture mode
	StelCaptureMode _mode;	
	StelTeamID _capturingTeamID;
	float _captureTime;
	bool _capturing;

	// Gameplay node
	Node* _node;
};

#endif
