#include "StelTotem.hpp"

StelTotem::StelTotem() :
  _node(NULL),
  _capturingTeamID(UNASSIGNED_TEAMID),
  _captureTime(0.f),
  _capturing(false),
  _mode(MODE_STATIC)
{
}

StelTotem::~StelTotem()
{
}

void StelTotem::initialize()
{
	_node->setTag("Usable", "True");
}

void StelTotem::initPosition()
{
	switch (_mode)
	{
		case MODE_ATTACK:
			_node->translateForward(90.f);//TODO use or remove _sphereRadius
			_node->rotateX(MATH_DEG_TO_RAD(180));
			break;
		case MODE_DEFEND:
			_node->translateUp(-90.f);
			_node->rotateX(MATH_DEG_TO_RAD(90));
			break;
		case MODE_ENERGIZE:
			_node->translateLeft(90.f);
			_node->rotateY(MATH_DEG_TO_RAD(270));
			break;
	}
}

void StelTotem::startUsing(void* who)
{
}

void StelTotem::stopUsing()
{
	_inUse = false;
	_capturingTeamID = UNASSIGNED_TEAMID;
}

void StelTotem::lostUser()
{
	stopUsing();
}
