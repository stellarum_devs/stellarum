#include "StelPrimitives.hpp"


Mesh* createCubeMesh(const float size)
{
	float a = size;
	float vertices[] =
	{
		-a, -a, -a, 0.0, 1.0,
		-a, a, -a, 0.0, 0.0,
		a, a, -a, 1.0, 0.0,
		a, -a, -a, 1.0, 1.0,
		-a, -a, a, 0.0, 0.0,
		a, -a, a, 0.0, 1.0,
		a, a, a, 0.0, 0.0,
		-a, a, a, 0.0, 1.0
	};

	short indices[] =
	{
		0, 1, 2, 2, 3, 0, 4, 5, 6,
		6, 7, 4, 0, 3, 5, 5, 4, 0,
		3, 2, 6, 6, 5, 3, 2, 1, 7,
		7, 6, 2, 1, 0, 4, 4, 7, 1
	};
	unsigned int vertexCount = 8;
	unsigned int indexCount = 36;
	VertexFormat::Element elements[] =
	{
		VertexFormat::Element(VertexFormat::POSITION, 3),
		VertexFormat::Element(VertexFormat::TEXCOORD0, 2)
	};
	Mesh* mesh = Mesh::createMesh(VertexFormat(elements, 2), vertexCount, false);
	if (mesh == NULL)
	{
		GP_ERROR("Failed to create cube mesh.");
		return NULL;
	}
	mesh->setVertexData(vertices, 0, vertexCount);
	MeshPart* meshPart = mesh->addPart(Mesh::TRIANGLES, Mesh::INDEX16, indexCount, false);
	meshPart->setIndexData(indices, 0, indexCount);

	mesh->setBoundingBox(BoundingBox(Vector3(-size, -size, -size), Vector3(size, size, size)));
	mesh->setBoundingSphere(BoundingSphere(Vector3::zero(), sqrt(3 * size * size))); 
	return mesh;
}

Mesh* createSphereMesh(float radius, int nRings, int nSlices)
{
	// Allocate enough buffer space.
	int sVertexBufferCapacity = nRings * (nSlices + 1);
	int sIndexBufferCapacity = (nRings - 1) * (nSlices + 1) * 2;

	// Create vertex buffer
	std::vector<float> sVertexBuffer;
	sVertexBuffer.resize(sVertexBufferCapacity * 8);

	// Create index buffer
	std::vector<unsigned short> sIndexBuffer;
	sIndexBuffer.resize(sIndexBufferCapacity);

	unsigned int vertexCounter = -1; // lolololo esto es un poco asco
	unsigned int i;
	for (i = 0; i < nRings; i++)
	{
		float phi = ((float)i / (float)(nRings - 1) - 0.5f) * (float)MATH_PI;
		for (int j = 0; j <= nSlices; j++)
		{
			float theta = (float)j / (float)nSlices * (float)MATH_PIX2;
			int n = i * (nSlices + 1) + j;
			float x = (float)(cos(phi) * cos(theta));
			float y = (float)sin(phi);
			float z = (float)(cos(phi) * sin(theta));

			sVertexBuffer[++vertexCounter] = x * radius;
			sVertexBuffer[++vertexCounter] = y * radius;
			sVertexBuffer[++vertexCounter] = z * radius;

			sVertexBuffer[++vertexCounter] = x;
			sVertexBuffer[++vertexCounter] = y;
			sVertexBuffer[++vertexCounter] = z;

			sVertexBuffer[++vertexCounter] = 1.0f - (float)j / (float)nSlices;
			sVertexBuffer[++vertexCounter] = 1.0f - (float)i / (float)(nRings - 1);

			// Compute the tangent--required for bump mapping
			/* float tx = (float) (sin(phi) * sin(theta));
			float ty = (float) -cos(phi);
			float tz = (float) (sin(phi) * cos(theta));
			sVertexBuffer[++vertexCounter] = tx;
			sVertexBuffer[++vertexCounter] = ty;
			sVertexBuffer[++vertexCounter] = tz;*/
		}
	}

	unsigned int indexCounter = -1;
	for (i = 0; i < nRings - 1; i++)
	{
		for (int j = 0; j <= nSlices; j++)
		{
			int n = i * (nSlices + 1) + j;
			sIndexBuffer[++indexCounter] = i * (nSlices + 1) + j;
			sIndexBuffer[++indexCounter] = (i + 1) * (nSlices + 1) + j;
		}
	}

	VertexFormat::Element elements[] =
	{
		VertexFormat::Element(VertexFormat::POSITION, 3),
		VertexFormat::Element(VertexFormat::NORMAL, 3),
		VertexFormat::Element(VertexFormat::TEXCOORD0, 2)
		//,VertexFormat::Element(VertexFormat::TANGENT, 3)
	};

	Mesh* mesh = Mesh::createMesh(VertexFormat(elements, 3), sVertexBufferCapacity, false);
	if (mesh == NULL)
	{
		GP_ERROR("Failed to create sphere mesh.");
		return NULL;
	}
	mesh->setVertexData(&sVertexBuffer[0], 0, sVertexBufferCapacity);

	MeshPart* meshPart = mesh->addPart(Mesh::TRIANGLE_STRIP, Mesh::INDEX16, sIndexBufferCapacity, false);
	meshPart->setIndexData(&sIndexBuffer[0], 0, sIndexBuffer.size());

	mesh->setBoundingBox(BoundingBox(Vector3(-radius, -radius, -radius), Vector3(radius, radius, radius)));
	mesh->setBoundingSphere(BoundingSphere(Vector3::zero(), radius));
	return mesh;
}

Mesh* createPrimitive(std::string name)
{
	// Load primitive mesh from bundle
	Bundle* bundle = Bundle::create("res/models/primitives.gpb");
	GP_ASSERT(bundle);
	std::string meshSuffix = "_Mesh";
	Mesh* mesh = bundle->loadMesh(name.append(meshSuffix).c_str());
	SAFE_RELEASE(bundle);

	return mesh;
}

Node* createPlanetModel()
{
	// Load primitive mesh from bundle
	Bundle* bundle = Bundle::create("res/models/planet_Med.gpb");
	GP_ASSERT(bundle);

	Node* n = bundle->loadNode("RoadKillOut");
	SAFE_RELEASE(bundle);

	return n;
}

void rotationAlign( const Vector3 & d, const Vector3 & z, Matrix* dst )
{
    Vector3  v;
	Vector3::cross(z, d, &v);
    const float c = Vector3::dot(z,d);
    const float k = 1.0f/(1.0f+c);
	


/*
    return mat3x3( v.x*v.x*k + c,     v.y*v.x*k - v.z,    v.z*v.x*k + v.y,
                   v.x*v.y*k + v.z,   v.y*v.y*k + c,      v.z*v.y*k - v.x,
                   v.x*v.z*K - v.y,   v.y*v.z*k + v.x,    v.z*v.z*k + c    );

*/

	dst->m[0] = v.x*v.x*k + c; 
	dst->m[1] = v.y*v.x*k - v.z;
	dst->m[2] = v.z*v.x*k + v.y;
	dst->m[3] = 0.0f;

	dst->m[4] = v.x*v.y*k + v.z;
	dst->m[5] = v.y*v.y*k + c;
	dst->m[6] = v.z*v.y*k - v.x;
	dst->m[7] = 0.0f;

	dst->m[8] = v.x*v.z*k - v.y;
	dst->m[9] = v.y*v.z*k + v.x;
	dst->m[10] = v.z*v.z*k + c;
	dst->m[11] = 0.0f;

	dst->m[12] = 0.0f;
	dst->m[13] = 0.0f;
	dst->m[14] = 0.0f;
	dst->m[15] = 1.0f;


}


