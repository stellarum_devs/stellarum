#include "StelNetCube.hpp"
#include "StelPrimitives.hpp"

static const float DOWNWARD_ACCELERATION = -15.0f;
static const float POSITION_VARIANCE = 100.0f;

StelNetCube::StelNetCube() : _position(Vector3::zero()), _rotation(Quaternion::identity()), _velocity(Vector3::zero())
{
	// Initialize netcube node
	std::string nodeId = "netCube_" + std::to_string(Scene::getScene()->getNodeCount());
	_netCubeNode = Scene::getScene()->addNode(nodeId.c_str());
	Mesh* netCubeMesh = createCubeMesh(10.f);
	Model* netCubeModel = Model::create(netCubeMesh);
	_netCubeNode->setModel(netCubeModel);
	// Release items
	SAFE_RELEASE(netCubeMesh);
	SAFE_RELEASE(netCubeModel);
}

StelNetCube::~StelNetCube()
{
	_netCubeNode->release();
}