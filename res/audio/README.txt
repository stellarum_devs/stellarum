oggdec.exe myfile.ogg
oggenc2.exe --downmix myfile.wav -o myfile_mono.ogg
rm myfile.wav
If you wish to control the output quality, run oggenc with the -q flag with a value from -1 (very low) to 10 (very high)