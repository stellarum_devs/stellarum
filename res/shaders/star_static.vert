#version 120

// Inputs
attribute vec4 a_position;
attribute vec4 a_color;

// Uniforms
uniform mat4 u_worldViewProjectionMatrix;
uniform float u_size;

// Varyings
varying vec4 starColor;

float rand(vec2 n)
{
  return 2.0 + u_size * fract(sin(dot(n.xy, vec2(12.9898, 78.233)))* 43758.5453);
}

void main()
{	
	starColor = a_color * 2.0;

    gl_Position = u_worldViewProjectionMatrix * a_position;
	gl_PointSize = rand(a_position.xz);
}