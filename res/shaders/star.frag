#version 120

// Uniforms
uniform sampler2D u_texture;

// Varyings
varying vec4 starColor;

void main ()
{
	gl_FragColor = starColor * texture2D(u_texture, gl_PointCoord);
	//gl_FragColor = vec4(gl_PointCoord.st, 0, 1);
}