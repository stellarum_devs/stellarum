#version 120

// Inputs
attribute vec4 a_position;
attribute vec4 a_color;

// Uniforms
uniform float u_elapsedTime;
uniform vec3 u_center;
uniform mat4 u_worldViewProjectionMatrix;

// Varyings
varying vec4 starColor;

void main()
{
	vec4 newVertex = a_position;
		
	float size = max(90.0 - (10.0 * u_elapsedTime), 7.0);
	
    starColor = smoothstep(1.0, 7.0, size) * a_color;
		
	float xxx = u_elapsedTime * 20.0;
	
	newVertex = a_position;
	
	vec3 dir = newVertex.xyz - u_center;
	dir = normalize(dir);
	newVertex += vec4(dir * xxx, 0.0);
	
    gl_Position = u_worldViewProjectionMatrix * newVertex;
	gl_PointSize = size;
}