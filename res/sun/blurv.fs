#version 120

uniform sampler2D u_texture;
uniform int u_width;
uniform float u_odh;

void main()
{
	vec3 color = vec3(0.0);
	int wp1 = u_width + 1;
	float sum = 0.0;
	
	for (int y = -u_width; y <= u_width; y++)
	{
		float width = (wp1 - abs(float(y)));
		color += texture2D(u_texture, gl_TexCoord[0].st + vec2(0.0, u_odh * y)).rgb * width;
		sum += width;
	}
	
	gl_FragColor = vec4(color / sum, 1.0);
}
