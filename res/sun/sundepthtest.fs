#version 120

uniform sampler2D u_sunTexture;
uniform sampler2D u_sunDepthTexture;
uniform sampler2D u_depthTexture;

void main()
{
	if(texture2D(u_depthTexture, gl_TexCoord[0].st).r < texture2D(u_sunDepthTexture, gl_TexCoord[0].st).r)
	{
		gl_FragColor = vec4(vec3(0.0), 1.0);
	}
	else
	{
		gl_FragColor = vec4(texture2D(u_sunTexture, gl_TexCoord[0].st).rgb, 1.0);
	}
}
