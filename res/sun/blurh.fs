#version 120

uniform sampler2D u_texture;
uniform int u_width;
uniform float u_odw;

void main()
{
	vec3 color = vec3(0.0);
	int wp1 = u_width + 1;
	float sum = 0.0;
	
	for (int x = -u_width; x <= u_width; x++)
	{
		float width = (wp1 - abs(float(x)));
		color += texture2D(u_texture, gl_TexCoord[0].st + vec2(u_odw * x, 0.0)).rgb * width;
		sum += width;
	}
	
	gl_FragColor = vec4(color / sum, 1.0);
}
