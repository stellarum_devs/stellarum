#version 120

uniform sampler2D u_lowBlurredSunTexture, u_highBlurredSunTexture, u_dirtTexture;

uniform float u_dispersal, u_haloWidth, u_intensity;
uniform vec2 u_sunPosProj;
uniform vec3 u_distortion;

vec3 texture2DDistorted(sampler2D tex, vec2 texCoord, vec2 Offset)
{
	return vec3(
		texture2D(tex, texCoord + Offset * u_distortion.r).r,
		texture2D(tex, texCoord + Offset * u_distortion.g).g,
		texture2D(tex, texCoord + Offset * u_distortion.b).b
	);
}

void main()
{
	vec3 radialBlur = vec3(0.0);
	vec2 texCoord = gl_TexCoord[0].st;
	int radialBlurSamples = 64;//128; //boost!
	vec2 radialBlurVector = (u_sunPosProj - texCoord) / radialBlurSamples;

	for(int i = 0; i < radialBlurSamples; i++)
	{
		radialBlur += texture2D(u_lowBlurredSunTexture, texCoord).rgb;
		texCoord += radialBlurVector;
	}

	radialBlur /= radialBlurSamples;

	vec3 lensFlareHalo = vec3(0.0);
	texCoord = 1.0 - gl_TexCoord[0].st;
	vec2 lensFlareVector = (vec2(0.5) - texCoord) * u_dispersal;
	vec2 lensFlareOffset = vec2(0.0);

	for(int i = 0; i < 5; i++)
	{
		lensFlareHalo += texture2DDistorted(u_highBlurredSunTexture, texCoord, lensFlareOffset).rgb;
		lensFlareOffset += lensFlareVector;
	}

	lensFlareHalo += texture2DDistorted(u_highBlurredSunTexture, texCoord, normalize(lensFlareVector) * u_haloWidth);

	lensFlareHalo /= 6.0;

//    vec2 aa = vec2(0.0, 0.0);
//    gl_FragColor = vec4(abs((gl_TexCoord[0].st - u_sunPosProj.st)), 0.0, 1.0);
	gl_FragColor = vec4((texture2D(u_highBlurredSunTexture, gl_TexCoord[0].st).rgb + (radialBlur + lensFlareHalo) * texture2D(u_dirtTexture, gl_TexCoord[0].st).rgb) * u_intensity, 1.0);
}
