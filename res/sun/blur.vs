attribute vec4 a_position;                            // Vertex Normal (x, y, z)

void main()
{
    vec4 position = a_position;
	gl_TexCoord[0] = position;
    gl_Position = position * 2.0 - 1.0;
}

